//
//  BasePresentVC.m
//  Mp3Player
//
//  Created by doduong on 4/6/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "BasePresentVC.h"

@interface BasePresentVC ()

@end

@implementation BasePresentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addBanner];
    
    [_btnClose setTitle:TextForKey(@"Close") forState:UIControlStateNormal];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
//    [AppDelegate trackScreen:NSStringFromClass([self class])];
//    
//    NSString *name = [NSString stringWithFormat:@"%@-%@",kAppId,[HDVLocalSetting getCurrentVersion]];
//    [AppDelegate trackScreen:name];
}

-(void) addBanner
{
    if([HDVLocalSetting getDisableShowAd])
    {
        return;
    }
    
    if([[HDVAdsManager sharedInstance] bannerIsReady])
    {
        UIView *banner = [[HDVAdsManager sharedInstance] getCurrentBanner];
        CGRect frame = banner.frame;
        frame.origin.x = (kWinsize.width - frame.size.width)*0.5;
        frame.origin.y = kWinsize.height - frame.size.height;
        banner.frame = frame;
        banner.autoresizingMask = UIViewAutoresizingNone;
        [self.view addSubview:banner];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)setupNavItem
{
    
}

- (IBAction)clickClose:(id)sender
{
    [self dismiss];
}

-(void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [[HDVAdsManager sharedInstance] showAds];
}

@end








