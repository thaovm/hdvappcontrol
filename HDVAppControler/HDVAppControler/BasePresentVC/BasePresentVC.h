//
//  BasePresentVC.h
//  Mp3Player
//
//  Created by doduong on 4/6/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BasePresentVC : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *cvMain;
@property (weak, nonatomic) IBOutlet UIView *viewBottom;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;

- (IBAction)clickClose:(id)sender;

-(void) dismiss;


@end
