//
//  HDVAdsManager.m
//  HDVAppControler
//
//  Created by ThaoVM on 9/4/15.
//  Copyright (c) 2015 ThaoVM. All rights reserved.
//

#import "HDVAdsManager.h"
#import "AppDelegate.h"
#import "HDVLocalData.h"
#import "ObjectScoreApp.h"
#import "HDVAdsView.h"
#import "HDVAdsPopup.h"
#import "HDVLocalSetting.h"
#import "HDVAppControll.h"

@interface HDVAdsManager ()<AdColonyAdDelegate,AdColonyDelegate,GADBannerViewDelegate,GADInterstitialDelegate,STABannerDelegateProtocol,STADelegateProtocol,FBAdViewDelegate,FBInterstitialAdDelegate,MCAdUnitEventDelegate,MobileCoreDelegate,HDVAdsPopupDelegate>{
    
    
    GADInterstitial *_gadPopup;
    
    GADBannerView   *_gadBanner;
    GADBannerView   *_gadBannerBottom;
    GADBannerView   *_gadBannerForAudio;
    GADBannerView   *_gadThumbnail;
    STABannerView *_staBanner;
    STABannerView *_staBannerBottom;
    STABannerView *_staBannerForAudio;
    
    
    STAStartAppAd *_staPopup;
    
    FBAdView *_faceBanner;
    FBAdView *_faceBannerBottom;
    FBAdView *_faceBannerForAudio;
    FBAdView *_faceThumbnail;
    
    FBInterstitialAd *_facePopup;
    
    HDVAdsView *_hdvBanner;
    HDVAdsView *_hdvBannerForAudio;
    HDVAdsView *_hdvBannerBottom;
    HDVAdsView *_hdvThumbnail;
    
    HDVAdsPopup *_hdvPopup;
    
    NSInteger _popupStamp;
    NSInteger _timeClickBanner;
    NSInteger _createStamp;
    
    
    
    NSInteger _mobileCoreStamp;
    
    BOOL _havePopup;

    
    BOOL _haveThumbnail;
    BOOL _haveBanner;
    BOOL _haveBannerForAudio;
    BOOL _haveBannerBottom;
    
    BOOL _haveClickAds;
    int _positionMobileCore;
    NSArray * _arrayListAds;
    eAdsTypePopup _typePopup;
    
}

@end
@implementation HDVAdsManager
+(instancetype)sharedInstance
{
    static HDVAdsManager *manager = NULL;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[HDVAdsManager alloc] init];
    });
    return manager;
}
-(instancetype) init
{
    if(self = [super init])
    {
        
    }
    return self;
}

-(void)prepareAds{
    

    if([HDVLocalSetting getDisableShowAd] || _leaveAppWhenClick)
    {
        return;
    }
    _timeClickBanner = [[[NSUserDefaults standardUserDefaults] objectForKey:@"com.hdv.clickBanner"] integerValue];
    _arrayListAds = [HDVAppControll sharedInstance].dictAppControll[@"list_ads"];
    _createStamp = [[NSDate date] timeIntervalSince1970];
    //Create Banner
    if([self showBanner])
    {
        _haveBanner = NO;
        _haveBannerBottom = NO;
        _haveBannerForAudio = NO;
        NSString * typeBanner = [self getAdsBannerType];
        if([typeBanner isEqualToString: kHDVStringAdmob]){
            _gadBanner = [self createAdsGADBanner];
//            _gadBannerForAudio = [self createAdsGADBanner];
//            _gadBannerBottom = [self createAdsGADBanner];
        }else if ([typeBanner isEqualToString:kHDVStringFace]){
            _faceBanner = [self createAdsFaceBanner];
//            _faceBannerBottom = [self createAdsFaceBanner];
//            _faceBannerForAudio = [self createAdsFaceBanner];
        }else if ([typeBanner isEqualToString:kHDVStringSTA]){
            _staBanner = [self createAdsSTABanner];
//            _staBannerForAudio = [self createAdsSTABanner];
//            _staBannerBottom = [self createAdsSTABanner];
        }
        _hdvBanner = [self createHDVAdsBaner];
        _hdvBannerBottom = [self createHDVAdsBaner];
        _hdvBannerForAudio = [self createHDVAdsBaner];
    }
    
    //Create Popup
    _havePopup = NO;
    _popupStamp =  [self getTimePopupStamp];
    NSString * typePopup = [self getAdsPopupType];
    if([typePopup isEqualToString: kHDVStringAdmob]){
        _gadPopup = [self createAdsGADPopup];
    }else if ([typePopup isEqualToString:kHDVStringFace]){
        _facePopup = [self createAdsFacePopup];
    }else if ([typePopup isEqualToString:kHDVStringSTA]){
        _staPopup = [self createAdsSTAPopup];
    }
    _hdvPopup = [self createHDVAdsPopup];
    
    //Create Thumbnail
    _haveThumbnail = NO;
    NSString * typeThumb = [self getAdsThumbType];
    if([typeThumb isEqualToString: kHDVStringFace]){
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
            _faceThumbnail = [self createAdsFaceThumbnail];
        }else{
            _gadThumbnail = [self createAdsGADThumbnail];
        }
    }
    
    _hdvThumbnail = [self createHDVAdsThumbmail];
    //load Mobile Core
    [self loadAdsMobileCore];
}

-(void)showAds{
    if([HDVLocalSetting getDisableShowAd])
    {
        return;
    }
    if([MobileCore isStickeeReady]){
        [self showAdsMobileCore];
    }
    [self showCurrentNormalPopup];
}



-(NSString *)getAppleStoreId
{
    if(![[HDVAppControll sharedInstance].dictAppControll[@"appid"] isKindOfClass:[NSString class]])
    {
        return nil;
    }
    
    if([[HDVAppControll sharedInstance].dictAppControll[@"appid"] length] == 0)
    {
        return nil;
    }
    
    return [HDVAppControll sharedInstance].dictAppControll[@"appid"];
}


-(NSString *)getAdsBannerType{
//    return kHDVStringFace;
    if(![[HDVAppControll sharedInstance].dictAppControll[@"ads_network_new"][@"banner"] isKindOfClass:[NSString class]]){
        return @"";
    }
    return [HDVAppControll sharedInstance].dictAppControll[@"ads_network_new"][@"banner"];
}

-(NSString *)getAdsPopupType{
//    return kHDVStringFace;
    if(![[HDVAppControll sharedInstance].dictAppControll[@"ads_network_new"][@"popup"] isKindOfClass:[NSString class]]){
        return @"";
    }
    return [HDVAppControll sharedInstance].dictAppControll[@"ads_network_new"][@"popup"];
}

-(NSString *)getAdsThumbType{
    if(![[HDVAppControll sharedInstance].dictAppControll[@"ads_network_new"][@"thumbai"] isKindOfClass:[NSString class]]){
        return @"";
    }
    return [HDVAppControll sharedInstance].dictAppControll[@"ads_network_new"][@"thumbai"];
}


-(NSString *)getAdsIdWith:(eAdsType)type{
    NSString * aId = @"";
    NSString * sId = @"";
    NSString * key = @"";
    if(type == eAdsTypeGadBanner){
        key = @"common_ad_id_banner";
        aId = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        sId = [HDVAppControll sharedInstance].dictAppControll[@"key"][@"admob"][@"banner"];
        sId = sId.length > 0? sId:kHDVAdsGADBannerId;
    }
    if (type == eAdsTypeGadPopup) {
        key = @"common_ad_id_popup";
        aId = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        sId =[HDVAppControll sharedInstance].dictAppControll[@"key"][@"admob"][@"popup"];
        sId = sId.length > 0? sId:kHDVAdsGADPopupId;
    }
    if(type == eAdsTypeStaDevId){
        key = @"common_ad_id_dev";
        aId = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        sId = [HDVAppControll sharedInstance].dictAppControll[@"key"][@"startapps"][@"devid"];
        sId = sId.length > 0? sId:kHDVAdsSTADevId;
    }
    if(type == eAdsTypeMobileCore){
        key = @"common_ad_id_mobilecore";
        aId = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        sId = [HDVAppControll sharedInstance].dictAppControll[@"key"][@"mobilecore"][@"developer_hashcode"];
        sId = sId.length > 0? sId:KHDVMobileCoreHashCode;
    }
    if(type == eAdsTypeFaceBanner){
        key = @"common_ad_id_facebanner";
        aId = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        sId = [HDVAppControll sharedInstance].dictAppControll[@"key"][@"facebook"][@"banner"];
        sId = sId.length > 0? sId: kHDVFaceBannerId;
    }
    if(type == eAdsTypeFaceThumbnai){
        key = @"common_ad_id_facethumb";
        aId = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        sId = [HDVAppControll sharedInstance].dictAppControll[@"key"][@"facebook"][@"thumbai"];
        sId = sId.length > 0? sId: kHDVFaceThumbnaiId;
    }
    if(type == eAdsTypeFacePopup){
        key = @"common_ad_id_facepopup";
        aId = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        sId = [HDVAppControll sharedInstance].dictAppControll[@"key"][@"facebook"][@"popup"];
        sId = sId.length > 0? sId: kHDVFacePopupId;
        
    }
    if(type == eAdsTypeCodonyAppId){
        key = @"common_ad_id_codony_appid";
        aId = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        sId = [HDVAppControll sharedInstance].dictAppControll[@"key"][@"adcolony"][@"appid"];
        sId = sId.length > 0? sId:kAdsColonyAppID;
    }
    if(type == eAdsTypeCodonyZoneId){
        key = @"common_ad_id_codony_zoneid";
        aId = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        sId = [HDVAppControll sharedInstance].dictAppControll[@"key"][@"adcolony"][@"zoneid"];
        sId = sId.length > 0? sId:kAdsColonyScoreZoneID;
    }
    if(![sId isKindOfClass:[NSString class]])
    {
        if(aId != nil)
        {
            return aId;
        }
        return nil;
    }
    else if(![sId length])
    {
        if(aId != nil)
        {
            return aId;
        }
        return nil;
    }
    if(![sId isEqualToString:aId])
    {
        [[NSUserDefaults standardUserDefaults] setObject:sId forKey:key];
    }
    return sId;
}
#pragma mark - limit click

-(NSInteger) getClickAdsLimit
{
    return [HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"max_click_of_date"]?[[HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"max_click_of_date"] integerValue]:5;
}


#pragma mark - check show ads
-(BOOL)showBanner{
    return [[HDVAppControll sharedInstance].dictAppControll[@"show_banner_ads_network"] intValue] == 1;
}


#pragma mark - get time progress

-(NSInteger) getTimeOffsetShowUpdate
{
    return [HDVAppControll sharedInstance].dictAppControll[@"update"][@"offset_show"]?[[HDVAppControll sharedInstance].dictAppControll[@"update"][@"offset_show"] integerValue]:60;
}

-(NSInteger) getTimeStartShowAd
{
    return [HDVAppControll sharedInstance].dictAppControll[@"time_start_show_ads"]?[[HDVAppControll sharedInstance].dictAppControll[@"time_start_show_ads"] integerValue]:60;
}
-(NSInteger) getTimeOffsetShowPopup
{

    return [HDVAppControll sharedInstance].dictAppControll[@"offset_time_show_popup"]?[[HDVAppControll sharedInstance].dictAppControll[@"offset_time_show_popup"] integerValue]:100;
}

-(NSInteger) getTimeOffsetToRefresh
{
    return [HDVAppControll sharedInstance].dictAppControll[@"offset_time_request"]?[[HDVAppControll sharedInstance].dictAppControll[@"offset_time_request"] integerValue]:100;
}

-(NSInteger) getOffsetShowThumbnail
{
    NSInteger time = [HDVAppControll sharedInstance].dictAppControll[@"offset_time_show_thumbai"]?[[HDVAppControll sharedInstance].dictAppControll[@"offset_time_show_thumbai"] integerValue]:150;
    return time;
}

- (NSInteger)getTimeOffsetShowMobileCore {
    NSInteger time = [HDVAppControll sharedInstance].dictAppControll[@"offset_time_show_ads_mobilecore"]?[[HDVAppControll sharedInstance].dictAppControll[@"offset_time_show_ads_mobilecore"] integerValue]:150;
    return time;
}


-(NSInteger)getTimeHideAds
{
    if([[HDVAppControll sharedInstance].dictAppControll[@"time_hidden_ads_to_click"] isKindOfClass:[NSNull class]])
    {
        return 60;
    }
    return [[HDVAppControll sharedInstance].dictAppControll[@"time_hidden_ads_to_click"] integerValue];
}

-(void)saveTimeHideAds:(NSInteger)time{
    
}

-(NSInteger)getTimeoffsetShowScoreViewPoup
{
    return [HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"offset_time_view_popup"]?[[HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"offset_time_view_popup"] integerValue]:0;
}
-(NSInteger)getTimeoffsetShowScoreAd
{
    return [HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"offset_time_show_ads"]?[[HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"offset_time_show_ads"] integerValue]:0;
}



#pragma mark - get list bonus
-(NSArray *)getListBonus
{
    NSMutableArray *arr = [[NSMutableArray alloc] initWithArray:[HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"list_bonus"]];
    
    for (int i = 0; i<arr.count; i++)
    {
        NSDictionary *info = arr[i];
        if([info[@"status"] intValue] == 0)
        {
            [arr removeObject:info];
            i--;
        }
    }
    return arr;
}

-(NSArray *)getAllListBonus
{
    NSMutableArray *arr = [[NSMutableArray alloc] initWithArray:[HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"list_bonus"]];
    return arr;
}

-(NSArray *)getListBonusApps
{
    NSArray *arr = [HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"list_apps_bonus"];
    NSMutableArray *marr = [[NSMutableArray alloc] init];
    for(NSDictionary *app in arr)
    {
        NSString *scheme = app[@"url_scheme"];
        NSString *scheme1 = [scheme stringByAppendingString:@"://"];
        NSString *scheme2 = [@"://" stringByAppendingString:scheme];
        
        ObjectScoreApp *osa = [[ObjectScoreApp alloc] initWithHdvInfo:app];
        
        BOOL haveApp = NO;
        
        if(([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:scheme1]] || [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:scheme2]]) && ![osa appOnWait])
        {
            continue;
        }
        else
        {
            NSArray *marr2 = [self getPendingAppList];
            for(NSDictionary *app2 in marr2)
            {
                if([app2[@"appid"] isEqualToString:app[@"appid"]] && [app2[@"score"] intValue]<=0)
                {
                    haveApp = YES;
                    break;
                }
            }
        }
        if(!haveApp)
            [marr addObject:app];
    }
    
    return marr;
}

#pragma mark - Download
-(void)clickGetDownload{
    if(_haveClickAds)
    {
        [UIAlertView showAlertViewWithMessage:TextForKey(@"AdsHaveClicked") cancelButtonTitle:TextForKey(@"Close")];
        return;
    }
    _haveClickAds = YES;
    [[HDVDownloadManager sharedInstance] saveDownloadLimit:[[HDVDownloadManager sharedInstance] getDownloadLimit] + [[HDVDownloadManager sharedInstance] getAddDownload]];
    [self saveTimeClickDownloadAds];
}

#pragma mark - score
-(void)clickGetScore{
    if(_haveClickAds)
    {
        [UIAlertView showAlertViewWithMessage:TextForKey(@"AdsHaveClicked") cancelButtonTitle:TextForKey(@"Close")];
        return;
    }
    
    [HDVLocalSetting increaseClickCountForCurrentDay];
    
    _haveClickAds = YES;
    
    NSInteger score = [HDVLocalSetting getBonusScore] + [[self getAllListBonus][4][@"score"] integerValue];
    [HDVLocalSetting saveBonusScore:score];
    
    NSInteger more = [[self getAllListBonus][4][@"score"] integerValue];
    
    NSDictionary *info = [self getAllListBonus][4];
    [[HDVAppControll sharedInstance] reportScoreFromEvent:[info[@"name"] stringByAppendingString:@" startapp"] andScore:[info[@"score"] integerValue] Complete:nil];
    
    NSString *msg = @"";
    if([[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
    {
        msg = [NSString stringWithFormat:@"Thưởng %ld điểm! Cảm ơn bạn đã click quảng cáo.",more];
    }
    else
    {
        msg = [NSString stringWithFormat:@"Earn %ld coins! Thank you!",more];
    }
    
    [UIAlertView showAlertViewWithMessage:msg cancelButtonTitle:TextForKey(@"Ok")];
    if(_didClickScorePopup)
    {
        _didClickScorePopup();
    }
}

-(BOOL) getSystemScoreStatus
{
    return [[HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"status"] intValue] == 1;
}
-(void)savePendingApp:(NSDictionary *)app
{
    NSMutableArray *marr = [[NSMutableArray alloc] initWithArray:[self getPendingAppList]];
    
    for (NSDictionary *app2 in marr) {
        if([app2[@"appid"] isEqualToString:app[@"appid"]])
        {
            return;
        }
    }
    
    if([app[@"url_scheme"] length] == 0)
    {
        return;
    }
    
    NSInteger time = [[NSDate date] timeIntervalSince1970];
    NSInteger score = [app[@"score"] integerValue]?:0;
    
    NSDictionary *app2 = @{
                           @"url_scheme":app[@"url_scheme"],
                           @"time":@(time),
                           @"score":@(score),
                           @"title": app[@"title"]?:@"untitled",
                           @"check":[HDVLocalSetting checksumForIntValue:time+score],
                           @"appid":app[@"appid"],
                           };
    
    [marr addObject:app2];
    BOOL save = [marr writeToFile:[self pathForFile:@"pendingApp"] atomically:YES];
    NSLog(@"Save file: %d",save);
}

-(void)deletePendingApp:(NSDictionary *)app
{
    NSMutableArray *marrApp = [[NSMutableArray alloc] initWithArray:[self getPendingAppList]] ;
    
    for (int i = 0; i<marrApp.count;i++)
    {
        NSMutableDictionary *app2 = [[NSMutableDictionary alloc] initWithDictionary:marrApp[i]];
        
        if([app2[@"url_scheme"] isEqualToString:app[@"url_scheme"]])
        {
            int now = [[NSDate date] timeIntervalSince1970];

            app2[@"score"]=@(0);
            app2[@"time"]=@(now);
            app2[@"check"]=[HDVLocalSetting checksumForIntValue:now];
            marrApp[i] = app2;
        }
    }
    
    [marrApp writeToFile:[self pathForFile:@"pendingApp"] atomically:YES];
}

-(NSArray *) checkAppInstallAndAddPoint
{
    NSArray *arrApp = [self getPendingAppList];
    NSMutableArray *arrScore = [[NSMutableArray alloc] init];
    
    // check to add score...
    for (NSDictionary *app in arrApp)
    {
        NSString *urlScheme = app[@"url_scheme"];
        urlScheme = [urlScheme stringByAppendingString:@"://"];
        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:urlScheme]])
        {
            NSString *check = app[@"check"];
            NSInteger score = [app[@"score"] integerValue];
            NSInteger time = [app[@"time"] integerValue];
            NSString *valid = [HDVLocalSetting checksumForIntValue:time+score];
            
            if([valid isEqualToString:check] && score>0)
            {
                [[HDVAppControll sharedInstance] reportScoreFromEvent:[@"install_app" stringByAppendingFormat:@" %@",app[@"title"]] andScore:score Complete:nil];
                
                score += [HDVLocalSetting getBonusScore];
                [HDVLocalSetting saveBonusScore:score];
                [arrScore addObject:app];
                [self deletePendingApp:app];
            }
        }
    }
    
    arrApp = [self getPendingAppList];
    
    // remove too old pending app
    NSInteger now = [[NSDate date] timeIntervalSince1970];
    NSInteger twoWeeks = 3600*24*14;
    for (NSDictionary *app in arrApp)
    {
        NSInteger time = [app[@"time"] integerValue];
        if(now - time > twoWeeks)
        {
            [self deletePendingApp:app];
        }
    }
    
    return arrScore;
}

-(NSArray *)getPendingAppList
{
    NSArray *arr = [[NSArray alloc] initWithContentsOfFile:[self pathForFile:@"pendingApp"]];
    return arr;
}

-(NSString *)pathForFile:(NSString *)file
{
    NSString *doc = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    return [doc stringByAppendingPathComponent:file];
}

#pragma mark - score pages

-(NSString *)getScoreFanpageLink
{
    return [HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"link_fanpage"];
}

-(NSString *)getScoreFanpageId
{
    return [HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"id_fanpage"];
}

-(NSArray *)getScorePageList
{
    NSArray *arrList = [HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"list_page_bonus"];
    NSMutableArray *arr = [[NSMutableArray alloc] initWithArray:arrList];
    NSArray *arrPending = [HDVLocalSetting getPendingPageList];
    
    for(int i =0; i<arr.count;i++)
    {
        NSDictionary *info = arr[i];
        
        // remove liked objc
        for(NSDictionary *pending in arrPending)
        {
            if([pending[@"id_fanpage"] isEqualToString:info[@"id_fanpage"]])
            {
                if([pending[@"score"] intValue] == 0)
                {
                    [arr removeObject:info];
                    i--;
                }
            }
        }
    }
    
    return arr;
    
}

-(NSArray *)getScoreShareList
{
    NSArray *arrList = [HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"list_share_bonus"];
    
    NSMutableArray *arr = [[NSMutableArray alloc] initWithArray:arrList];
    
    for(int i =0; i<arr.count;i++)
    {
        NSDictionary *info = arr[i];
        
        if([[NSUserDefaults standardUserDefaults] boolForKey:[@"com.hdv.shareApp." stringByAppendingString:info[@"id"]]])
        {
            [arr removeObject:info];
            i--;
        }
    }
    
    return arr;
}

-(NSArray *)getScoreRateList
{
    NSArray *arrRate = [HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"list_rate_bonus"];
    NSMutableArray *arrCanRate = [[NSMutableArray alloc] init];
    
    for(NSDictionary *rate in arrRate)
    {
        NSString *key = [NSString stringWithFormat:@"com.hdv.rated.%@",rate[@"id"]];
        if([[NSUserDefaults standardUserDefaults] boolForKey:key])
        {
            continue;
        }
        
        [arrCanRate addObject:rate];
    }
    
    return arrCanRate;
}

#pragma mark - time thumbnail
-(void) saveTimeCloseThumbnail:(NSInteger)time
{
    [HDVLocalData saveInteger:time forKey:eLocalFileKeyTimeCloseThumbnail appendKey:@""];
}

-(NSInteger) getTimeCloseThumbnail
{
    return [HDVLocalData getIntegerForKey:eLocalFileKeyTimeCloseThumbnail appendKey:@""];
}

#pragma mark - Get list ads
-(NSArray *)getListAds
{
    NSMutableArray *arrCanShow = [[NSMutableArray alloc] init];
    for(NSDictionary *ads in [HDVAppControll sharedInstance].dictAppControll[@"list_ads"])
    {
        NSString *url1 = [NSString stringWithFormat:@"%@://",ads[@"url_scheme"]];
        NSString *url2 = [NSString stringWithFormat:@"://%@",ads[@"url_scheme"]];
        
        if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url1]]||[[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url2]])
        {
            continue;
        }
        
        [arrCanShow addObject:ads];
    }
    
    return arrCanShow;
}


#pragma mark - HDVAds

-(HDVAdsView *)createHDVAdsBaner{
    if(_arrayListAds.count == 0){
        return nil;
    }
    NSDictionary * dict = [_arrayListAds objectAtIndex:arc4random()%_arrayListAds.count];
    
    return [[HDVAdsView alloc] initHDVBanner:dict];
}

-(HDVAdsView *)createHDVAdsThumbmail{
    if(_arrayListAds.count == 0){
        return nil;
    }
    NSDictionary * dict = [_arrayListAds objectAtIndex:arc4random()%_arrayListAds.count];
    HDVAdsView * ads = [[HDVAdsView alloc] initHDVThumbnail:dict];
    __weak HDVAdsView * adsb = ads;
    ads.didClickClose = ^{
        [adsb removeFromSuperview];
    };
    return ads;
}

-(HDVAdsPopup *)createHDVAdsPopup{
    if(_arrayListAds.count == 0){
        return nil;
    }
    NSDictionary * dict = [_arrayListAds objectAtIndex:arc4random()%_arrayListAds.count];
    HDVAdsPopup * ads =[[HDVAdsPopup alloc] initHDVPopup:dict];
    ads.delegate = self;
    
    return ads;
}



#pragma mark -Load GAD.
-(GADInterstitial *)createAdsGADPopup{
    
    GADInterstitial * popup = [[GADInterstitial alloc] initWithAdUnitID:[self getAdsIdWith:eAdsTypeGadPopup]];
    popup.delegate = self;
    @try {
        dispatch_async(dispatch_get_main_queue(), ^{
            [popup loadRequest:[GADRequest request]];
        });
    }
    @catch (NSException *exception) {
        NSLog(@"GAD POPUP EXCEPTION:%@",exception);
    }
    @finally {
        
    }
    return popup;
}

-(GADBannerView *)createAdsGADBanner{
    GADBannerView * banner = [[GADBannerView alloc] initWithAdSize:GADAdSizeFullWidthPortraitWithHeight(kAdHeigh) origin:CGPointMake(0, 0)];
    banner.adUnitID = [self getAdsIdWith:eAdsTypeGadBanner];
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    banner.rootViewController = app.window.rootViewController;
    banner.delegate = self;
    @try {
        dispatch_async(dispatch_get_main_queue(), ^{
            [banner loadRequest:[GADRequest request]];
        });

    }
    @catch (NSException *exception) {
        NSLog(@"GAD BANNER EXCEPTION:%@",exception);
    }
    @finally {

    }
    return banner;
}

-(GADBannerView *)createAdsGADThumbnail{
    float height = kWinsize.width * 9/16;
    GADBannerView * thumbnail = [[GADBannerView alloc] initWithAdSize:GADAdSizeFullWidthPortraitWithHeight(height) origin:CGPointMake(0, 0)];
    thumbnail.adUnitID = [self getAdsIdWith:eAdsTypeGadBanner];
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    thumbnail.rootViewController = app.window.rootViewController;
    thumbnail.delegate = self;
    @try {
        dispatch_async(dispatch_get_main_queue(), ^{
            [thumbnail loadRequest:[GADRequest request]];
        });
    }
    @catch (NSException *exception) {
         NSLog(@"GAD THUMBNAIL EXCEPTION:%@",exception);
    }
    @finally {
        
    }
    
    return thumbnail;
}



#pragma mark - Load STA.
-(STABannerView *)createAdsSTABanner{
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    STABannerView *banner = [[STABannerView alloc] initWithSize:STA_AutoAdSize autoOrigin:STAAdOrigin_Top withView:app.window withDelegate:self];
    return banner;
}

-(STAStartAppAd *)createAdsSTAPopup{
    STAStartAppAd * popup = [[STAStartAppAd alloc] init];
    @try {
        dispatch_async(dispatch_get_main_queue(), ^{
            [popup loadAd:STAAdType_Automatic withDelegate:self];
        });
    }
    @catch (NSException *exception) {
        NSLog(@"STA POPUP EXCEPTION:%@",exception);
    }
    @finally {
        
    }
    
    return popup;
}

#pragma mark - Load Face.

-(FBAdView *)createAdsFaceBanner{
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    BOOL isIPAD = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad);
    FBAdSize adSize = isIPAD ? kFBAdSizeHeight90Banner : kFBAdSizeHeight50Banner;
    FBAdView * banner = [[FBAdView alloc] initWithPlacementID:[self getAdsIdWith:eAdsTypeFaceBanner]
                                                       adSize:adSize
                                           rootViewController:app.window.rootViewController];
    banner.delegate = self;
    @try {
        dispatch_async(dispatch_get_main_queue(), ^{
             [banner loadAd];
        });
    }
    @catch (NSException *exception) {
        NSLog(@"FACE BANNER EXCEPTION:%@",exception);
    }
    @finally {
        
    }

   
    banner.frame = CGRectMake(0, 0, kWinsize.width, kAdHeigh);
    return banner;
}

-(FBInterstitialAd *)createAdsFacePopup{
    FBInterstitialAd * popup = [[FBInterstitialAd alloc] initWithPlacementID:[self getAdsIdWith:eAdsTypeFacePopup]];
    popup.delegate = self;
    @try {
        dispatch_async(dispatch_get_main_queue(), ^{
            [popup loadAd];
        });
    }
    @catch (NSException *exception) {
        NSLog(@"FACE POPUP EXCEPTION:%@",exception);
    }
    @finally {
        
    }
    
    return popup;
}

-(FBAdView *)createAdsFaceThumbnail{
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    BOOL isIPAD = ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad);
    if(isIPAD)
        return nil;
    FBAdSize adSize = isIPAD?[FBAdCustomSize customSize:CGSizeMake(-1,kWinsize.width * 9/16)]:kFBAdSizeHeight250Rectangle;
    FBAdView * thumbnail = [[FBAdView alloc] initWithPlacementID:[self getAdsIdWith:eAdsTypeFaceThumbnai]
                                                          adSize:adSize
                                              rootViewController:app.window.rootViewController];

    thumbnail.hidden = NO;
    thumbnail.delegate = self;
    @try {
        dispatch_async(dispatch_get_main_queue(), ^{
            [thumbnail loadAd];
        });
    }
    @catch (NSException *exception) {
        NSLog(@"FACE THUMBNAIL EXCEPTION:%@",exception);
    }
    @finally {
        
    }
    
    thumbnail.frame = CGRectMake(0, 0, kWinsize.width, kWinsize.width * 9/16);
    return thumbnail;
    
}

#pragma mark - Load Mobile Core

-(BOOL)enableMobileCore{
    return [HDVAppControll sharedInstance].dictAppControll[@"status_mobilecore"]?[[HDVAppControll sharedInstance].dictAppControll[@"status_mobilecore"] boolValue]:NO;
}

-(void)loadAdsMobileCore{
    if([self enableMobileCore]){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [MobileCore initWithToken:[self getAdsIdWith:eAdsTypeMobileCore]
                             logLevel:DEBUG_LOG_LEVEL
                              adUnits:@[[NSNumber numberWithInt:AD_UNIT_ALL_UNITS]]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [MobileCore sharedInstance].adUnitEventsDelegate= self;
                _mobileCoreStamp = [[NSDate date] timeIntervalSince1970];
                [HDVLocalData saveInteger:_mobileCoreStamp forKey:eLocalFileKeyMobileCoreStamp appendKey:@""];
                
            });
        });
    }
}

-(void)showAdsMobileCore{
    if([self enableMobileCore]){
        NSInteger now = [[NSDate date] timeIntervalSince1970];
        _mobileCoreStamp = [HDVLocalData getIntegerForKey:eLocalFileKeyMobileCoreStamp appendKey:@""];
        if (now - _mobileCoreStamp < [self getTimeOffsetShowMobileCore]) {
            return;
        }
        
        [HDVLocalData saveInteger:_mobileCoreStamp forKey:eLocalFileKeyMobileCoreStamp appendKey:@""];
        if ([MobileCore isStickeeReady]) {
            [MobileCore setStickeezPosition:BOTTOM_LEFT];
            AppDelegate * app = [UIApplication sharedApplication].delegate;
            [MobileCore showStickeeFromViewController:app.window.rootViewController];
        }
    }
}

#pragma mark - AdsConody
-(void)loadAdsColony{
    NSString *appId = [self getAdsIdWith:eAdsTypeCodonyAppId]?:kAdsColonyAppID;
    NSString *zoneId = [self getAdsIdWith:eAdsTypeCodonyZoneId]?:kAdsColonyScoreZoneID;
    [AdColony configureWithAppID:appId zoneIDs:@[zoneId] delegate:self logging:YES];
}

-(void) showScoreAdcolony
{
    NSString *zId = [self getAdsIdWith:eAdsTypeCodonyAppId]?:kAdsColonyScoreZoneID;
    [AdColony playVideoAdForZone:zId withDelegate:nil];
}


#pragma mark - MobileCoreDelegate
-(void)didReceiveEvent:(MCAdUnitEventType)event fromAdUnit:(MCAdUnits)adUnit{
    if(event == AD_UNIT_DISMISSED){
        _mobileCoreStamp = [[NSDate date] timeIntervalSince1970];
        [HDVLocalData saveInteger:_mobileCoreStamp forKey:eLocalFileKeyMobileCoreStamp appendKey:@""];
        [MobileCore hideStickee];
    }
    if(event == AD_UNIT_READY){
        
    }
}

#pragma mark - time click download
-(NSInteger)getTimeClickDownloadAds{
    return [HDVLocalData getIntegerForKey:eLocalFileKeyDownloadAdStamp appendKey:@""];
}

-(void)saveTimeClickDownloadAds{
    [HDVLocalData saveInteger:[[NSDate date] timeIntervalSince1970] forKey:eLocalFileKeyDownloadAdStamp appendKey:@""];
}


#pragma mark - STA Popup Delegate
-(void)didLoadAd:(STAAbstractAd *)ad
{
    if([ad isEqual:_staPopup])
    {
        _havePopup = YES;
    }
}

-(void)failedLoadAd:(STAAbstractAd *)ad withError:(NSError *)error
{
    if([ad isEqual:_staPopup]){

    }
}

-(void) didCloseAd:(STAAbstractAd *)ad
{
    if([ad isEqual:_staPopup])
    {
        _havePopup = NO;
        _staPopup = [self createAdsSTAPopup];
        if(!_haveClickAds){
            if(_typePopup == eAdsTypePopupScore){
                [UIAlertView showAlertViewWithMessage:TextForKey(@"HaventClickScoreAds") cancelButtonTitle:TextForKey(@"Close")];
            }
            if(_typePopup == eAdsTypePopupDownload){
                [UIAlertView showAlertViewWithMessage:TextForKey(@"HaventClickDownloadAds") cancelButtonTitle:TextForKey(@"Close")];
            }
            if(_typePopup == eAdsTypePopupDownload2){
                [UIAlertView showAlertViewWithMessage:TextForKey(@"HaventClickDownloadAds") cancelButtonTitle:TextForKey(@"Close")];
            }
        }
    }
}

-(void) didClickAd:(STAAbstractAd *)ad
{
    if([ad isEqual: _staPopup]){
        if(_typePopup == eAdsTypePopupScore){
            [self clickGetScore];
        }
        if(_typePopup == eAdsTypePopupDownload){
            [self clickGetDownload];
        }
        if(_typePopup == eAdsTypePopupDownload2){
            [self clickGetDownload];
        }
        _popupStamp += [self getTimeHideAds];
        [self saveTimePopupStamp:_popupStamp];
        [self didCloseAd:_staPopup];
        _leaveAppWhenClick = YES;
    }
}

#pragma mark - STA Baner Delegate
-(void)didDisplayBannerAd:(STABannerView *)banner
{
    if([banner isEqual:_staBanner])
    {
        _haveBanner = YES;
        
    }
    if([banner isEqual:_staBannerBottom]){
        _haveBannerBottom = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:kHDVAdsNotifyHaveBanner object:nil userInfo:@{@"view":banner}];
    }
    if([banner isEqual:_staBannerForAudio]){
        _haveBannerForAudio = YES;
    }
}

-(void)failedLoadBannerAd:(STABannerView *)banner withError:(NSError *)error
{
    if([banner isEqual:_staBanner])
    {
    }
    if([banner isEqual:_staBannerBottom]){
    }
    if([banner isEqual:_staBannerForAudio]){
    }
}

-(void) didClickBannerAd:(STABannerView *)banner
{
    _timeClickBanner = [[NSDate date] timeIntervalSince1970];
    [HDVLocalData saveInteger:_timeClickBanner forKey:eLocalFileKeyTimeClickBanner appendKey:@""];
    [[NSNotificationCenter defaultCenter] postNotificationName:kHDVAdsNotifyClickBanner object:nil userInfo:nil];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)([self getTimeHideAds] * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kHDVAdsNotifyHaveBanner object:nil userInfo:@{@"view":banner}];
    });
}

#pragma mark - GAD Popup Delegate

-(void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error
{
    if([ad isEqual:_gadPopup]){
    }
}
-(void)interstitialDidReceiveAd:(GADInterstitial *)ad
{
    if([ad isEqual:_gadPopup]){
        _havePopup = YES;
    }
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad
{
    if([ad isEqual:_gadPopup])
    {
        _popupStamp = [[NSDate date] timeIntervalSince1970];
        [self saveTimePopupStamp:_popupStamp];
        _havePopup = NO;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            _gadPopup = [self createAdsGADPopup];
        });
        if(!_haveClickAds){
            if(_typePopup == eAdsTypePopupScore){
                [UIAlertView showAlertViewWithMessage:TextForKey(@"HaventClickScoreAds") cancelButtonTitle:TextForKey(@"Close")];
            }
            if(_typePopup == eAdsTypePopupDownload){
                [UIAlertView showAlertViewWithMessage:TextForKey(@"HaventClickDownloadAds") cancelButtonTitle:TextForKey(@"Close")];
            }
            if(_typePopup == eAdsTypePopupDownload2){
                [UIAlertView showAlertViewWithMessage:TextForKey(@"HaventClickDownloadAds") cancelButtonTitle:TextForKey(@"Close")];
            }
        }

    }
    
}

-(void) interstitialWillPresentScreen:(GADInterstitial *)ad
{
    
}

-(void) interstitialWillLeaveApplication:(GADInterstitial *)ad
{
    if([_gadPopup isEqual:ad]){
        if(_typePopup == eAdsTypePopupScore){
            [self clickGetScore];
        }else if(_typePopup == eAdsTypePopupDownload){
            [self clickGetDownload];
        }else if (_typePopup == eAdsTypePopupDownload2){
            [self clickGetDownload];
        }
        _popupStamp+=[self getTimeHideAds];
        [self saveTimePopupStamp:_popupStamp];
          _leaveAppWhenClick = YES;
    }
}



#pragma mark - GAD Banner Delegate

-(void) adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
//     NSLog(@"Network:%@",_gadBanner.adNetworkClassName);
    if([view isEqual:_gadBanner]){
    }
    if([view isEqual:_gadBannerBottom]){
    }
    if([view isEqual:_gadBannerForAudio]){

    }
    
}

-(void) adViewDidReceiveAd:(GADBannerView *)view
{
    if([view isEqual:_gadBanner])
    {
        _haveBanner = YES;
        
    }
    if([view isEqual:_gadBannerBottom])
    {
        _haveBannerBottom = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:kHDVAdsNotifyHaveBanner object:nil userInfo:@{@"view":view}];
    }
    if([view isEqual:_gadBannerForAudio])
    {
        _haveBannerForAudio = YES;
    }
    if([view isEqual:_gadThumbnail])
    {
        _haveThumbnail = YES;
        
    }
}

-(void)adViewWillLeaveApplication:(GADBannerView *)adView
{
    
    _timeClickBanner = [[NSDate date] timeIntervalSince1970];
    [HDVLocalData saveInteger:_timeClickBanner forKey:eLocalFileKeyTimeClickBanner appendKey:@""];
    [[NSNotificationCenter defaultCenter] postNotificationName:kHDVAdsNotifyClickBanner object:nil userInfo:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)([self getTimeHideAds] * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kHDVAdsNotifyHaveBanner object:nil userInfo:@{@"view":adView}];
    });
}

#pragma mark - FB Popup delegate

- (void)interstitialAdDidLoad:(FBInterstitialAd *)interstitialAd
{
    
    if([interstitialAd isEqual:_facePopup]){
        _havePopup = YES;
    }
}

- (void)interstitialAd:(FBInterstitialAd *)interstitialAd didFailWithError:(NSError *)error
{
    if([interstitialAd isEqual:_facePopup]){

    }
}

- (void)interstitialAdDidClick:(FBInterstitialAd *)interstitialAd
{
    if([_facePopup isEqual:interstitialAd]){
        if(_typePopup == eAdsTypePopupScore){
            [self clickGetScore];
        }
        else
            if(_typePopup == eAdsTypePopupDownload){
                [self clickGetDownload];
            }
            else
                if(_typePopup == eAdsTypePopupDownload2){
                    [self clickGetDownload];
                }
        _popupStamp+=[self getTimeHideAds];
        [self saveTimePopupStamp:_popupStamp];
    }
}

- (void)interstitialAdDidClose:(FBInterstitialAd *)interstitialAd
{
    NSLog(@"Interstitial closed.");
    
    if([_facePopup isEqual:interstitialAd]){
        _havePopup = NO;
        _facePopup = [self createAdsFacePopup];
        _popupStamp = [[NSDate date] timeIntervalSince1970];
        [self saveTimePopupStamp:_popupStamp];
        if(!_haveClickAds){
            if(_typePopup == eAdsTypePopupScore){
                [UIAlertView showAlertViewWithMessage:TextForKey(@"HaventClickScoreAds") cancelButtonTitle:TextForKey(@"Close")];
            }
            if(_typePopup == eAdsTypePopupDownload){
                [UIAlertView showAlertViewWithMessage:TextForKey(@"HaventClickDownloadAds") cancelButtonTitle:TextForKey(@"Close")];
            }
            if(_typePopup == eAdsTypePopupDownload2){
                [UIAlertView showAlertViewWithMessage:TextForKey(@"HaventClickDownloadAds") cancelButtonTitle:TextForKey(@"Close")];
            }
        }
    }
}

- (void)interstitialAdWillClose:(FBInterstitialAd *)interstitialAd
{
    NSLog(@"Interstitial will close.");
    if([_facePopup isEqual:interstitialAd])
    {
        
    }
}


#pragma mark - FBView Delegate
- (void)adViewDidLoad:(FBAdView *)adView{
    if([_faceBanner isEqual:adView]){
        _haveBanner = YES;
    }
    else if ([_faceBannerForAudio isEqual:adView]){
        _haveBannerForAudio = YES;
    }
    else if ([_faceBannerBottom isEqual:adView]){
        _haveBannerBottom = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:kHDVAdsNotifyHaveBanner object:nil userInfo:@{@"view":adView}];
    }
    else if ([_faceThumbnail isEqual:adView]){
        _haveThumbnail = YES;
    }
}
- (void)adView:(FBAdView *)adView didFailWithError:(NSError *)error{
    if([_faceBanner isEqual:adView]){
    }
    else if ([_faceBannerForAudio isEqual:adView]){
    }
    else if ([_faceThumbnail isEqual:adView]){
    }
}


#pragma mark - HDVAds Popup Delegate
-(void)hdvAdsPopupDidClick:(HDVAdsPopup *)adsPopup{
    if([adsPopup isEqual:_hdvPopup]){
        if(_typePopup == eAdsTypePopupScore){
            [self clickGetScore];
        }
        else if (_typePopup == eAdsTypePopupDownload){
            [self clickGetDownload];
        }
        else if (_typePopup == eAdsTypePopupDownload2){
             [self clickGetDownload];
        }
        _popupStamp+=[self getTimeHideAds];
        [self saveTimePopupStamp:_popupStamp];
        _leaveAppWhenClick = YES;
    }
    
}

-(void)hdvAdsPopupDidClose:(HDVAdsPopup *)adsPopup{
    if([adsPopup isEqual:_hdvPopup]){
        _hdvPopup = [self createHDVAdsPopup];
        
        if(!_haveClickAds)
        {
            if (_typePopup == eAdsTypePopupScore) {
                [UIAlertView showAlertViewWithMessage:TextForKey(@"HaventClickScoreAds") cancelButtonTitle:TextForKey(@"Close")];
            }
            if (_typePopup == eAdsTypePopupDownload) {
                [UIAlertView showAlertViewWithMessage:TextForKey(@"HaventClickDownloadAds") cancelButtonTitle:TextForKey(@"Close")];
            }
            if (_typePopup == eAdsTypePopupDownload2) {
                [UIAlertView showAlertViewWithMessage:TextForKey(@"HaventClickDownloadAds") cancelButtonTitle:TextForKey(@"Close")];
            }
        }
    }
    
}

#pragma mark - Show Ads
-(BOOL)bannerIsReady{
    if([HDVLocalSetting getDisableShowAd])
    {
        return NO;
    }
    if(_haveBanner || _hdvBanner.imgView.image != nil)
    {
        return YES;
    }
    return NO;
}

-(BOOL)bannerBottomIsReady{
    if([HDVLocalSetting getDisableShowAd])
    {
        return NO;
    }
    if(_haveBannerBottom || _hdvBannerBottom.imgView.image != nil)
    {
        return YES;
    }
    return NO;
}


-(BOOL)bannerAudioIsReady{
    if([HDVLocalSetting getDisableShowAd])
    {
        return NO;
    }
    if(_haveBannerForAudio || _hdvBannerForAudio.imgView.image != nil)
    {
        return YES;
    }
    return NO;
}
-(BOOL)thumbnailIsReady{
    if([HDVLocalSetting getDisableShowAd])
    {
        return NO;
    }
    if(_haveThumbnail || _hdvThumbnail.imgView.image != nil)
    {
        return YES;
    }
    return NO;
}

-(BOOL)popupIsReady{
    if([HDVLocalSetting getDisableShowAd])
    {
        return NO;
    }
    if(_havePopup || _hdvPopup.imgView.image != nil)
    {
        return YES;
    }
    return NO;
}

-(BOOL)popupScoreIsReady{
    if([HDVLocalSetting getDisableShowAd])
    {
        return NO;
    }
    if(_havePopup || _hdvPopup.imgView.image != nil)
    {
        return YES;
    }
    return NO;
}


-(BOOL)popupDownloadIsReady{
    if([HDVLocalSetting getDisableShowAd])
    {
        return NO;
    }
    if(_havePopup || _hdvPopup.imgView.image != nil)
    {
        return YES;
    }
    return NO;
}

-(BOOL)popupDownload2IsReady{
    if([HDVLocalSetting getDisableShowAd])
    {
        return NO;
    }
    if(_havePopup || _hdvPopup.imgView.image != nil)
    {
        return YES;
    }
    return NO;
}

-(UIView *)getCurrentBanner{
    if(_haveBanner){
        NSString * typeBanner = [self getAdsBannerType];
        if([typeBanner isEqualToString:kHDVStringAdmob]){
            return _gadBanner;
        }else if ([typeBanner isEqualToString:kHDVStringFace]){
            return _faceBanner;
        }else if ([typeBanner isEqualToString:kHDVStringSTA]){
            return _staBanner;
        }
    }
    return _hdvBanner;
    
}

-(UIView *)getCurrentBannerBottom{
    
    if(_haveBannerBottom){
        NSString * typeBanner = [self getAdsBannerType];
        if([typeBanner isEqualToString:kHDVStringAdmob]){
            return _gadBannerBottom;
        }else if ([typeBanner isEqualToString:kHDVStringFace]){
            return _faceBannerBottom;
        }else if ([typeBanner isEqualToString:kHDVStringSTA]){
            return _staBannerBottom;
        }
    }
    return _hdvBannerBottom;
}

-(UIView *)getCurrentBannerForAudio{
    if(_haveBannerForAudio){
        NSString * typeBanner = [self getAdsBannerType];
        if([typeBanner isEqualToString:kHDVStringAdmob]){
            return _gadBannerForAudio;
        }else if ([typeBanner isEqualToString:kHDVStringFace]){
            return _faceBannerForAudio;
        }else if ([typeBanner isEqualToString:kHDVStringSTA]){
            return _staBannerForAudio;
        }
        
    }
    return _hdvBannerForAudio;
    
}

-(UIView *)getCurrentThumbnai{
    if(_haveThumbnail){
        NSString * typeThumb = [self getAdsThumbType];
        if ([typeThumb isEqualToString:kHDVStringFace]){
            if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
                return _faceThumbnail;
            }else{
                return _gadThumbnail;
            }
        }
    }
    return _hdvThumbnail;
}

-(void)showPopup{
    
    [self saveTimePopupStamp:_popupStamp];
    if(_havePopup){
        NSString *adNetwork = [self getAdsPopupType];
        @try {
            if([adNetwork isEqualToString:kHDVStringSTA])
            {
                
                _staPopup = [[STAStartAppAd alloc] init];
                [_staPopup loadAd:STAAdType_Automatic withDelegate:self];
            }
            
            if([adNetwork isEqualToString:kHDVStringAdmob])
            {
                AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
                [_gadPopup presentFromRootViewController:app.window.rootViewController];
                
            }
            
            if([adNetwork isEqualToString:kHDVStringFace])
            {
                AppDelegate * app = [UIApplication sharedApplication].delegate;
                [_facePopup showAdFromRootViewController:app.window.rootViewController];
            }
            
        }
        @catch (NSException *exception) {
            NSLog(@"EXCEPTION SHOWPOP:%@",exception);
        }
        @finally {
            
        }
    }else{
        AppDelegate * app = [UIApplication sharedApplication].delegate;
        [_hdvPopup showOnViewController:app.window.rootViewController];
    }
    
}

-(void)showCurrentNormalPopup{
    int now = [[NSDate date] timeIntervalSince1970];
    if(now - _createStamp < [self getTimeStartShowAd])
    {
        return;
    }
    
    if(now - _popupStamp  < [self getTimeOffsetShowPopup])
    {
        return;
    }
    _popupStamp = now;
    _typePopup = eAdsTypePopupNomal;
    [self showPopup];
}

-(void)showCurrentScorePopup{
    _leaveAppWhenClick = NO;
    _haveClickAds = NO;
    _typePopup = eAdsTypePopupScore;
    [self showPopup];
}

-(void)showCurrentDownloadPopup{
    _leaveAppWhenClick = NO;
    _haveClickAds = NO;
    _typePopup = eAdsTypePopupDownload;
    [self showPopup];
}

-(void)showCurrentDownloadPopup2{
    _leaveAppWhenClick = NO;
    _haveClickAds = NO;
    _typePopup = eAdsTypePopupDownload2;
    [self showPopup];
}

#pragma mark - Times
-(NSInteger)getTimePopupStamp{
    return [HDVLocalData getIntegerForKey:eLocalFileKeyPopupTimeStamp appendKey:@""];
}

-(void)saveTimePopupStamp:(NSInteger)time{
    [HDVLocalData saveInteger:time forKey:eLocalFileKeyPopupTimeStamp appendKey:@""];
}

@end
