//
//  HDVDownloadManager.h
//  HDVAppControler
//
//  Created by ThaoVM on 9/7/15.
//  Copyright (c) 2015 ThaoVM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HDVDownloadManager : NSObject
@property(nonatomic, strong)NSDictionary * dicInfoDownload;
+(instancetype)sharedInstance;
-(void)setDicInfoDownloadData:(NSDictionary *)dicInfoDownload;
-(BOOL)allowDownloadLimit;
-(int)getCreateDownload;
-(int)getAddDownload;
-(int)getPreAddDownload;
-(NSString *)getDownloadMessage;
-(NSString *)getSongSourceName;
-(NSString *)getVideoSourceName;
-(BOOL) getAllowShowPurchaseVip;
-(NSString *)getUrlPurchaseVip;
-(void) saveDownloadLimit:(NSInteger)downloadCount;
-(NSInteger)getDownloadLimit;
-(void) saveDownloadedCount;
-(NSInteger) getDownloadedCount;
-(BOOL) haveInitDownloadLimit;

@end
