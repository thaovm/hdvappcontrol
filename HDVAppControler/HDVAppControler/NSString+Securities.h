//
//  NSString+Securities.h
//  PubFW
//
//  Created by Lai The Su on 1/19/13.
//  Copyright (c) 2013 LaiTheSu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Securities)
- (NSString*) stringSHA1; //:(NSString*)input;
- (NSString *) stringMD5; //:(NSString *) input;
@end
