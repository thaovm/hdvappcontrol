//
//  AddDownloadVC.m
//  Mp3Player
//
//  Created by doduong on 5/22/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "AddDownloadVC.h"

@interface AddDownloadVC ()
{
    NSInteger _stampClickAds;
}

@end

@implementation AddDownloadVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_btnMoreDownload setTitle:TextForKey(@"AddDownload") forState:UIControlStateNormal];
    _lblTitle.text = TextForKey(@"AddDownload");
    _lblInstruction.text = TextForKey(@"AdviseAddDownload");
    [self updateText];
    _stampClickAds = [[HDVAdsManager sharedInstance] getTimeClickDownloadAds];
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    self.view.alpha = 1.0;
    [self updateText];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.view.alpha = 0.0;
}

-(void) updateText
{
    _lblRemainDownload.text = [NSString stringWithFormat:@"%@: %ld",TextForKey(@"RemainDownload"),[[HDVDownloadManager sharedInstance] getDownloadLimit]];
    _lblTotalDownloaded.text = [NSString stringWithFormat:@"%@: %ld",TextForKey(@"TotalDownloaded"),[[HDVDownloadManager sharedInstance] getDownloadedCount]];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickMoreDownload:(id)sender
{
    NSString *msg = [NSString stringWithFormat:@"Bạn đồng ý click quảng cáo để nhận thêm %d lượt tải?",[[HDVDownloadManager sharedInstance] getAddDownload]];
    if(![[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
    {
        msg = [NSString stringWithFormat:@"Do you want click ads to get %d download(s)?",[[HDVDownloadManager sharedInstance] getAddDownload]];
    }
    
    NSInteger now = [[NSDate date] timeIntervalSince1970];
    
    if(now - _stampClickAds <0)
    {
        ;
    }
//    else if(now - _stampClickAds < [[HDVAppControlManager sharedInstance] getTimeoffsetShowScoreAd] || ![HDVAdsManager sharedInstance].haveDownloadPopup2)
//    {
//        
//        NSInteger remain = [[HDVAppControlManager sharedInstance] getTimeoffsetShowScoreAd] - (now - _stampClickAds);
//        remain = remain/60 + ((remain%60)?1:0);
//        NSString *msg = [[NSString alloc] initWithFormat:@"Hiện tại chưa có quảng cáo. Vui lòng thực hiện lại sau %@ phút. Xin cảm ơn!",(remain<1)?@"ít":@(remain)];
//        
//        if(![[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
//        {
//            msg = [[NSString alloc] initWithFormat:@"No ads available, please check back after %@ minute(s). Thank you!",(remain<1)?@"a":@(remain)];
//        }
//        
//        [UIAlertView showAlertViewWithMessage:msg cancelButtonTitle:TextForKey(@"Close")];
//        return;
//    }
    
    [HDVAdsManager sharedInstance].didClickDownloadPopup = ^{
        int  now2 = [[NSDate date] timeIntervalSince1970];
        _stampClickAds = now2;
        [[HDVAdsManager sharedInstance] saveTimeClickDownloadAds];
    };
    
    [UIAlertView showAlertViewWithMessage:msg cancelButtonTitle:TextForKey(@"No") otherButtonTitle:TextForKey(@"Yes") tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex == 1)
        {
            if([[HDVAdsManager sharedInstance] popupDownload2IsReady])
            {

                   [[HDVAdsManager sharedInstance] showCurrentDownloadPopup2];
                
            }
            else
            {
                [UIAlertView showAlertViewWithMessage:TextForKey(@"NoAds") cancelButtonTitle:TextForKey(@"Close")];
            }
        }
    }];
}

- (IBAction)clickClose:(id)sender
{
    _clickClose?_clickClose():0;
    
}
@end
