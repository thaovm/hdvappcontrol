//
//  AddDownloadVC.h
//  Mp3Player
//
//  Created by doduong on 5/22/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "BasePresentVC.h"

@interface AddDownloadVC : BasePresentVC
@property (weak, nonatomic) IBOutlet UILabel *lblTotalDownloaded;
@property (weak, nonatomic) IBOutlet UILabel *lblRemainDownload;
@property (weak, nonatomic) IBOutlet UIButton *btnMoreDownload;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblInstruction;
@property (strong, nonatomic) void(^clickClose)();
- (IBAction)clickMoreDownload:(id)sender;
- (IBAction)clickClose:(id)sender;
- (void) updateText;
@end
