//
//  OtherWebVC.h
//  tubemate
//
//  Created by doduong on 3/20/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherWebVC : UIViewController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UINavigationItem *itemMain;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *itemClose;


- (IBAction)clickClose:(id)sender;

-(void)loadUrl:(NSString *)url;

@end
