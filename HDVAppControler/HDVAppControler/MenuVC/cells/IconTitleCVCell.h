//
//  IconTitleCVCell.h
//  tubemate
//
//  Created by doduong on 2/9/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IconTitleCVCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgvIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgvIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *imgvBg;
@property (weak, nonatomic) IBOutlet UILabel *lblLine;
@property (weak, nonatomic) IBOutlet UILabel *lblNumber;
@property (strong, nonatomic) UIColor *colorDefault;
@property (weak, nonatomic) IBOutlet UISwitch *switcher;
@property (weak, nonatomic) IBOutlet UILabel *lbHeader;
@property (weak, nonatomic) IBOutlet UILabel *lbHighLight;
@property (strong, nonatomic) NSDictionary *dictInfo;
@property (strong, nonatomic)IBOutlet UIImageView *imageStatus;
@property (copy, nonatomic) void(^didChangeSwitch)(BOOL onOrOff);

-(void)setContent:(NSDictionary *)info;
- (IBAction)switchValueChanged:(id)sender;


@end
