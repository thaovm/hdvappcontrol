//
//  IconTitleCVCell.m
//  tubemate
//
//  Created by doduong on 2/9/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "IconTitleCVCell.h"

@implementation IconTitleCVCell

- (void)awakeFromNib
{
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
    self.lbHeader.hidden = YES;
    self.lbHighLight.hidden = YES;
}


-(void)setHighlighted:(BOOL)highlighted
{
    NSString *fId = _dictInfo[@"functionId"];
    if([fId hasPrefix:@"section"])
    {
        return;
    }
    
    if(!highlighted)
    {
        self.backgroundColor = _colorDefault?:[UIColor clearColor];
    }
    else
    {
        self.backgroundColor = kColor80;
    }
}

-(void)setSelected:(BOOL)selected
{
    NSString *fId = _dictInfo[@"functionId"];
    
    if([fId hasPrefix:@"section"])
    {
        
        return;
    }
    
    if(!selected)
    {
        self.backgroundColor = _colorDefault?:[UIColor clearColor];
        self.lbHeader.hidden = YES;
        self.lbHighLight.hidden = YES;
    }
    else
    {
        self.lbHeader.hidden = NO;
        self.lbHighLight.hidden = NO;
        self.backgroundColor = kColor80;
    }

}

-(void)setContent:(NSDictionary *)info
{
    _dictInfo = info;
    _lblTitle.text = TextForKey(info[@"title"]);
    _imgvIcon.image = nil;
    
    if([info[@"functionId"] isEqualToString:@"bonusApp"])
    {
        _lblTitle.text = [info[@"title"] stringByAppendingFormat:@" (+%@)",info[@"score"]];
    }
    
    if([info[@"icon"] isKindOfClass:[NSString class]])
    {
        if([info[@"icon"] hasPrefix:@"http://"] || [info[@"icon"] hasPrefix:@"https://"])
        {
            [_imgvIcon setImageWithURL:[NSURL URLWithString:info[@"icon"]] placeholderImage:[UIImage imageNamed:@"ic_menu_other"]];
        }
        else
        {
            _imgvIcon.image = [UIImage imageNamed:info[@"icon"]];
        }
    }
    
    if([info[@"childs"] count]>0)
    {
        _imgvIndicator.hidden = false;
        _imgvIndicator.transform = CGAffineTransformIdentity;
    }
    else
    {
        _imgvIndicator.hidden = true;
    }
    
    NSString *fId = _dictInfo[@"functionId"];
    if([fId isEqualToString:@"gameHot"] || [fId isEqualToString:@"videoHot"] || [fId isEqualToString:@"otherFunctions"])
    {
        _imgvBg.image = [UIImage imageNamed:@"ipad_nav_bg"];
    }
    else
    {
        _imgvBg.image = [UIImage imageNamed:@"Opition_bgr_104"];
    }
    
    if([fId isEqualToString:@"gameHot"] || [fId isEqualToString:@"videoHot"])
    {
        CGRect frame = _lblTitle.frame;
        frame.origin.x = 10;
        _lblTitle.frame = frame;
    }
    else
    {
        CGRect frame = _lblTitle.frame;
        frame.origin.x = 40;
        _lblTitle.frame = frame;
    }
    //COMMENTTEST
//    _switcher.on = ![HDVLocalSetting getSettingCellularOff];
}

- (IBAction)switchValueChanged:(id)sender
{
    [HDVLocalSetting saveSettingCellularOff:!_switcher.on];
}

@end








