//
//  ScoreCVCell.h
//  tubemate
//
//  Created by doduong on 3/5/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoreCVCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblScore;
@property (weak, nonatomic) IBOutlet UIView *viewScore;
@property (weak, nonatomic) IBOutlet UIImageView *imgvCoin;

-(void) setContent:(NSDictionary *)info;

@end
