//
//  ScoreCVCell.m
//  tubemate
//
//  Created by doduong on 3/5/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "ScoreCVCell.h"

@implementation ScoreCVCell

- (void)awakeFromNib {
    // Initialization code
    self.backgroundColor = [UIColor clearColor];
    
    _viewScore.layer.cornerRadius = 5;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateScore) name:kLSNotityChangeScore object:nil];
    
    _lblScore.font = kRobotoFontWithSize(50);
    _lblSubTitle.font = kRobotoFontWithSize(13);
    _lblTitle.font = kRobotoFontWithSize(16);
}

-(void) layoutSubviews
{
    [super layoutSubviews];
    
    _lblScore.font = kRobotoFontWithSize(50);
    _lblSubTitle.font = kRobotoFontWithSize(13);
    _lblTitle.font = kRobotoFontWithSize(16);
}

-(void) setContent:(NSDictionary *)info
{
    _imgvCoin.hidden = YES;
    if([[HDVAdsManager sharedInstance] popupScoreIsReady])
    {
        _imgvCoin.hidden = NO;
    }
    else if([HDVAdsManager sharedInstance].haveScoreAdcolony)
    {
        _imgvCoin.hidden = NO;
    }
    else if([[HDVAdsManager sharedInstance] getListBonusApps].count >0)
    {
        _imgvCoin.hidden = NO;
    }
    else if(![[NSUserDefaults standardUserDefaults] boolForKey:@"com.hdv.likefp"])
    {
        _imgvCoin.hidden = NO;
    }
    else if(![[NSUserDefaults standardUserDefaults] boolForKey:@"com.hdv.rate"])
    {
        _imgvCoin.hidden = NO;
    }
    else if(![[NSUserDefaults standardUserDefaults] boolForKey:@"com.hdv.sharefb"])
    {
        _imgvCoin.hidden = NO;
    }
    
    _lblTitle.text = TextForKey(@"BonusScore");
    
    NSString *msg = @"";
    NSInteger limit = [HDVLocalSetting getScoreLimitToPro];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *sLimit = [formatter stringFromNumber:@(limit)];
    
    if([[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
    {
        msg = [NSString stringWithFormat:@"Đủ %@ điểm - lên bản VIP, xoá quảng cáo!",sLimit];
    }
    else
    {
        msg = [NSString stringWithFormat:@"Collect %@ coins - go VIP, remove ads!",sLimit];
    }
    
    
    _lblSubTitle.text = msg;
    
    [self updateScore];
}

-(void) updateScore
{
    NSInteger score = [HDVLocalSetting getBonusScore];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSString *sScore = [formatter stringFromNumber:@(score)];
    
    _lblScore.text = [NSString stringWithFormat:@"%@",sScore];
}

-(void) blinkAnimation
{
    [UIView animateWithDuration:0.3 animations:^{
        _imgvCoin.alpha = 0.5;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            _imgvCoin.alpha = 1;
        } completion:^(BOOL finished) {
            [self blinkAnimation];
        }];
    }];
}

@end




