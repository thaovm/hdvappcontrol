//
//  FeedbackVC.h
//  TaiNhac
//
//  Created by doduong on 3/10/15.
//  Copyright (c) 2015 Hai Nguyen Minh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackVC : UIViewController <UITextViewDelegate>
{
    NSString *_reportType;
    NSString *_reportMessage;
}

@property (weak, nonatomic) IBOutlet UIButton *btnRecommen;
@property (weak, nonatomic) IBOutlet UIButton *btnReport;
@property (weak, nonatomic) IBOutlet UITextView *txtvContent;
- (IBAction)clickCancel:(id)sender;
- (IBAction)clickSend:(id)sender;
- (IBAction)clickRecommen:(id)sender;
- (IBAction)clickReport:(id)sender;

@end
