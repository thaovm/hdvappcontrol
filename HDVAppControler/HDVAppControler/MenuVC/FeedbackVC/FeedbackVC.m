//
//  FeedbackVC.m
//  TaiNhac
//
//  Created by doduong on 3/10/15.
//  Copyright (c) 2015 Hai Nguyen Minh. All rights reserved.
//

#import "FeedbackVC.h"

@interface FeedbackVC ()

@end

@implementation FeedbackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _reportMessage = @"";
    _reportType = @"1";
    
    _txtvContent.text = [NSString stringWithFormat:@"%@...",TextForKey(@"FeedbackPlaceholder")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)clickSend:(id)sender
{
    _reportMessage = _txtvContent.text;
    _reportMessage = [_reportMessage stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if([_reportMessage hasPrefix:TextForKey(@"FeedbackPlaceholder")])
    {
        [UIAlertView showAlertViewWithMessage:TextForKey(@"NeedFeedbackContent") cancelButtonTitle:TextForKey(@"Ok")];
        return;
    }
    
    if(_reportMessage.length < 20)
    {
        [UIAlertView showAlertViewWithMessage:TextForKey(@"NeedFeedbackContent") cancelButtonTitle:TextForKey(@"Ok")];
        return;
    }
    else
    {
//        [[TNClientNew sharedClient] sendFeedback:_reportMessage Type:_reportType];
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES WithText:TextForKey(@"ThankFeedback") DelayHide:2];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self clickCancel:nil];
    });
}

- (IBAction)clickRecommen:(id)sender
{
    [_btnRecommen setImage:[UIImage imageNamed:@"radio_select"] forState:0];
    [_btnReport setImage:[UIImage imageNamed:@"radio_unselect"] forState:0];
    _reportType = @"0";
}

- (IBAction)clickReport:(id)sender
{
    [_btnRecommen setImage:[UIImage imageNamed:@"radio_unselect"] forState:0];
    [_btnReport setImage:[UIImage imageNamed:@"radio_select"] forState:0];
    _reportType = @"1";
}

#pragma mark - UITextView

-(void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    _reportMessage = textView.text;
    _reportMessage = [_reportMessage stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if(_reportMessage.length == 0)
    {
        _txtvContent.text = [NSString stringWithFormat:@"%@...",TextForKey(@"FeedbackPlaceholder")];
        _txtvContent.textColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.05 alpha:1];
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if([_txtvContent.text isEqualToString:[NSString stringWithFormat:@"%@...",TextForKey(@"FeedbackPlaceholder")]])
    {
        _txtvContent.text = @"";
    }
    
    _txtvContent.textColor = [UIColor colorWithRed:0.05 green:0.05 blue:0.05 alpha:1];
}

@end
