//
//  OtherWebVC.m
//  tubemate
//
//  Created by doduong on 3/20/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "OtherWebVC.h"

@interface OtherWebVC ()

@end

@implementation OtherWebVC

- (void)viewDidLoad {
    
    self.view.frame = CGRectMake(0, 0, kWinsize.width, kWinsize.height);
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if(_itemMain.title.length == 0)
    {
        _itemMain.title = TextForKey(@"Help");
    }
    
    [_itemClose setTitle:TextForKey(@"Close")];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadUrl:(NSString *)url
{
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [_webView loadRequest:request];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES WithText:@"loading..." DelayHide:5];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickClose:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
  
@end
