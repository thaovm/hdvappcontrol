//
//  MenuVC.h
//  tubemate
//
//  Created by doduong on 2/9/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "BaseVC.h"
#import <MessageUI/MessageUI.h>

@interface MenuVC : BaseVC <MFMessageComposeViewControllerDelegate>
{
    BOOL _needHideGameHot;
    BOOL _needHideVideoHot;
    BOOL _showProductViewForRate;
    
    NSInteger _stampWatchAds;
    NSInteger _stampClickAds;
    NSInteger _stampViewPopupAds;
    
    NSDictionary *_dictScoreRate;
}

@property (strong, nonatomic) NSMutableArray *marrMenu;
@property (strong, nonatomic) NSString *titleKey;

@property (nonatomic) NSString *otherAppsString;
@property (nonatomic) BOOL didQuitApp;
@property (nonatomic) BOOL willLoadAdmob;

-(void) updateMenu;
-(void) selectMyMusic;

#pragma mark - score
-(void) clickBonusScore;
-(void) clickHelpScore;
-(void) clickInstallApp;
@end
