//
//  MenuVC.m
//  tubemate
//
//  Created by doduong on 2/9/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "MenuVC.h"
#import "IconTitleCVCell.h"
#import "ScoreCVCell.h"
#import <StoreKit/StoreKit.h>
#include "AFNetworkReachabilityManager.h"
#import "FeedbackVC.h"




#import "OtherWebVC.h"
#import "SelectCountryVC.h"
#import "AddDownloadVC.h"
#import "ObjectScoreApp.h"
#import "HDVAppControll.h"
@interface MenuVC () <SKStoreProductViewControllerDelegate,MFMailComposeViewControllerDelegate>{
    BOOL _selectHome;
    NSIndexPath *_indexHome;
    AddDownloadVC *_addDownloadVC;
}

@end

@implementation MenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_nav_menu"] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:kRobotoFontWithSize(18)}];

    UIImageView *v = [[UIImageView alloc] initWithFrame:self.view.bounds];
    v.image = [UIImage imageNamed:@"nav_black30"];
    self.cvMain.backgroundView = v;
    
    [self.cvMain registerNib:[UINib nibWithNibName:@"IconTitleCVCell" bundle:nil] forCellWithReuseIdentifier:@"IconTitleCVCell"];
    [self.cvMain registerNib:[UINib nibWithNibName:@"ScoreCVCell" bundle:nil] forCellWithReuseIdentifier:@"ScoreCVCell"];

    self.cvMain.backgroundColor = [UIColor clearColor];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    if(_marrMenu != nil)
    {
        [self setupNavItem];
    }
    
    _stampClickAds =  [[[NSUserDefaults standardUserDefaults] objectForKey:@"timeClickAds"] integerValue];
    _stampWatchAds =  [[[NSUserDefaults standardUserDefaults] objectForKey:@"timeWatchAds"] integerValue];
    _stampViewPopupAds = [[[NSUserDefaults standardUserDefaults] objectForKey:@"timeViewScorePopup"] integerValue];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAppControlNotify:) name:kHDVACM_Update object:nil];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(_addDownloadVC){
        [_addDownloadVC updateText];
    }
    
    
    if(_marrMenu == nil) // menu level 1 --> need load menu from server...
    {
        [self makeMenu];
    }
    
    NSNumber *value = [NSNumber numberWithInt:[UIDevice currentDevice].orientation];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _selectHome = NO;
    AppDelegate * app = [UIApplication sharedApplication].delegate;
    UIViewController * view = [app.navHome.viewControllers firstObject];
//    if([view isKindOfClass:[UserMusicVC class]] && app.navHome.viewControllers.count == 1){
//        _selectHome = YES;
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) selectMyMusic
{
    [self clickMyMusic];
}

-(void) updateMenu
{
    if(self.title.length == 0)
    {
        [self makeMenu];
    }
    
}

#pragma mark - implement BaseVC

-(void)layoutForAds
{
    return;
}

-(void) setupNavItem
{    
    if(_marrMenu!=nil)
    {
        UIButton *btnBack = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 12, 20)];
        [btnBack setBackgroundImage:[UIImage imageNamed:@"btn_nav_back"] forState:0];
        [btnBack addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
        
        btnBack.adjustsImageWhenHighlighted = NO;
        btnBack.adjustsImageWhenDisabled = NO;
        btnBack.showsTouchWhenHighlighted = YES;
        
        UIBarButtonItem *itemMenu = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = itemMenu;
    }
    else
    {
        UIImageView *v = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
        
//        if([[HDVLocalSetting getLanguage] isEqualToString:@"VN"])
//        {
            v.image = [UIImage imageNamed:@"ic_logo"];
//        }
//        else
//        {
//            v.image = [UIImage imageNamed:@"ic_logo_en"];
//        }
        
        v.contentMode = UIViewContentModeScaleAspectFit;
        
        UIBarButtonItem *itemLogo = [[UIBarButtonItem alloc] initWithCustomView:v];
        self.navigationItem.leftBarButtonItem = itemLogo;
    }
}

#pragma mark - UICollectioView Delegate + data source + layout

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _marrMenu.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    NSDictionary *info = _marrMenu[indexPath.row];
    NSString *fId = info[@"functionId"];
    
    if ([fId isEqualToString:@"bonusScore"]) {
    
        ScoreCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ScoreCVCell" forIndexPath:indexPath];
        [cell setContent:info];
  
        return cell;
    }
    
    IconTitleCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"IconTitleCVCell" forIndexPath:indexPath];
    
    [cell setContent:info];
    cell.imgvBg.image = [UIImage imageNamed:@"bg_menu"];
    cell.lblLine.hidden = YES;
    cell.imageStatus.image = nil;
    if([fId isEqualToString:@"hotMusic"]){
        [cell.imageStatus setImage:[UIImage imageNamed:@"hot_music"]];
        CGRect labelRect = [cell.lblTitle.text
                            boundingRectWithSize:cell.lblTitle.frame.size
                            options:NSStringDrawingUsesLineFragmentOrigin
                            attributes:@{
                                         NSFontAttributeName : [UIFont systemFontOfSize:14]
                                         }
                            context:nil];
        [cell.imageStatus setFrame:CGRectMake(cell.lblTitle.frame.origin.x + labelRect.size.width + 5,5, 20, 15)];
    }
    if([fId isEqualToString:@"musicForYou"]){
        [cell.imageStatus setImage:[UIImage imageNamed:@"new"]];
        [cell.imageStatus setFrame:CGRectMake(cell.frame.size.width - 2 * cell.imageStatus.frame.size.width, (cell.frame.size.height - cell.imageStatus.frame.size.height)/2, 25, 20)];
    }
    if([fId isEqualToString:@"myMusic"] && _selectHome){
        [cell setSelected:YES];
        _indexHome = indexPath;

    }
    
    // config indicator
    if([fId isEqualToString:@"gameHot"])
    {
        cell.imgvIndicator.hidden = NO;
        if(_needHideGameHot)
        {
            cell.imgvIndicator.transform = CGAffineTransformIdentity;
        }
        else
        {
            cell.imgvIndicator.transform = CGAffineTransformMakeRotation(M_PI_2);
        }
    }
    else if([fId isEqualToString:@"videoHot"])
    {
        cell.imgvIndicator.hidden = NO;
        if(_needHideVideoHot)
        {
            cell.imgvIndicator.transform = CGAffineTransformIdentity;
        }
        else
        {
            cell.imgvIndicator.transform = CGAffineTransformMakeRotation(M_PI_2);
        }
    }
    
    if([fId hasPrefix:@"section"])
    {
        cell.backgroundColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:0.8];
        CGRect frame = cell.lblTitle.frame;
        frame.origin.x = 20;
        cell.lblTitle.frame = frame;
        cell.lblLine.backgroundColor = [UIColor clearColor];
    }
    else
    {
        cell.backgroundColor = [UIColor clearColor];
        CGRect frame = cell.lblTitle.frame;
        frame.origin.x = 40;
        cell.lblTitle.frame = frame;
        cell.lblLine.backgroundColor = [UIColor colorWithRed:0.05 green:0.05 blue:0.05 alpha:0.3];
    }
    
    if([fId isEqualToString:@"settingCellular"])
    {
        cell.switcher.hidden = NO;
    }
    else
    {
        cell.switcher.hidden = YES;
    }
    
    cell.lblTitle.font = kRobotoFontWithSize(17);
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *info = _marrMenu[indexPath.row];
    NSString *fId = info[@"functionId"];
    
    if([fId hasPrefix:@"section"])
    {
        return CGSizeMake(kWinsize.width*0.85>350?350:kWinsize.width*0.85, 30);
    }
    
    if([fId isEqualToString:@"bonusScore"])
    {
        return CGSizeMake(kWinsize.width*0.85>350?350:kWinsize.width*0.85, 250);
    }
    
    return CGSizeMake(kWinsize.width*0.85>350?350:kWinsize.width*0.85, 50);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(_selectHome){
        _selectHome = NO;
        IconTitleCVCell * cell = (IconTitleCVCell *)[self.cvMain cellForItemAtIndexPath:_indexHome];
        [cell setSelected:NO];
    }
   
    
    NSDictionary *menu = _marrMenu[indexPath.row];
    NSString *fId = menu[@"functionId"];
    
    if([fId isEqualToString:@"bonusScore"])
    {
        [self clickBonusScore];
    }
    else if([fId isEqualToString:@"like_fanpage"])
    {
        [self gotoLikePage:menu];
    }
    else if([fId isEqualToString:@"share_facebook"])
    {
        [self gotoShareFB:menu];
    }
    else if([fId isEqualToString:@"rate"])
    {
        [self gotoScoreRate:menu];
    }
    else if([fId isEqualToString:@"clickScoreRate"])
    {
        [self clickScoreRate:menu];
    }
    else if([fId isEqualToString:@"rateApp"])
    {
        [self clickRateApp];
    }
    else if([fId isEqualToString:@"install_app"])
    {
        [self clickInstallApp];
    }
    else if([fId isEqualToString:@"click_ads"])
    {
        [self clickClickAds];
    }
    else if([fId isEqualToString:@"view_popup"])
    {
        [self clickWatchScorePopup];
    }
    else if([fId isEqualToString:@"view_ads"])
    {
        [self clickWatchAdcolony];
    }
    else if([fId isEqualToString:@"bonusApp"])
    {
        [self clickApp:menu];
    }
    else if([fId isEqualToString:@"likeScorePage"])
    {
        [self clickLikeFanpage:menu];
    }
    else if([fId isEqualToString:@"shareScoreApp"])
    {
        [self clickShareFB:menu];
    }
    else if([fId hasPrefix:@"http"]||[fId hasPrefix:@"https"])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fId]];
            });
        });
        
    }
    else if([fId isEqualToString:@"playing"])
    {
        [self clickMenuPlay];
    }
    else if([fId isEqualToString:@"myMusic"])
    {
        [self clickMyMusic];
    }
    else if([fId isEqualToString:@"musicForYou"])
    {
        [self clickMusicForYou];
    }
    else if([fId isEqualToString:@"hotMusic"])
    {
        [self clickHotMusic];
    }
    else if([fId isEqualToString:@"playlist"])
    {
        [self clickMyPlaylist];
    }
    else if([fId isEqualToString:@"home"])
    {
        [self clickHome];
    }
    else if([fId isEqualToString:@"hotAlbum"])
    {
        [self clickHotAlbum];
    }
    else if([fId isEqualToString:@"hotSong"])
    {
        [self clickHotSong];
    }
    else if([fId isEqualToString:@"hotVideo"])
    {
        [self clickHotVideo];
    }
    else if([fId isEqualToString:@"hotArtist"])
    {
        [self clickHotArtist];
    }
    else if([fId isEqualToString:@"topChart"])
    {
        [self clickTopChart];
    }
    else if([fId isEqualToString:@"shutdownTimer"])
    {
        [self clickShutdownTimer];
    }
    else if([fId isEqualToString:@"vip"])
    {
        [self clickVip];
    }
    else if([fId isEqualToString:@"feedback"])
    {
        [self clickFeedback];
    }
    else if([fId isEqualToString:@"exitApp"])
    {
        [self clickExitApp];
    }
    else if ([fId isEqualToString:@"otherFunctions"])
    {
        [self clickOtherFunction:menu];
    }
    else if ([fId isEqualToString:@"shareApp"])
    {
        [self clickShareApp];
    }
    else if ([fId isEqualToString:@"adminAnswer"])
    {
        [self clickAdminAnswer];
    }
    else if ([fId isEqualToString:@"otherApps"])
    {
        [self clickOtherApps];
    }
    else if([fId isEqualToString:@"installOtherApps"])
    {
        [self clickInstallOtherApps:menu];
    }
    else if([fId isEqualToString:@"setting"])
    {
        [self clickSetting:menu];
    }
    else if([fId isEqualToString:@"settingLanguage"])
    {
        [self clickSettingLanguage];
    }
    else if([fId isEqualToString:@"helpScore"])
    {
        [self clickHelpScore];
    }
    else if([fId isEqualToString:@"faq"])
    {
        [self clickFAQ];
    }
    else if([fId isEqualToString:@"upgradeVIP"])
    {
        [self clickUpgradeVIP];
    }
    else if ([fId isEqualToString:@"addDownload"])
    {
        [self clickAddDownload];
    }
}

#pragma mark - Mp3 nagivate method

-(void)clickAddDownload
{
//    if(![[HDVAdsManager sharedInstance] popupDownload2IsReady])
//    {
//        [[HDVAdsManager sharedInstance] prepareAdsForDownload2];
//    }
    
    _addDownloadVC = [[AddDownloadVC alloc] initWithNibName:@"AddDownloadVC" bundle:nil];
    AppDelegate * app = [UIApplication sharedApplication].delegate;
    [_addDownloadVC.view setFrame:CGRectMake(0, kWinsize.height, kWinsize.width, kWinsize.height)];
    [app.window.rootViewController.view addSubview:_addDownloadVC.view];
    [UIView animateWithDuration:0.3f animations:^{
        [_addDownloadVC.view setFrame:CGRectMake(0, 0, kWinsize.width, kWinsize.height)];
    }];

    _addDownloadVC.clickClose = ^{
        [UIView animateWithDuration:0.3f animations:^{
            [_addDownloadVC.view setFrame:CGRectMake(0, kWinsize.height, kWinsize.width, kWinsize.height)];
        }completion:^(BOOL finished) {
            [_addDownloadVC.view removeFromSuperview];
            _addDownloadVC = nil;
        }];
    };
    
}

-(void) clickFAQ
{
    NSString *url = @"http://mp3.hdvietpro.com/mp3_zing_new/faq/faq-vi.php";
    
    if(![[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
    {
        url = @"http://mp3.hdvietpro.com/mp3_zing_new/faq/faq-en.php";
    }
    
    OtherWebVC *owVC = [[OtherWebVC alloc] initWithNibName:@"OtherWebVC" bundle:nil];
    [self presentViewController:owVC animated:YES completion:^{
        [owVC loadUrl:url];
    }];
}

-(void) clickHelpScore
{
//    http://ios.hdvietpro.com/ios-manager/system-score/faq-vi.php
//    http://ios.hdvietpro.com/ios-manager/system-score/faq-en.php
    
    NSString *url = @"http://ios.hdvietpro.com/ios-manager/system-score/faq-vi.php";
    
    if(![[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
    {
       url = @"http://ios.hdvietpro.com/ios-manager/system-score/faq-en.php";
    }
    
    OtherWebVC *owVC = [[OtherWebVC alloc] initWithNibName:@"OtherWebVC" bundle:nil];
    [self presentViewController:owVC animated:YES completion:^{
        [owVC loadUrl:url];
    }];
}

-(void)clickOtherFunction:(NSDictionary *)item
{
    MenuVC *vc = [[MenuVC alloc] initWithNibName:@"BaseVC" bundle:nil];
    vc.title = TextForKey(@"OtherFunctions");
    vc.titleKey = @"OtherFunctions";

    NSMutableArray *marrItem = [[NSMutableArray alloc] init];
    
    for (NSDictionary *child in item[@"childs"])
    {
        if([child[@"functionId"] isEqualToString:@"addDownload"] && ([HDVLocalSetting getDisableShowAd] || ![[HDVDownloadManager sharedInstance] allowDownloadLimit]))
        {
            continue;
        }
        else if([child[@"functionId"] isEqualToString:@"vip"] && ![[HDVDownloadManager sharedInstance] getAllowShowPurchaseVip])
        {
            continue;
        }
        else if([child[@"functionId"] isEqualToString:@"vip"])
        {
#if IS_PRO
            continue;
#endif
        }
        
        
        [marrItem addObject:child];
    }
    
    vc.marrMenu = marrItem;
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void) clickMenuPlay
{
    ;
}

-(void)clickMyMusic
{
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [app.navHome popToRootViewControllerAnimated:NO];
    [app.sideMenu toggleLeftSideMenuCompletion:nil];
}

-(void)clickHotMusic{
//    HotMusicVC *vc = [[HotMusicVC alloc] initWithNibName:@"BaseVC" bundle:nil];
//    vc.type = TNClientObjectTypeSong;
//    vc.btLeftMenu = YES;
//    
//    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [app.sideMenu toggleLeftSideMenuCompletion:nil];
//    [app.navHome popToRootViewControllerAnimated:NO];
//    [app.navHome pushViewController:vc animated:NO];

    
    
}


-(void)clickMusicForYou{
//    MusicForYouVC * mus4youVC = [[MusicForYouVC alloc] initWithNibName:@"BaseVC" bundle:nil];
//    mus4youVC.btLeftMenu = YES;
//    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [app.sideMenu toggleLeftSideMenuCompletion:nil];
//    [app.navHome popToRootViewControllerAnimated:NO];
//    [app.navHome pushViewController:mus4youVC animated:NO];
}

-(void)clickMyPlaylist
{
//    LocalPlaylistVC *vc = [[LocalPlaylistVC alloc] initWithNibName:@"LocalPlaylistVC" bundle:nil];
//    vc.title = TextForKey(@"Playlists");
//    vc.btLeftMenu = YES;
//    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [app.navHome popToRootViewControllerAnimated:NO];
//    [app.navHome pushViewController:vc animated:NO];
//    [app.sideMenu toggleLeftSideMenuCompletion:nil];
    
    
//    [app.userMusicVC collectionView:app.userMusicVC.cvMain didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];
}

-(void) clickHome
{

}

-(void) clickHotAlbum
{
//    HotCategoryVC *hcVC = [[HotCategoryVC alloc] initWithNibName:@"BaseVC" bundle:nil];
//    hcVC.type = TNClientObjectTypeAlbum;
//    hcVC.btLeftMenu = YES;
//    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [app.navHome popToRootViewControllerAnimated:NO];
//    [app.navHome pushViewController:hcVC animated:NO];
//    [app.sideMenu toggleLeftSideMenuCompletion:nil];

}

-(void) clickHotSong
{
//    HotCategoryVC *hcVC = [[HotCategoryVC alloc] initWithNibName:@"BaseVC" bundle:nil];
//    hcVC.type = TNClientObjectTypeSong;
//    hcVC.btLeftMenu = YES;
//    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [app.navHome popToRootViewControllerAnimated:NO];
//    [app.navHome pushViewController:hcVC animated:NO];
//    [app.sideMenu toggleLeftSideMenuCompletion:nil];

}

-(void) clickHotVideo
{
//    HotCategoryVC *hcVC = [[HotCategoryVC alloc] initWithNibName:@"BaseVC" bundle:nil];
//    hcVC.type = TNClientObjectTypeVideo;
//    hcVC.btLeftMenu = YES;
//    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [app.navHome popToRootViewControllerAnimated:NO];
//    [app.navHome pushViewController:hcVC animated:NO];
//    [app.sideMenu toggleLeftSideMenuCompletion:nil];

}

-(void) clickHotArtist
{
//    OnlineArtistVC *oaVC = [[OnlineArtistVC alloc] initWithNibName:@"BaseVC" bundle:nil];
//    oaVC.btLeftMenu = YES;
//    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [app.navHome popToRootViewControllerAnimated:NO];
//    [app.navHome pushViewController:oaVC animated:NO];
//    [app.sideMenu toggleLeftSideMenuCompletion:nil];
}

-(void) clickTopChart
{
//    TopChartCategoryVC *tccVC = [[TopChartCategoryVC alloc] initWithNibName:@"BaseVC" bundle:nil];
//    tccVC.btLeftMenu = YES;
//    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [app.navHome popToRootViewControllerAnimated:NO];
//    [app.navHome pushViewController:tccVC animated:NO];
//    [app.sideMenu toggleLeftSideMenuCompletion:nil];
}

-(void) clickShutdownTimer
{
//    TNClockViewController *vc = [[TNClockViewController alloc] initWithNibName:@"TNClockViewController" bundle:nil];
//    
//    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [app.window.rootViewController presentViewController:vc animated:YES completion:nil];
//    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    [app showAlarmVC];
}

-(void) clickVip
{
    NSString *url = [NSString stringWithFormat:@"%@",[[HDVDownloadManager sharedInstance] getUrlPurchaseVip]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        });
    });
    
//    if(![HDVLocalSetting getDisableShowAd])
//    {
//        [[PurchaseManager sharedManager] presentPurchaseVcForItem:kPurchaseItemPro InVc:self];
//    }
//    else
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kAppName message:@"Bạn đã mua bản pro rồi!" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles: nil];
//        [alert show];
//    }
    
}

-(void) clickFeedback
{
    FeedbackVC *fvc = [[FeedbackVC alloc] initWithNibName:@"FeedbackVC" bundle:nil];
    [self presentViewController:fvc animated:YES completion:nil];
}

-(void)clickAdminAnswer
{
//    if(![AFNetworkReachabilityManager sharedManager].reachable)
//    {
//        [UIAlertView showAlertViewWithMessage:TextForKey(@"CheckInternet") cancelButtonTitle:TextForKey(@"Close")];
//        return;
//    }
//    
//    NSString *url = [NSString stringWithFormat:@"http://ios.hdvietpro.com/ios-manager/answer-feedback.php?deviceID=%@&code=%@",[HDVLocalSetting getAdId],kAppId];
//    
//    OtherWebVC *owVC = [[OtherWebVC alloc] initWithNibName:@"OtherWebVC" bundle:nil];
//    [owVC loadView];
//    owVC.itemMain.title = TextForKey(@"AdminResponse");
//    [owVC.itemClose setTitle:TextForKey(@"Close")];
//    [self presentViewController:owVC animated:YES completion:^{
//        [owVC loadUrl:url];
//    }];
    
}

-(void) clickExitApp
{
    
}


- (void)suspendAndQuitApp {
    UIApplication *app = [UIApplication sharedApplication];
    [app performSelector:@selector(suspend)];
}


-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if(result == MFMailComposeResultSent)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Cảm ơn bạn đã gửi thông báo lỗi cho chúng tôi. Chúng tôi sẽ sớm khắc phục và thông báo cho bạn ngay sau khi có phiên bản mới !" delegate:nil cancelButtonTitle:@"Đồng ý" otherButtonTitles:nil];
        [alert show];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Private Method

-(void)makeMenu
{
    NSArray *arrMenuDefault = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Menu" ofType:@"plist"]];
    
    _marrMenu = [[NSMutableArray alloc] init];
    
    
    for(int i = 0; i<arrMenuDefault.count ;i++) // load home, downloaded, downloading, web
    {
        
        // setup for review version
        if(![HDVLocalSetting getFullVersion])
        {
            if(i!= 2 && i!=3 && i!=4)
            {
                continue;
            }
        }
        
//        if(i == arrMenuDefault.count-1 && ([HDVLocalSetting getDisableShowAd] || ![[HDVAdsManager sharedInstance] getSystemScoreStatus]))
//        {
//            continue;
//        }
        
        NSDictionary *obj = @{
                              @"title":TextForKey(arrMenuDefault[i][@"title"]),
                              @"icon":arrMenuDefault[i][@"icon"],
                              @"functionId":arrMenuDefault[i][@"functionId"],
                              @"childs":arrMenuDefault[i][@"childs"]?:[NSArray new],
                              };
        
        [_marrMenu addObject:obj];
    }
    
    [self.cvMain reloadData];
    
}

-(void) clickBonusScore
{
    //COMMENTTEST
//    if(![[HDVAdsManager sharedInstance] popupScoreIsReady])
//    {
//        [[HDVAdsManager sharedInstance] prepareAdsForScore];
//    }
//    if(![HDVAdsManager sharedInstance].havePopup)
//    {
//        [[HDVAdsManager sharedInstance] loadViewPopup];
//    }
    
    
    
    if(![AFNetworkReachabilityManager sharedManager].reachable)
    {
        [UIAlertView showAlertViewWithMessage:TextForKey(@"NetworkError") cancelButtonTitle:TextForKey(@"Close")];
        return;
    }
    
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"acceptScore"])
    {
        [UIAlertView showAlertViewWithMessage:TextForKey(@"AskRegisterScore") cancelButtonTitle:TextForKey(@"No") otherButtonTitle:TextForKey(@"Ok") tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if(buttonIndex == 1)
            {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"acceptScore"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self showBonusMenu];
            }
        }];
        
        return;
    }
    else
    {
        //load list bonus score
        [self showBonusMenu];
    }
}

-(void) showBonusMenu
{
    //load list bonus score
    NSMutableArray *marr = [[NSMutableArray alloc] init];
    NSArray *arrBonus = [[HDVAdsManager sharedInstance] getListBonus];
    
    for(NSDictionary *function in arrBonus)
    {
        if([function[@"status"] intValue] != 1)
        {
            continue;
        }
        
        NSString *s = function[@"name"];
        
        if([s isEqualToString:@"like_fanpage"])
        {
            
            NSArray *arr = [[HDVAdsManager sharedInstance] getScorePageList];
            
            NSMutableArray *marrPage = [[NSMutableArray alloc] init];
            
            for(NSDictionary *info in arr)
            {
                NSDictionary *item = @{
                                       @"title":[info[@"name"] stringByAppendingFormat:@" (+%@)",info[@"score"]],
                                       @"icon":info[@"icon"],
                                       @"score":info[@"score"],
                                       @"link":info[@"link_fanpage"],
                                       @"id":info[@"id_fanpage"],
                                       @"functionId":@"likeScorePage",
                                       @"pageName":info[@"name"]?:@"untitled",
                                       };
                [marrPage addObject:item];
            }
            
            
            NSDictionary *item = @{
                                   @"title":TextForKey(@"LikeFanpage"),
                                   @"icon":@"icon_like1",
                                   @"functionId":s,
                                   @"childs":@[@"",@""],
                                   };
            
            [marr addObject:item];
            
        }
        else if([s isEqualToString:@"share_facebook"])
        {
            
            NSArray *arr = [[HDVAdsManager sharedInstance] getScoreShareList];
            
            NSMutableArray *marrShare = [[NSMutableArray alloc] init];
            
            for(NSDictionary *info in arr)
            {
                NSDictionary *item = @{
                                       @"title":[info[@"app_name"] stringByAppendingFormat:@" (+%@)",info[@"score"]],
                                       @"appName":info[@"app_name"],
                                       @"icon":info[@"icon"],
                                       @"score":info[@"score"],
                                       @"link":info[@"url_store"],
                                       @"id":info[@"id"],
                                       @"functionId":@"shareScoreApp",
                                       };
                [marrShare addObject:item];
            }
            
            NSDictionary *item = @{
                                   @"title":TextForKey(@"ShareFB"),
                                   @"icon":@"icon_shareFb1",
                                   @"functionId":s,
                                   @"childs":@[@"",@""],
                                   };
            
            [marr addObject:item];
        }
        else if([s isEqualToString:@"rate"])
        {
            if([[NSUserDefaults standardUserDefaults] boolForKey:@"com.hdv.rate"])
            {
                continue;
            }
            
            NSDictionary *item = @{
                                   @"title":TextForKey(@"RateApp"),
                                   @"icon":@"icon_rate1",
                                   @"functionId":s,
                                   @"childs":@[@"",@""],
                                   };
            
            [marr addObject:item];
        }
        else if([s isEqualToString:@"install_app"])
        {
            if([function[@"score"] intValue] != 1)
            {
                continue;
            }
            
            NSArray *arrApp = [[HDVAdsManager sharedInstance] getListBonusApps];
            if(arrApp.count == 0)
            {
                continue;
            }
            
            
            NSDictionary *item = @{
                                   @"title":TextForKey(@"InstallApps"),
                                   @"icon":@"icon_otherApp1",
                                   @"functionId":s,
                                   @"childs":@[@"",@""],
                                   };
            
            [marr addObject:item];
        }
        else if([s isEqualToString:@"click_ads"])
        {
            NSDictionary *item = @{
                                   @"title":[TextForKey(@"ClickAds") stringByAppendingFormat:@" (+%@)",function[@"score"]],
                                   @"icon":@"icon_clickAds1",
                                   @"functionId":s,
                                   @"childs":[NSArray new],
                                   };
            
            [marr addObject:item];
        }
        else if([s isEqualToString:@"view_ads"])
        {
            if(![HDVAdsManager sharedInstance].haveScoreAdcolony)
            {
                continue;
            }
            
            NSDictionary *item = @{
                                   @"title":[TextForKey(@"WatchVideo") stringByAppendingFormat:@" (+%@)",function[@"score"]],
                                   @"icon":@"icon_click_video",
                                   @"functionId":s,
                                   @"childs":[NSArray new],
                                   };
            
            [marr addObject:item];
        }
        else if([s isEqualToString:@"view_popup"])
        {
            NSDictionary *item = @{
                                   @"title":[TextForKey(@"ViewAds") stringByAppendingFormat:@" (+%@)",function[@"score"]],
                                   @"icon":@"icon_watchAds1",
                                   @"functionId":s,
                                   @"childs":[NSArray new],
                                   };
            
            [marr addObject:item];
        }
        
    }
    
    // add help menu and upgrade vip for score system
    NSDictionary *itemUpgradeVIP = @{
                                     @"title":TextForKey(@"UpgradeVIP"),
                                     @"icon":@"ic_vip255",
                                     @"functionId":@"upgradeVIP",
                                     @"childs":[NSArray new],
                                     };
    
    [marr addObject:itemUpgradeVIP];
    
    NSDictionary *itemHelp = @{
                               @"title":TextForKey(@"Help"),
                               @"icon":@"ic_help255",
                               @"functionId":@"helpScore",
                               @"childs":[NSArray new],
                               };
    
    [marr addObject:itemHelp];
    
    MenuVC *mvc = [[MenuVC alloc] initWithNibName:@"BaseVC" bundle:nil];
    mvc.marrMenu = marr;
    mvc.title = TextForKey(@"BonusScore");
    mvc.titleKey = @"BonusScore";
    [self.navigationController pushViewController:mvc animated:YES];
}


-(void) clickPurchasePro
{
    [UIAlertView showAlertViewWithMessage:@"Bạn muốn mua bản VIP không quảng cáo và sử dụng nhiều chức năng cao cấp hơn?" cancelButtonTitle:TextForKey(@"Cancel") otherButtonTitles:@[TextForKey(@"Ok")] tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex== 1) // OK
        {
//            PurchaseVC *vc = [[PurchaseVC alloc] initWithNibName:@"PurchaseVC" bundle:nil];
//            
//            [self presentViewController:vc animated:YES completion:nil];
        }
    }];
}

-(void) messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    if(result == MessageComposeResultSent)
    {
        [UIAlertView showAlertViewWithMessage:@"Cảm ơn bạn!\nỨng dụng sẽ tự động cập nhật lên bản VIP khi chúng tôi ghi nhận được thông báo từ nhà mạng!" cancelButtonTitle:TextForKey(@"Ok")];
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

-(void)suspendApp
{
    if([HDVLocalSetting getDisableShowAd])
    {
        UIApplication *app = [UIApplication sharedApplication];
        [app performSelector:@selector(suspend)];
        return;
    }
    
//    [[HDVAdsManager sharedInstance] showEndPopupResult:^(BOOL result) {
//        if(!result)
//        {
//            UIApplication *app = [UIApplication sharedApplication];
//            [app performSelector:@selector(suspend)];
//        }
//        
//    } DismissBlock:^{
//        UIApplication *app = [UIApplication sharedApplication];
//        [app performSelector:@selector(suspend)];
//    }];
}

- (void)clickGameHot
{
    
    NSArray *arrApps = [[HDVAdsManager sharedInstance] getListAds];
    if(arrApps.count>0)
    {
        NSDictionary *ads = arrApps[arc4random()%arrApps.count];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ads[@"store_url"]]];
            });
        });
        
    }
}

-(void) clickRateApp
{
    NSString *sRateUrl = [NSString stringWithFormat:@"https://itunes.apple.com/app/id%@",[[HDVAdsManager sharedInstance] getAppleStoreId]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sRateUrl]];
        });
    });
}

-(void)clickShareApp
{
    NSString *sRateUrl = [NSString stringWithFormat:@"https://itunes.apple.com/app/id%@",[[HDVAdsManager sharedInstance] getAppleStoreId]];
    [[FBManager sharedManager] shareWithName:kAppName caption:@"" description:@"" link:sRateUrl picture:sRateUrl success:^{
        ;
    } failed:^(NSString *reason) {
        ;
    }];
}

-(void) clickInstallApp
{
    NSArray *arrApp = [[HDVAdsManager sharedInstance] getListBonusApps];
    
    if(arrApp.count ==0)
    {
    }
    else
    {
//        HDVScoreAppVC *vc = [[HDVScoreAppVC alloc] initWithNibName:@"HDVBaseVC" bundle:nil];
//        
//        NSMutableArray *marrObj = [[NSMutableArray alloc] init];
//        for(NSDictionary *app in arrApp)
//        {
//            ObjectScoreApp *osa = [[ObjectScoreApp alloc] initWithHdvInfo:app];
//            if([osa appDidAwarded])
//            {
//                continue;
//            }
//            [marrObj addObject:osa];
//        }
//        
//        [vc setDataSoure:marrObj];
//        
//        AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//        [app.navHome popToRootViewControllerAnimated:NO];
//        [app.navHome pushViewController:vc animated:NO];
//        [app.sideMenu toggleLeftSideMenuCompletion:nil];
        
    }
}

-(void) clickWatchAdcolony
{
    NSInteger now = [[NSDate date] timeIntervalSince1970];
    if(now - _stampWatchAds < [[HDVAdsManager sharedInstance] getTimeoffsetShowScoreAd] || ![HDVAdsManager sharedInstance].haveScoreAdcolony)
    {
        NSInteger remain = [[HDVAdsManager sharedInstance] getTimeoffsetShowScoreAd] - (now - _stampWatchAds);
        remain = remain/60 + ((remain%60)?1:0);
        
        NSString *msg = [[NSString alloc] initWithFormat:@"Hiện tại chưa có quảng cáo. Vui lòng thực hiện lại sau %@ phút. Xin cảm ơn!",(remain<1)?@"ít":@(remain)];
        
        if(![[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
        {
            msg = [[NSString alloc] initWithFormat:@"No ads available, please check back after %@ minute(s). Thank you!",(remain<1)?@"a":@(remain)];
        }
        
        [UIAlertView showAlertViewWithMessage:msg cancelButtonTitle:TextForKey(@"Close")];
        return;
    }
    
    [[HDVAdsManager sharedInstance] showScoreAdcolony];
    _stampWatchAds = now;
    [[NSUserDefaults standardUserDefaults] setObject:@(now) forKey:@"timeWatchAds"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(20 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSInteger score = [HDVLocalSetting getBonusScore] + [[[HDVAdsManager sharedInstance] getAllListBonus][5][@"score"] integerValue];
        [HDVLocalSetting saveBonusScore:score];
        
        NSInteger more = [[[HDVAdsManager sharedInstance] getAllListBonus][5][@"score"] integerValue];
        if(score < [HDVLocalSetting getScoreLimitToPro])
        {
            NSString *msg = @"";
            if([[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
            {
                msg = [NSString stringWithFormat:@"Thưởng %ld điểm! Cảm ơn bạn đã xem video.",more];
            }
            else
            {
                msg = [NSString stringWithFormat:@"Earn %ld coins! Thank you!",more];
            }
            
            [UIAlertView showAlertViewWithMessage:msg cancelButtonTitle:TextForKey(@"Close")];
        }
    });
}

-(void) clickClickAds
{
    if([HDVLocalSetting getClickCountForCurrentDay] >= [[HDVAdsManager sharedInstance] getClickAdsLimit])
    {
        NSString *msg = [NSString stringWithFormat:@"Bạn chỉ được thực hiện chức năng này %ld lần trong 1 ngày. Xin mời quay lại vào ngày mai. Xin cảm ơn!",[[HDVAdsManager sharedInstance] getClickAdsLimit]];
        if(![[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
        {
             msg = [NSString stringWithFormat:@"You can click ads %ld time(s) per day.Please try later!",[[HDVAdsManager sharedInstance] getClickAdsLimit]];
        }
        
        [UIAlertView showAlertViewWithMessage:msg cancelButtonTitle:TextForKey(@"Close")];
        
        return;
    }

    
    NSInteger now = [[NSDate date] timeIntervalSince1970];
    
    if(now - _stampClickAds <0)
    {
        ;
    }
    else if(now - _stampClickAds < [[HDVAdsManager sharedInstance] getTimeoffsetShowScoreAd] || ![[HDVAdsManager sharedInstance] popupScoreIsReady])
    {
               
        NSInteger remain = [[HDVAdsManager sharedInstance] getTimeoffsetShowScoreAd] - (now - _stampClickAds);
        remain = remain/60 + ((remain%60)?1:0);
        NSString *msg = [[NSString alloc] initWithFormat:@"Hiện tại chưa có quảng cáo. Vui lòng thực hiện lại sau %@ phút. Xin cảm ơn!",(remain<1)?@"ít":@(remain)];
        
        if(![[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
        {
            msg = [[NSString alloc] initWithFormat:@"No ads available, please check back after %@ minute(s). Thank you!",(remain<1)?@"a":@(remain)];
        }
        
        [UIAlertView showAlertViewWithMessage:msg cancelButtonTitle:TextForKey(@"Close")];
        return;
    }
    
    
    [[HDVAdsManager sharedInstance] showCurrentScorePopup];
    
    [HDVAdsManager sharedInstance].didClickScorePopup = ^{
        int  now2 = [[NSDate date] timeIntervalSince1970];
        _stampClickAds = now2;
        [[NSUserDefaults standardUserDefaults] setObject:@(now2) forKey:@"timeClickAds"];
        [[NSUserDefaults standardUserDefaults] synchronize];

    };
}

-(void) clickWatchScorePopup
{
    NSInteger now = [[NSDate date] timeIntervalSince1970];
    
    if(now - _stampViewPopupAds <0)
    {
        ;
    }
    else if(now - _stampViewPopupAds < [[HDVAdsManager sharedInstance] getTimeoffsetShowScoreViewPoup] || ![HDVAdsManager sharedInstance].popupIsReady)
    {
        
        NSInteger remain = [[HDVAdsManager sharedInstance] getTimeoffsetShowScoreViewPoup] - (now - _stampViewPopupAds);
        remain = remain/60 + ((remain%60)?1:0);
        NSString *msg = [[NSString alloc] initWithFormat:@"Hiện tại chưa có quảng cáo. Vui lòng thực hiện lại sau %@ phút. Xin cảm ơn!",(remain<1)?@"ít":@(remain)];
        
        if(![[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
        {
            msg = [[NSString alloc] initWithFormat:@"No ads available, please check back after %@ minute(s). Thank you!",(remain<1)?@"a":@(remain)];
        }
        
        [UIAlertView showAlertViewWithMessage:msg cancelButtonTitle:TextForKey(@"Close")];
        return;
    }
    //COMMENTTEST
    [[HDVAdsManager sharedInstance] showCurrentScorePopup];
    
    [HDVAdsManager sharedInstance].doneWithViewPopup = ^{
        int  now2 = [[NSDate date] timeIntervalSince1970];
        _stampViewPopupAds = now2;
        [[NSUserDefaults standardUserDefaults] setObject:@(now2) forKey:@"timeViewScorePopup"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    };
    
}

-(void) gotoShareFB:(NSDictionary *)menu
{
    NSArray *arr = [[HDVAdsManager sharedInstance] getScoreShareList];
    NSMutableArray *marrShare = [[NSMutableArray alloc] init];
    
    for(NSDictionary *info in arr)
    {
        NSDictionary *item = @{
                               @"title":[info[@"app_name"] stringByAppendingFormat:@" (+%@)",info[@"score"]],
                               @"appName":info[@"app_name"],
                               @"icon":info[@"icon"],
                               @"score":info[@"score"],
                               @"link":info[@"url_store"],
                               @"id":info[@"id"],
                               @"functionId":@"shareScoreApp",
                               };
        [marrShare addObject:item];
    }
    
    MenuVC *mvc = [[MenuVC alloc] initWithNibName:@"BaseVC" bundle:nil];
    mvc.marrMenu = marrShare;
    mvc.title = menu[@"title"];
    mvc.titleKey = mvc.title;
    [self.navigationController pushViewController:mvc animated:YES];
}

-(void) gotoLikePage:(NSDictionary *)menu
{
    NSArray *arr = [[HDVAdsManager sharedInstance] getScorePageList];
    NSMutableArray *marrPage = [[NSMutableArray alloc] init];
    
    for(NSDictionary *info in arr)
    {
        NSDictionary *item = @{
                               @"title":[info[@"name"] stringByAppendingFormat:@" (+%@)",info[@"score"]],
                               @"icon":info[@"icon"],
                               @"score":info[@"score"],
                               @"link":info[@"link_fanpage"],
                               @"id":info[@"id_fanpage"],
                               @"functionId":@"likeScorePage",
                               @"pageName":info[@"name"],
                               };
        [marrPage addObject:item];
    }
    
    MenuVC *mvc = [[MenuVC alloc] initWithNibName:@"BaseVC" bundle:nil];
    mvc.marrMenu = marrPage;
    mvc.title = menu[@"title"];
    mvc.titleKey = mvc.title;
    [self.navigationController pushViewController:mvc animated:YES];
}

-(void) clickShareFB:(NSDictionary *)item
{
    
    NSString *appName = item[@"appName"];
    
    [[FBManager sharedManager] shareWithName:appName caption:@"Great App" description:@"" link:item[@"link"] picture:item[@"icon"] success:^{
        NSLog(@"%s: success",__FUNCTION__);
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[@"com.hdv.shareApp." stringByAppendingString:item[@"id"]]];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSArray *arrBonus = [[HDVAdsManager sharedInstance] getAllListBonus];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSInteger score = [HDVLocalSetting getBonusScore] + [item[@"score"] intValue];
        NSInteger more = [item[@"score"] intValue];
        [HDVLocalSetting saveBonusScore:score];
        
        NSDictionary *info = arrBonus[1];
        [[HDVAppControll sharedInstance] reportScoreFromEvent:[info[@"name"] stringByAppendingFormat:@" %@",item[@"appName"]] andScore:[item[@"score"] integerValue] Complete:nil];
        
        
        NSString *msg = @"";
        
        if([[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
        {
            msg = [NSString stringWithFormat:@"Thưởng %ld điểm!\nCảm ơn bạn đã chia sẻ ứng dụng!",more];
        }
        else
        {
            msg = [NSString stringWithFormat:@"Earn %ld coins!\nThanks you for sharing this app!",more];
        }
        
        [UIAlertView  showAlertViewWithMessage:msg cancelButtonTitle:TextForKey(@"Ok")];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    } failed:^(NSString *reason) {
        NSLog(@"%s fail: %@",__FUNCTION__,reason);
        
        [UIAlertView  showAlertViewWithMessage:TextForKey(@"CommonError") cancelButtonTitle:TextForKey(@"Ok")];
    }];
}

-(void) clickLikeFanpage:(NSDictionary *)item
{
    NSArray *arr = [[HDVAdsManager sharedInstance] getPendingAppList];
    for (NSDictionary *info in arr)
    {
        if([info[@"id_fanpage"] isEqualToString:item[@"id"]])
        {
            if([info[@"score"] intValue] == 0)
            {
                [UIAlertView showAlertViewWithMessage:TextForKey(@"HaveLikePage") cancelButtonTitle:TextForKey(@"Close")];
                
                return;
            }
        }
    }
    
    [HDVLocalSetting savePendingPage:item];
    
    NSString *fanpageId = item[@"id"];
    NSString *fanpageLink = item[@"link"];
    
    NSURL *facebookURL = [NSURL URLWithString:[NSString stringWithFormat:@"fb://profile/%@",fanpageId]];
    if ([[UIApplication sharedApplication] canOpenURL:facebookURL])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] openURL:facebookURL];
            });
        });
        
    }
    else
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fanpageLink]];
            });
        });
        
    }
    
}

// attempt to install bonus app
-(void) clickApp:(NSDictionary *)app
{
    NSString *message = app[@"message"];
    if(message.length>0)
    {
        [UIAlertView showAlertViewWithMessage:message cancelButtonTitle:TextForKey(@"Ok") tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:app[@"url_store"]]];
                });
            });
            
            [[HDVAdsManager sharedInstance] savePendingApp:app];
        }];
    }
    else
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:app[@"url_store"]]];
            });
        });
        
        [[HDVAdsManager sharedInstance] savePendingApp:app];
    }
}

-(void) clickOtherApps
{
    NSMutableArray *marr = [[NSMutableArray alloc] init];
    NSArray *arr = [[HDVAdsManager sharedInstance] getListAds];
    for(NSDictionary *info in arr)
    {
        NSDictionary *app = @{
                              @"functionId":@"installOtherApps",
                              @"link":info[@"store_url"]?:@"",
                              @"title":info[@"title"]?:@"",
                              @"icon":info[@"icon"]?:@"",
                              };
        [marr addObject:app];
    }
    
    MenuVC *vc = [[MenuVC alloc] initWithNibName:@"BaseVC" bundle:nil];
    vc.marrMenu = marr;
    vc.title = TextForKey(@"OtherApps");
    vc.titleKey = @"OtherApps";
    [self.navigationController pushViewController:vc animated:YES];
}

-(void) clickInstallOtherApps:(NSDictionary *)menu
{
    NSString *sUrl = menu[@"link"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sUrl]];
        });
    });
    
}

-(void) gotoScoreRate:(NSDictionary *)menu
{
    NSArray *arr = [[HDVAdsManager sharedInstance] getScoreRateList];
    NSMutableArray *marrRate = [[NSMutableArray alloc] init];
    
    for(NSDictionary *info in arr)
    {
        NSDictionary *item = @{
                               @"title":[info[@"app_name"] stringByAppendingFormat:@" (+%@)",info[@"score"]],
                               @"icon":info[@"icon"],
                               @"score":info[@"score"],
                               @"url_store":info[@"url_store"],
                               @"id":info[@"id"],
                               @"appId":info[@"appid"],
                               @"functionId":@"clickScoreRate",
                               @"appName":info[@"app_name"],
                               };
        [marrRate addObject:item];
    }
    
    MenuVC *mvc = [[MenuVC alloc] initWithNibName:@"BaseVC" bundle:nil];
    mvc.marrMenu = marrRate;
    mvc.title = menu[@"title"];
    mvc.titleKey = mvc.title;
    [self.navigationController pushViewController:mvc animated:YES];
}

-(void) clickScoreRate:(NSDictionary *)item
{
    _dictScoreRate = [[NSDictionary alloc] initWithDictionary:item];
    
    if([item[@"message"] isKindOfClass:[NSString class]])
    {
        if([item[@"message"] length] > 0)
        {
            [UIAlertView showAlertViewWithMessage:item[@"message"] cancelButtonTitle:TextForKey(@"Ok") tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                [self clickRateApp:item[@"appId"]];
            }];
        }
    }
    else
    {
        [self clickRateApp:item[@"appId"]];
    }
    
}

-(void) clickRateApp:(NSString *)appId
{
    
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    
    NSString *url = [@"https://itunes.apple.com/app/id" stringByAppendingString:appId];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
        });
    });
    
    
    NSString *key = [NSString stringWithFormat:@"com.hdv.rated.%@",_dictScoreRate[@"id"]];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey: key];
    NSString *keyScore = @"currentRate";
    [[NSUserDefaults standardUserDefaults] setObject:_dictScoreRate forKey: keyScore];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:@([[NSDate date] timeIntervalSince1970]) forKey:@"rateTime"];
    [[NSUserDefaults standardUserDefaults] setObject:_dictScoreRate[@"id"] forKey:@"currentRateId"];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)clickSetting:(NSDictionary *)menu
{
    MenuVC *vc = [[MenuVC alloc] initWithNibName:@"BaseVC" bundle:nil];
    vc.marrMenu = menu[@"childs"];
    
#if US
    if([HDVLocalSetting getFullVersion])
    {
        vc.marrMenu = [NSMutableArray arrayWithArray:@[menu[@"childs"][1]]];
    }
    
#endif
    
    vc.title = TextForKey(@"Setting");
    vc.titleKey = @"Setting";
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)clickSettingLanguage
{
    SelectCountryVC *vc = [[SelectCountryVC alloc] initWithNibName:@"SelectCountryVC" bundle:nil];
    vc.showForLanguage = YES;
    [self presentViewController:vc animated:YES completion:nil];
}

-(void) clickUpgradeVIP
{
    NSInteger currentScore = [HDVLocalSetting getBonusScore];
    NSInteger proScore = [HDVLocalSetting getScoreLimitToPro];
    
    if(currentScore >= proScore /*|| DEBUG*/)
    {
        [UIAlertView showAlertViewWithMessage:TextForKey(@"EnoughScore") cancelButtonTitle:TextForKey(@"Cancel") otherButtonTitle:TextForKey(@"Yes") tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if(buttonIndex == 1)
            {
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                [[HDVAppControll sharedInstance] reportProByScoreComplete:^(bool result){
                    
                    [MBProgressHUD hideHUDForView:self.view animated:YES];

                    if(result)
                    {
                        [HDVLocalSetting setDisableShowAd:YES];
                        [UIAlertView showAlertViewWithMessage:TextForKey(@"RemoveAdsByScore") cancelButtonTitle:TextForKey(@"Close")];
                    }
                    else
                    {
                        [UIAlertView showAlertViewWithMessage:TextForKey(@"RemoveAdsByScoreFail") cancelButtonTitle:TextForKey(@"Close")];
                    }
                }];
            }
        }];
    }
    else
    {
        NSString *lang = [[HDVLanguage sharedInstance] getAppLanguage];
        NSString *msg = @"";
        NSInteger score = [HDVLocalSetting getScoreLimit];
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        NSString *sScore = [formatter stringFromNumber:@(score)];
        
        if([lang isEqualToString:@"VN"])
        {
            msg = [NSString stringWithFormat:@"Bạn cần tích luỹ đủ %@ điểm để được nâng cấp lên bản VIP!",sScore];
        }
        else
        {
            msg = [NSString stringWithFormat:@"You need collect %@ coins to upgrade this app to VIP version!",sScore];
        }
        
        [UIAlertView showAlertViewWithMessage:msg cancelButtonTitle:TextForKey(@"Close")];
    }
    

}

#pragma mark - SKStoreProduct
-(void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController
{
    [self dismissViewControllerAnimated:viewController completion:nil];
    
    if(_showProductViewForRate)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"com.hdv.rate"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSArray *arrBonus = [[HDVAdsManager sharedInstance] getAllListBonus];
        
        NSInteger more = [arrBonus[2][@"score"] intValue];
        NSInteger score = [HDVLocalSetting getBonusScore] + more;
        
        [HDVLocalSetting saveBonusScore:score];
        
        if(score < [HDVLocalSetting getScoreLimitToPro])
        {
            NSString *msg = @"";
            if([[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
            {
                msg = [NSString stringWithFormat:@"Thưởng %ld điểm!\nCảm ơn bạn đã đánh giá ứng dụng!",more];
            }
            else
            {
                msg = [NSString stringWithFormat:@"Earn %ld coins!\nThanks you for your support!",more];
            }
            [UIAlertView  showAlertViewWithMessage:msg cancelButtonTitle:TextForKey(@"Close")];
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    _showProductViewForRate = false;
}

-(void) checkInstalledAppForScore
{
    NSLog(@"Call: %s",__FUNCTION__);
    
    NSArray *arrInstalledApp = [[HDVAdsManager sharedInstance] checkAppInstallAndAddPoint];
    if(arrInstalledApp.count>0)
    {
        NSInteger score = 0;
        NSString *appName = @"";
        for (NSDictionary *app in arrInstalledApp)
        {
            score +=[app[@"score"] integerValue];
            appName = [[appName stringByAppendingString:app[@"title"]] stringByAppendingString:@","];
        }
        
        appName = [appName substringToIndex:appName.length-2];
        
        if(score < [HDVLocalSetting getScoreLimitToPro])
        {
            NSString *msg = @"";
            
            if([[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
            {
                msg = [NSString stringWithFormat:@"Thưởng %ld điểm!\nCài đặt thành công ứng dụng: %@",score,appName];
            }
            else
            {
                msg = [NSString stringWithFormat:@"Earn %ld coins!\nInstall app successfully: %@",score,appName];
            }
            
            [UIAlertView showAlertViewWithMessage:msg cancelButtonTitle:TextForKey(@"Close")];
        }
    }
}

#pragma mark - notify

-(void)handleLSNotify:(NSNotification *)notify
{
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        [UIAlertView showAlertViewWithMessage:TextForKey(@"RemoveAdsSuccess") cancelButtonTitle:TextForKey(@"Close")];
//        [self updateMenu];
//    });
}

-(void) handleAppControlNotify:(NSNotification *)notify
{
    if([notify.name isEqualToString:kHDVACM_Update])
    {
        [self updateMenu];
    }
}

@end



















