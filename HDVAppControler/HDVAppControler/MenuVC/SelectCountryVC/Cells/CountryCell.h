//
//  CountryCell.h
//  tubemate
//
//  Created by doduong on 3/2/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblCountry;
@property (weak, nonatomic) IBOutlet UIImageView *imgvSelect;

-(void) setContent:(NSDictionary *)info ForLanguageSetting:(BOOL)forLang;

@end
