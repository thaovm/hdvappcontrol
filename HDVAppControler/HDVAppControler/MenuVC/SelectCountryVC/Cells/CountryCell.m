//
//  CountryCell.m
//  tubemate
//
//  Created by doduong on 3/2/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "CountryCell.h"

@implementation CountryCell

- (void)awakeFromNib
{
    // Initialization code
    _imgvSelect.layer.borderColor = [kColor80 CGColor];
    _imgvSelect.layer.borderWidth = 1;
    _imgvSelect.layer.cornerRadius = 2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if(!selected)
    {
        self.contentView.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        self.contentView.backgroundColor = kColor90;
    }
}

-(void) setContent:(NSDictionary *)info ForLanguageSetting:(BOOL)forLang
{
    BOOL isCurrentCountry = [info[@"code"] isEqualToString:[HDVLocalSetting getCountry][@"country"]];
    if(forLang)
    {
        isCurrentCountry = [info[@"code"] isEqualToString:[[HDVLanguage sharedInstance] getAppLanguage]];
    }
    
    _lblCountry.text = info[@"name"];
    _imgvSelect.image = [UIImage imageNamed:isCurrentCountry?@"ic_checked":@""];
}

@end
