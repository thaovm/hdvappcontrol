//
//  SelectCountryVC.m
//  tubemate
//
//  Created by doduong on 3/2/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "SelectCountryVC.h"
#import "CountryCell.h"
//#import "ListVC.h"
#import "MenuVC.h"

@interface SelectCountryVC ()

@end

@implementation SelectCountryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [_tableCountry registerNib:[UINib nibWithNibName:@"CountryCell" bundle:nil] forCellReuseIdentifier:@"CountryCell"];
    
    if(!_showForLanguage)
    {
//        _marrCountry = [[NSMutableArray alloc] initWithArray:[HDVLocalSetting getCountryList]];
//        if(_marrCountry.count == 0)
//        {
//            [[TubeClient sharedInstance] getCountryListComplete:^(NSArray *arr) {
//                _marrCountry = [[NSMutableArray alloc] initWithArray:arr];
//                [_tableCountry reloadData];
//            }];
//        }
//        
//        [_itemDone setTitle:TextForKey(@"Done")];
//        [_navItemMain setTitle:TextForKey(@"SettingCountry")];
    }
    else
    {
        _marrCountry = [[NSMutableArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"LanguageOption" ofType:@"plist"]];
        [_tableCountry reloadData];
        [_itemDone setTitle:TextForKey(@"Done")];
        [_navItemMain setTitle:TextForKey(@"SettingLanguage")];
    }

    // setup searchBar
    if(!_showForLanguage)
    {
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, kWinsize.width, 44)];
        _searchBar.placeholder = TextForKey(@"SearchCountry");
        _searchBar.showsCancelButton = YES;
        _searchBar.delegate = self;
        [_tableCountry setTableHeaderView:_searchBar];
        _currentCountry = [HDVLocalSetting getCountry][@"country"];
    }
    else
    {
        _currentLanguage = [[HDVLanguage sharedInstance] getAppLanguage];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)clickDone:(id)sender
{
    [_searchBar resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if(!_showForLanguage)
    {
//        if([_currentCountry isEqualToString:[HDVLocalSetting getCountry][@"country"]])
//        {
//            // user make no change --> just close VC.
//            return;
//        }
//        
//        // update backend...
//        [[TubeClient sharedInstance] refresh];
//        [[HDVAppControlManager sharedInstance] updateCountry];
//        
//        AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
//        // force update home content...
//        ListVC *lvc = (ListVC *)[app.navHome.viewControllers firstObject];
//        [lvc changeId:@"" WithType:0];
//        
//        // force update menu
//        [[TubeClient sharedInstance] saveMenu:@[]];
//        // update menu....
//        [[TubeClient sharedInstance] getMenuV3Complete:^{
//            
//            for (MenuVC *mVC in app.navMenu.viewControllers)
//            {
//                [mVC updateMenu];
//            }
//        }];
//        
//
//        
//        [MBProgressHUD showHUDAddedTo:app.window animated:YES WithText:TextForKey(@"UpdateCountry") DelayHide:5];
    }
    else
    {
        if([_currentLanguage isEqualToString:[[HDVLanguage sharedInstance] getAppLanguage]])
        {
            // user make no change --> just close VC.
            return;
        }
        
        AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
        
        // force update menu
        for (MenuVC *mVC in app.navMenu.viewControllers)
        {
            [mVC updateMenu];
            [mVC updateTitle];
        }
        
//        [app.menuVC updateMenu];
//        [app.navMenu popToRootViewControllerAnimated:NO];
        
        for (BaseVC *vc in app.navHome.viewControllers)
        {
            if([vc isKindOfClass:[BaseVC class]]){
                [vc updateTitle];
            }
//            [app.navHome popToRootViewControllerAnimated:YES];
        }
        
        [MBProgressHUD showHUDAddedTo:app.window animated:YES WithText:TextForKey(@"UpdateLanguage") DelayHide:3];
    }
}

#pragma mark - UITableView Delegate + Data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _marrCountry.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CountryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CountryCell"];
    
    [cell setContent:_marrCountry[indexPath.row] ForLanguageSetting:_showForLanguage];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *country = _marrCountry[indexPath.row];
    
    if(!_showForLanguage)
    {
        [HDVLocalSetting saveCountry:@{@"country":country[@"code"]}];
    }
    else
    {
        [[HDVLanguage sharedInstance] setAppLanguage:country[@"code"]];
    }
    
    [tableView reloadData];
    [_searchBar resignFirstResponder];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

#pragma mark - UISearchBar Delegate
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    _marrCountry = [[NSMutableArray alloc] initWithArray:[HDVLocalSetting getCountryList]];
    [_tableCountry reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    
    NSString *searchText = searchBar.text;
    
    searchText = [searchText uppercaseString];
    
    NSArray *arrAll = [HDVLocalSetting getCountryList];
    [_marrCountry removeAllObjects];
    
    for(NSDictionary *country in arrAll)
    {
        NSString *name = country[@"name"];
        NSString *code = country[@"code"];
        
        if([name rangeOfString:searchText].location != NSNotFound)
        {
            [_marrCountry addObject:country];
        }
        else if([code rangeOfString:searchText].location != NSNotFound)
        {
            [_marrCountry addObject:country];
        }
    }
    
    [_tableCountry reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    searchText = [searchText uppercaseString];
    
    NSArray *arrAll = [HDVLocalSetting getCountryList];
    [_marrCountry removeAllObjects];
    
    for(NSDictionary *country in arrAll)
    {
        NSString *name = country[@"name"];
        NSString *code = country[@"code"];
        
        if([name rangeOfString:searchText].location != NSNotFound)
        {
            [_marrCountry addObject:country];
        }
        else if([code rangeOfString:searchText].location != NSNotFound)
        {
            [_marrCountry addObject:country];
        }
    }
    
    [_tableCountry reloadData];
}


@end







