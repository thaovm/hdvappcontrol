//
//  SelectCountryVC.h
//  tubemate
//
//  Created by doduong on 3/2/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectCountryVC : UIViewController <UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    NSMutableArray *_marrCountry;
    UISearchBar *_searchBar;
    NSString *_currentCountry;
    NSString *_currentLanguage;
}

@property (weak, nonatomic) IBOutlet UIBarButtonItem *itemDone;
@property (weak, nonatomic) IBOutlet UINavigationItem *navItemMain;
@property (weak, nonatomic) IBOutlet UITableView *tableCountry;
@property (assign, nonatomic) BOOL showForLanguage;

- (IBAction)clickDone:(id)sender;

@end
