//
//  HDVLocalSetting.h
//  HDVAppControler
//
//  Created by ThaoVM on 9/3/15.
//  Copyright (c) 2015 ThaoVM. All rights reserved.
//

#import <Foundation/Foundation.h>
#define kFullVersionCheckPath @"http://mp3.hdvietpro.com/mp3_zing/publish.php?url_scheme=%@&date=%@"

#define kLSNotityChangeScore @"klsn_1"
#define kLSNotityChangeScoreToPro @"klsn_2"
@interface HDVLocalSetting : NSObject
//+(BOOL)canShowDownload;
//+(BOOL)canShowShare;
//+(BOOL)canshowBKPopup;
//+(BOOL)canshowCloseApp;
//

+(void)saveInstallDate;
+(void)saveInstallDate:(NSString *)date;
+(NSString *) getInstallDate;

+(void)saveInstallDateV2;
+(void)saveInstallDateV2:(NSString *)date;
+(NSString *) getInstallDateV2;

+(void)setInstallStamp;
+(void)setInstallStamp:(NSInteger)stamp;
+(NSInteger)getInstallStamp;


+(BOOL)getDisableShowAd;
+(void)setDisableShowAd:(BOOL)showApp;

+(void)setIgnoreUpdateVersion:(NSString *)version;
+(BOOL)getIgnoreUpdateVersion:(NSString *)version;
+(void)setRemindUpdateTime:(int)lastRemindTime forVersion:(NSString *)version;
+(NSInteger)getRemindUpdateTimeForVersion:(NSString *)version;

+(void) saveAppControl:(NSDictionary *)dict;
+(NSDictionary *)getAppControl;


+(void)setFullVersion:(BOOL)full;
+(BOOL)getFullVersion;
+(NSString*) getCurrentVersion;


#pragma mark - show ipod song

+(void) saveShowIPod:(BOOL)show;
+(BOOL) getShowIPod;


#pragma mark - share

//+(void) saveSuggestShareStamp;
//+(NSInteger) getSuggestShareStamp;

//+(void) saveSuggestCount;
//+(NSInteger) getSuggestCount;

+(void) suggestUserShare;

+(void)saveCountry:(NSDictionary *)country;
+(NSDictionary *)getCountry;

#pragma mark - Collect Device info
+(NSString *)getUUID;
+(NSString *)getAdId;
+(NSString*) platform;
+(NSString *)getDeviceType;
+(NSString *)getCarrier;
+(NSString *)getCurrentOSVersion;
#pragma mark - Score
+(void)saveScoreLimit:(NSInteger)score;
+(NSInteger)getScoreLimit;
+(NSInteger)getBonusScore;
+(void)saveBonusScore:(NSInteger)score;
+(NSInteger)getScoreLimitToPro;
#pragma mark - Page
+(NSArray *)getPendingPageList;
+(void)savePendingPage:(NSDictionary *)page;
#pragma mark - save report apns
+(void) setReportStatus:(BOOL)status forApnsCode:(NSString *)code;
+(BOOL) getReportStatusForApnsCode:(NSString *)code;
#pragma mark - save token
+(void)saveApnsToken:(NSString *)token;
+(NSString *)getApnsToken;
#pragma mark - setting download cellular
+(void) saveSettingCellularOff:(BOOL)value;
+(BOOL) getSettingCellularOff;
#pragma mark - Page
+(void) remeovePendingPage;
+(void) forceRemovePendingPage;
#pragma mark - limit click ads

+(void) increaseClickCountForCurrentDay;
+(NSInteger) getClickCountForCurrentDay;

+(NSString *)checksumForIntValue:(NSInteger)value;
+(NSArray *)checkPageInstallAndAddPointWithInputPages:(NSArray *)arrLikes;
#pragma mark - subcribe

+(void) saveSuggestSubcribeStamp;

+(NSInteger) getSuggestSubcribeStamp;

+(void) saveSubcribeCount;

+(NSInteger) getSubcribeCount;

+(void) suggestUserSubscribeFBPage;

+(void)saveCountryList:(NSArray*)countryList;
+(NSArray *)getCountryList;
@end
