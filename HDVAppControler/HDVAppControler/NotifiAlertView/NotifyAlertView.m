//
//  NotifyAlertView.m
//  tubemate
//
//  Created by doduong on 6/3/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "NotifyAlertView.h"

@implementation NotifyAlertView

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    _viewContent.layer.cornerRadius = 4;
    self.backgroundColor = [UIColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:0.7];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+(void)showAlertWithTitle:(NSString *)title Message:(NSString *)message AllowClose:(BOOL)allowClose ActionTitle:(NSString *)actionTitle CloseBlock:(void (^)())closeBlock ActionBlock:(void (^)())actionBlock
{
    CGSize winSize = [UIScreen mainScreen].bounds.size;
    CGSize messageSize = [message sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(260, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    
    NotifyAlertView *v = [[NSBundle mainBundle] loadNibNamed:@"NotifyAlertView" owner:nil options:nil][0];
    v.frame = CGRectMake(0, 0, winSize.width, winSize.height);
    
    CGRect frameContent = v.viewContent.frame;
    frameContent.size.height = messageSize.height + 20 + 128;
    frameContent.size.height>(winSize.height-10)?(frameContent.size.height = winSize.height - 20):0;
    frameContent.origin.y = (winSize.height - frameContent.size.height)*0.5;
    v.viewContent.frame = frameContent;
    
    v.didClickAction = actionBlock;
    v.didClickClose = closeBlock;
    allowClose?(v.btnClose.hidden = NO):(v.btnClose.hidden = YES);
    v.lblMessage.text = message;
    v.lblTitle.text = title;
    
    if([actionTitle isEqualToString:@"subribeFBPage"])
    {
        [v.btnAction setTitle:@"" forState:UIControlStateNormal];
        [v.btnAction setBackgroundImage:[UIImage imageNamed:@"btn_like"] forState:UIControlStateNormal];
    }
    else if([actionTitle isEqualToString:@"suggestShare"])
    {
        [v.btnAction setTitle:@"" forState:UIControlStateNormal];
        [v.btnAction setBackgroundImage:[UIImage imageNamed:@"btn_sharefb"] forState:UIControlStateNormal];
    }
    else
    {
        [v.btnAction setTitle:actionTitle forState:UIControlStateNormal];
    }
    
    [v layoutSubviews];
    
    v.viewContent.transform = CGAffineTransformMakeScale(0,0);
    
    [UIView animateWithDuration:0.3 animations:^{
        v.viewContent.transform = CGAffineTransformMakeScale(1.2, 1.2);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            v.viewContent.transform = CGAffineTransformMakeScale(0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.05 animations:^{
                v.viewContent.transform = CGAffineTransformIdentity;
            } completion:^(BOOL finished) {
                ;
            }];
        }];
    }];
    
    id<UIApplicationDelegate> app = [UIApplication sharedApplication].delegate;
    [[app window] addSubview:v];
}

- (IBAction)clickAction:(id)sender
{
    _didClickAction?_didClickAction():0;
    
    [UIView animateWithDuration:0.1 animations:^{
        self.viewContent.transform = CGAffineTransformMakeScale(1.2, 1.2);
        self.alpha = 0.4;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3 animations:^{
            self.viewContent.transform = CGAffineTransformMakeScale(0.1, 0.1);
            self.alpha = 0;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }];
    
}

- (IBAction)clickClose:(id)sender
{
    _didClickClose?_didClickClose():0;
    [self removeFromSuperview];
}

@end
