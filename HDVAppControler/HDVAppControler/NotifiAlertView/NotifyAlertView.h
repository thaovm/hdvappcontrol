//
//  NotifyAlertView.h
//  tubemate
//
//  Created by doduong on 6/3/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotifyAlertView : UIView
{
    
}

@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (weak, nonatomic) IBOutlet UIImageView *imgvLeft;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnAction;
@property (copy, nonatomic) void(^didClickClose)();
@property (copy, nonatomic) void(^didClickAction)();

+(void)showAlertWithTitle:(NSString *)title Message:(NSString *)message AllowClose:(BOOL)allowClose ActionTitle:(NSString *)actionTitle CloseBlock:(void(^)())closeBlock ActionBlock:(void(^)())actionBlock;

- (IBAction)clickAction:(id)sender;
- (IBAction)clickClose:(id)sender;
@end
