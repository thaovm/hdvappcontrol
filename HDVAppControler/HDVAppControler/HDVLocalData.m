//
//  HDVSettingManager.m
//  HDVAppControler
//
//  Created by ThaoVM on 9/2/15.
//  Copyright (c) 2015 ThaoVM. All rights reserved.
//

#import "HDVLocalData.h"

@implementation HDVLocalData
+(void)saveJsonObject:(NSDictionary *)info forKey:(NSString *)key
{
    NSData *data = [NSJSONSerialization dataWithJSONObject:info options:NSJSONWritingPrettyPrinted error:nil];
    if(data)
    {
        [data writeToFile:[self getPathForKey:key] atomically:YES];
    }
}

+(NSObject *)getJsonObjectForKey:(NSString *)key
{
    NSData *data = [NSData dataWithContentsOfFile:[self getPathForKey:key]];
    if(data)
    {
        NSObject *obj = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        
        return obj;
    }
    
    return nil;
}

+ (NSString *) enumStringName:(eLocalFileKey)eLocalKey appendKey:(NSString *)apkey{
    NSString *result = nil;
    
    switch(eLocalKey) {
        case eLocalFileKeyLanguage:
            result = @"com.gamexp.lang";
            break;
        case eLocalFileKeyUserAgent:
            result = @"com.gamexp.useragent";
            break;
        case eLocalFileKeyDownloadAdStamp:
            result = @"com.mp3.downloadAdsStamp";
            break;
        case eLocalFileKeyDateInstall:
            result = [NSString stringWithFormat:@"com.hdv.mp3.installDate.%@",apkey];
            break;
        case eLocalFileKeyDateInstall2:
            result = [NSString stringWithFormat:@"com.hdv.mp3.installDate2.%@",apkey];
            break;
        case eLocalFileKeyStampInstall:
            result = [@"com.gamexp.installStamp_" stringByAppendingString:apkey];
            break;
        case eLocalFileKeyHaveDownloadLimit:
            result = @"com.gamexp.downloadLimit";
            break;
        case eLocalFileKeyHaveInitDownloadLimit:
            result = @"com.gamexp.haveinitdownload";
            break;
        case eLocalFileKeyDownloadCount:
            result = @"com.hdv.downloadedCount";
            break;
        case eLocalFileKeyDisableShowAd:
            result = @"com.gamexp.ad_dis";
            break;
        case eLocalFileKeyIgnoreUpdate:
            result = [@"com.gamexp.ignoreUpdate_" stringByAppendingString:apkey];
            break;
        case eLocalFileKeyRemindUpdateTime:
            result = [@"com.gamexp.updateRemind_" stringByAppendingString:apkey];
            break;
        case eLocalFileKeyHaveFullVersion:
            result = [@"com.mp3.haveFull" stringByAppendingString:apkey];
            break;
        case eLocalFileKeyMp3AppControl:
            result = [@"com.mp3.appControl." stringByAppendingString:apkey];
            break;
        case eLocalFileKeyTimeCloseThumbnail:
            result = @"closeThumbStamp";
            break;
        case eLocalFileKeyCellularOff:
            result = @"cellularDownload";
            break;
        case eLocalFileKeySyncIpod:
            result = @"com.hdv.show_ipod";
            break;
        case eLocalFileKeySuggestShareStamp:
            result = @"SuggestShareStamp";
            break;
        case eLocalFileKeySuggestSubcribeCount:
            result = @"suggestSubcribeCount";
            break;
        case eLocalFileKeyListPendingApp:
            result = @"pendingApp";
            break;
        case eLocalFileKeySuggestShareCount:
            result = @"suggestShareCount";
            break;
        case eLocalFileKeyMobileCoreStamp:
            result = @"com.hdv.mobileCoreStamp";
            break;
        case eLocalFileKeyUserRegister:
            result = @"com.gamexp.register";
            break;
        case eLocalFileKeyPopupTimeStamp:
            result = @"com.hdv.normalpopupStamp";
            break;
        case eLocalFileKeyTimeClickBanner:
            result = @"com.hdv.clickBanner";
            break;
        case eLocalFileKeyHeaderAgent:
            result = @"com.hdv.headerAgent";
            break;
        default:
            result = [[NSString alloc] initWithFormat:@"%ld",eLocalKey];
            break;
    }
    
    return result;
}


+(void)saveBool:(BOOL)value forKey:(eLocalFileKey)key appendKey:(NSString *)apkey
{
    NSDictionary *info = @{
                           @"bool":@(value),
                           };
    [self saveJsonObject:info forKey:[self enumStringName:key appendKey:apkey]];
}

+(BOOL)getBoolForKey:(eLocalFileKey)key appendKey:(NSString *)apkey
{
    NSDictionary *info = (NSDictionary *)[self getJsonObjectForKey:[self enumStringName:key appendKey:apkey]];
    if(info == nil)
    {
        return NO;
    }
    
    return [info[@"bool"] intValue] == 1;
}

+(void)saveString:(NSString *)value forKey:(eLocalFileKey)key appendKey:(NSString *)apkey
{
    NSDictionary *info = @{
                           @"string":value?:@"",
                           };
    [self saveJsonObject:info forKey:[self enumStringName:key appendKey:apkey]];
}

+(NSString *)getStringForKey:(eLocalFileKey)key appendKey:(NSString *)apkey
{
   
    
    NSDictionary *info = (NSDictionary *)[self getJsonObjectForKey:[self enumStringName:key appendKey:apkey]];
    if(!info)
    {
        NSObject * obj = [[NSUserDefaults standardUserDefaults] objectForKey:[self enumStringName:key appendKey:apkey]];
        return obj?(NSString *)obj:@"";
    }
    
    return info[@"string"];
}

+(void)saveInteger:(NSInteger)value forKey:(eLocalFileKey)key appendKey:(NSString *)apkey
{
    NSDictionary *info = @{
                           @"integer":@(value)?:@(0),
                           };
    [self saveJsonObject:info forKey:[self enumStringName:key appendKey:apkey]];
}
+(NSInteger )getIntegerForKey:(eLocalFileKey)key appendKey:(NSString *)apkey
{
    NSDictionary *info = (NSDictionary *)[self getJsonObjectForKey:[self enumStringName:key appendKey:apkey]];
    if(info == nil)
    {
        NSInteger value = [[[NSUserDefaults standardUserDefaults] objectForKey:[self enumStringName:key appendKey:apkey]] integerValue];
        return value;
    }
    
    return [info[@"integer"] integerValue];
}


+(void)saveObject:(NSObject *)obj forKey:(eLocalFileKey)key appendKey:(NSString *)apkey{
    NSDictionary *info = @{
                           @"object":obj?:@"",
                           };
    [self saveJsonObject:info forKey:[self enumStringName:key appendKey:apkey]];

}

+(NSObject *)getObjectForKey:(eLocalFileKey)key appendKey:(NSString *)apkey{
    
    NSDictionary *info = (NSDictionary *)[self getJsonObjectForKey:[self enumStringName:key appendKey:apkey]];
    if(info == nil)
    {
        return [[NSUserDefaults standardUserDefaults] objectForKey:[self enumStringName:key appendKey:apkey]];
    }
    
    return info[@"object"];
}

+(void)saveJsonObject:(NSDictionary *)info forKey:(eLocalFileKey)key appendKey:(NSString *)apkey{
    [self saveJsonObject:info forKey:[self enumStringName:key appendKey:apkey]];
}

+(NSDictionary *)getJsonObjectForKey:(eLocalFileKey)key appendKey:(NSString *)apkey{
    return (NSDictionary *)[self getJsonObjectForKey:[self enumStringName:key appendKey:apkey]];
}

+(NSString *)getDocumentsPath
{
    return NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
}

+(NSString *)getPathForKey:(NSString *)key
{
    NSString *docPath = [self getDocumentsPath];
    docPath = [docPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",key]];
    return docPath;
}




+(NSString *) getAppBundleId
{
    return [[NSBundle mainBundle] bundleIdentifier];
}


@end
