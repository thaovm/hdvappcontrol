//
//  HDVSettingManager.h
//  HDVAppControler
//
//  Created by ThaoVM on 9/2/15.
//  Copyright (c) 2015 ThaoVM. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kFullVersionCheckPath @"http://mp3.hdvietpro.com/mp3_zing/publish.php?url_scheme=%@&date=%@"

#define kLSNotityChangeScore @"klsn_1"
#define kLSNotityChangeScoreToPro @"klsn_2"
typedef NS_ENUM(NSInteger, eLocalFileKey)
{
    eLocalFileKeyNonOrganicInstallLog = -1,
    eLocalFileKeyHaveReportNonOrganicInstall = -2,
    eLocalFileKeyTokenAPNS = -3,
    eLocalFileKeyAppControl = -4,
    eLocalFileKeyUserLocation = -5,
    eLocalFileKeyTimeClickAds = -6,
    eLocalFileKeyAppPublished = -7,
    eLocalFileKeyDateInstall = -8,
    eLocalFileKeyStampInstall = -9,
    eLocalFileKeyLanguage = -10,
    eLocalFileKeyUserAgent = -11,
    eLocalFileKeyDownloadAdStamp = -12,
    eLocalFileKeyDateInstall2 = -13,
    eLocalFileKeyHaveDownloadLimit = -14,
    eLocalFileKeyHaveInitDownloadLimit = -15,
    eLocalFileKeyDownloadCount = -16,
    eLocalFileKeyDisableShowAd = -17,
    eLocalFileKeyIgnoreUpdate = -18,
    eLocalFileKeyRemindUpdateTime = -19,
    eLocalFileKeyHaveFullVersion = -20,
    eLocalFileKeyTimeCloseThumbnail = -21,
    eLocalFileKeyCellularOff = -22,
    eLocalFileKeySaveLimitClick = -23,
    eLocalFileKeySyncIpod = -24,
    eLocalFileKeySuggestShareStamp = -25,
    eLocalFileKeySuggestSubcribeCount = -26,
    eLocalFileKeyListPendingApp = -27,
    eLocalFileKeyMp3AppControl = -28,
    eLocalFileKeySuggestShareCount = -29,
    eLocalFileKeyNormalPopupStamp = -30,
    eLocalFileKeyMobileCoreStamp = -31,
    eLocalFileKeySaveCountry = -32,
    eLocalFileKeyUserRegister = -33,
    eLocalFileKeyPopupTimeStamp = -34,
    eLocalFileKeyTimeClickBanner = -35,
    eLocalFileKeyHeaderAgent = -36
    
};

@interface HDVLocalData : NSObject

//+(void)saveJsonObject:(NSDictionary *)info forKey:(NSString *)key;
//+(NSObject *)getJsonObjectForKey:(NSString *)key;
+(NSString *) enumStringName:(eLocalFileKey) eLocalKey appendKey:(NSString *)apkey;
+(void)saveBool:(BOOL)value forKey:(eLocalFileKey)key appendKey:(NSString *)apkey;

+(void)saveJsonObject:(NSDictionary *)info forKey:(eLocalFileKey)key appendKey:(NSString *)apkey;
+(NSDictionary *)getJsonObjectForKey:(eLocalFileKey)key appendKey:(NSString *)apkey;


+(BOOL)getBoolForKey:(eLocalFileKey)key appendKey:(NSString *)apkey;

+(void)saveString:(NSString *)value forKey:(eLocalFileKey)key appendKey:(NSString *)apkey;
+(NSString *)getStringForKey:(eLocalFileKey)key appendKey:(NSString *)apkey;

+(void)saveInteger:(NSInteger)value forKey:(eLocalFileKey)key appendKey:(NSString *)apkey;
+(NSInteger )getIntegerForKey:(eLocalFileKey)key appendKey:(NSString *)apkey;

+(void)saveObject:(NSObject *)obj forKey:(eLocalFileKey)key appendKey:(NSString *)apkey;
+(NSObject *)getObjectForKey:(eLocalFileKey)key appendKey:(NSString *)apkey;

+(NSString *)getDocumentsPath;
+(NSString *)getPathForKey:(NSString *)key;

+(NSString*) getCurrentVersion;
+(NSString *) getAppBundleId;
@end
