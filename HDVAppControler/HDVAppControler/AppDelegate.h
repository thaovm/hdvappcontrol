//
//  AppDelegate.h
//  HDVAppControler
//
//  Created by ThaoVM on 9/2/15.
//  Copyright (c) 2015 ThaoVM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "MFSideMenuContainerViewController.h"
#import "MenuVC.h"
#import "HomeVC.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) MFSideMenuContainerViewController *sideMenu;
@property (strong, nonatomic) UINavigationController *navMenu;
@property (strong, nonatomic) UINavigationController *navHome;
@property (strong, nonatomic) MenuVC *menuVC;
@property (strong, nonatomic) HomeVC *homeVC;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

