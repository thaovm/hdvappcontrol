//
//  HDVLocalSetting.m
//  HDVAppControler
//
//  Created by ThaoVM on 9/3/15.
//  Copyright (c) 2015 ThaoVM. All rights reserved.
//

#import "HDVLocalSetting.h"
#import "HDVLocalData.h"
#import <UIKit/UIKit.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <AdSupport/AdSupport.h>
#import <FacebookSDK/FacebookSDK.h>
#import <FBAudienceNetwork/FBAudienceNetwork.h>
#import <sys/utsname.h>
#import "NSString+Securities.h"
#import "NotifyAlertView.h"
#import "HDVLanguage.h"
#import "HDVAdsManager.h"
@implementation HDVLocalSetting

+(BOOL)canShowDownload
{
    return [HDVLocalSetting getFullVersion];
}

+(BOOL)canShowShare
{
    return [HDVLocalSetting canShowDownload];
}

+(BOOL) canshowBKPopup
{
    return [HDVLocalSetting canShowDownload];
}

+(BOOL)canshowCloseApp
{
    return [HDVLocalSetting canShowDownload];
}


#pragma mark - Save Install Date
+(void) saveInstallDate
{
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    
    NSString *dateString = [df stringFromDate:currentDate];
    [HDVLocalData saveString:dateString forKey:eLocalFileKeyDateInstall appendKey:[HDVLocalSetting getCurrentVersion]];
}

+(void)saveInstallDate:(NSString *)date{
    [HDVLocalData saveString:date forKey:eLocalFileKeyDateInstall appendKey:[HDVLocalSetting getCurrentVersion]];
}

+(NSString *)getInstallDate{
    return [HDVLocalData getStringForKey:eLocalFileKeyDateInstall appendKey:[HDVLocalSetting getCurrentVersion]];

}



+(void) saveInstallDateV2{
    NSDate *currentDate = [NSDate date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSString *dateString = [df stringFromDate:currentDate];
      [HDVLocalData saveString:dateString forKey:eLocalFileKeyDateInstall2 appendKey:[HDVLocalSetting getCurrentVersion]];
}

+(void)saveInstallDateV2:(NSString *)date{
    [HDVLocalData saveString:date forKey:eLocalFileKeyDateInstall2 appendKey:[HDVLocalSetting getCurrentVersion]];
}

+(NSString *)getInstallDateV2{
    return [HDVLocalData getStringForKey:eLocalFileKeyDateInstall2 appendKey:[HDVLocalSetting getCurrentVersion]];
}



+(void)setInstallStamp{
    NSDate *date = [NSDate date];
    int stamp = [date timeIntervalSince1970];
    [HDVLocalData saveInteger:stamp forKey:eLocalFileKeyStampInstall appendKey:[HDVLocalSetting getCurrentVersion]];
}

+ (void)setInstallStamp:(NSInteger)stamp{
     [HDVLocalData saveInteger:stamp forKey:eLocalFileKeyStampInstall appendKey:[HDVLocalSetting getCurrentVersion]];
}


+(NSInteger)getInstallStamp{
    return [HDVLocalData getIntegerForKey:eLocalFileKeyStampInstall appendKey:[HDVLocalSetting getCurrentVersion]];
}

#pragma mark - Disable Show Ads
+(void)setDisableShowAd:(BOOL)showApp{
    [HDVLocalData saveBool:showApp forKey:eLocalFileKeyDisableShowAd appendKey:@""];
}

+(BOOL)getDisableShowAd
{
    return [HDVLocalData getBoolForKey:eLocalFileKeyDisableShowAd appendKey:@""];
}

#pragma mark - Remind Update version
+(void)setRemindUpdateTime:(int)lastRemindTime forVersion:(NSString *)version{
    [HDVLocalData saveInteger:lastRemindTime forKey:eLocalFileKeyRemindUpdateTime appendKey:version];
}
+(NSInteger)getRemindUpdateTimeForVersion:(NSString *)version{
    return [HDVLocalData getIntegerForKey:eLocalFileKeyRemindUpdateTime appendKey:version];
}

+(void)setIgnoreUpdateVersion:(NSString *)version{
    [HDVLocalData saveBool:YES forKey:eLocalFileKeyIgnoreUpdate appendKey:version];
}

+(BOOL)getIgnoreUpdateVersion:(NSString *)version{
    return [HDVLocalData getBoolForKey:eLocalFileKeyIgnoreUpdate appendKey:version];
}
#pragma mark - App control
+(void)saveAppControl:(NSDictionary *)dict{
    if([self checkDictionaryHaveNull:dict])
    {
        return;
    }
    [HDVLocalData saveJsonObject:dict forKey:eLocalFileKeyMp3AppControl appendKey:[self getCurrentVersion]];
    
}

+(NSDictionary *)getAppControl{
    return [HDVLocalData getJsonObjectForKey:eLocalFileKeyMp3AppControl appendKey:[self getCurrentVersion]];
}

+(BOOL) checkDictionaryHaveNull:(NSDictionary *)dict
{
    for(NSString *key in dict.allKeys)
    {
        if([dict[key] isKindOfClass:[NSNull class]])
        {
            return true;
        }
        else if([dict[key] isKindOfClass:[NSDictionary class]])
        {
            if([self checkDictionaryHaveNull:dict[key]])
            {
                return true;
            }
        }
        else if([dict[key] isKindOfClass:[NSArray class]])
        {
            if([self checkArrayHaveNull:dict[key]])
            {
                return true;
            }
        }
    }
    
    return false;
}
+(BOOL) checkArrayHaveNull:(NSArray *)arr
{
    for(id item in arr)
    {
        if([item isKindOfClass:[NSArray class]])
        {
            if([self checkArrayHaveNull:item])
            {
                return  true;
            }
        }
        else if([item isKindOfClass:[NSDictionary class]])
        {
            if([self checkDictionaryHaveNull:item])
            {
                return true;
            }
        }
        else if([item isKindOfClass:[NSNull class]])
        {
            return true;
        }
    }
    
    return false;
}




#pragma mark - Set get Version
+(void)setFullVersion:(BOOL)full{
    [HDVLocalData saveBool:full forKey:eLocalFileKeyHaveFullVersion appendKey:[HDVLocalSetting getCurrentVersion]];
}

+(BOOL)getFullVersion{
    return [HDVLocalData getBoolForKey:eLocalFileKeyHaveFullVersion appendKey:[HDVLocalSetting getCurrentVersion]];
}


+(NSString*) getCurrentVersion
{
    NSString *s = [NSString stringWithFormat:@"%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
//    NSLog(@"Version: %@",s);
    return s;
}

#pragma mark - sync ipod
+(void)saveShowIPod:(BOOL)show{
    [HDVLocalData saveBool:show forKey:eLocalFileKeySyncIpod appendKey:@""];
}

+(BOOL)getShowIPod{
    return [HDVLocalData getBoolForKey:eLocalFileKeySyncIpod appendKey:@""];
}

#pragma mark - subcribe

+(void) saveSuggestSubcribeStamp
{
    NSString *key = @"suggestSubcribeStamp";
    [[NSUserDefaults standardUserDefaults] setObject:@([[NSDate date] timeIntervalSince1970]) forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSInteger) getSuggestSubcribeStamp
{
    NSString *key = @"suggestSubcribeStamp";
    NSInteger stamp = [[[NSUserDefaults standardUserDefaults] objectForKey:key] integerValue];
    return stamp;
}

+(void) saveSubcribeCount
{
    NSString *key = @"suggestSubcribeCount";
    [[NSUserDefaults standardUserDefaults] setObject:@([self getSuggestCount]+1) forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSInteger) getSubcribeCount
{
    NSString *key = @"suggestSubcribeCount";
    NSInteger count = [[[NSUserDefaults standardUserDefaults] objectForKey:key] integerValue];
    return count;
}

+(void) suggestUserSubscribeFBPage
{
    NSInteger duration2 = 60*60*24*2; // 2 days
    
    NSInteger lastSuggest = [self getSuggestSubcribeStamp];
    NSInteger installStamp = [self getInstallStamp];
    NSInteger now = [[NSDate date] timeIntervalSince1970];
    NSInteger detalT = now - lastSuggest;
    
    if(lastSuggest == 0 && (now - installStamp >= duration2))
    {
        [self saveSuggestSubcribeStamp];
        [self saveSuggestCount];
    }
    else if(detalT > duration2 && lastSuggest != 0  && [self getSuggestCount] < 5)
    {
        [self saveSuggestSubcribeStamp];
        [self saveSuggestCount];
    }
    else
    {
        return;
    }
    
    [NotifyAlertView showAlertWithTitle:TextForKey(@"SubscribeFBPageTitle") Message:TextForKey(@"SubscribeFBPageMessage") AllowClose:YES ActionTitle:@"subribeFBPage" CloseBlock:^{
        ;
    } ActionBlock:^{
        
        NSString *fanpageId = @"428107764019737";
        NSString *fanpageLink = @"https://www.facebook.com/pages/Mp3-Play/428107764019737";
        
        NSURL *facebookURL = [NSURL URLWithString:[NSString stringWithFormat:@"fb://profile/%@",fanpageId]];
        if ([[UIApplication sharedApplication] canOpenURL:facebookURL])
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] openURL:facebookURL];
                });
            });
        }
        else
        {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:fanpageLink]];
                });
            });
        }
        
    }];
}
#pragma mark - share

+(void) saveSuggestShareStamp
{
    [HDVLocalData saveInteger:[[NSDate date] timeIntervalSince1970] forKey:eLocalFileKeySuggestShareStamp appendKey:@""];
}

+(NSInteger) getSuggestShareStamp
{
    return [HDVLocalData getIntegerForKey:eLocalFileKeySuggestShareStamp appendKey:@""];
}

+(void) saveSuggestCount
{
    [HDVLocalData saveInteger:[self getSuggestCount]+1 forKey:eLocalFileKeySuggestShareCount appendKey:@""];
}

+(NSInteger) getSuggestCount
{
    return [HDVLocalData getIntegerForKey:eLocalFileKeySuggestShareCount appendKey:@""];
}

+(void) suggestUserShare
{
    NSInteger duration1 = 60*60*24; // 1 day
    NSInteger duration2 = duration1*2; // 2 days
    
    NSInteger lastSuggest = [self getSuggestShareStamp];
    NSInteger installStamp = [self getInstallStamp];
    NSInteger now = [[NSDate date] timeIntervalSince1970];
    NSInteger detalT = now - lastSuggest;
    
    if(lastSuggest == 0 && (now - installStamp >= duration1))
    {
        [self saveSuggestShareStamp];
        [self saveSuggestCount];
    }
    else if(detalT > duration2 && lastSuggest != 0  && [self getSuggestCount] < 3)
    {
        [self saveSuggestShareStamp];
        [self saveSuggestCount];
    }
    else
    {
        return;
    }
    
    [NotifyAlertView showAlertWithTitle:TextForKey(@"ShareFB") Message:TextForKey(@"SuggestShareApp") AllowClose:YES ActionTitle:@"suggestShare" CloseBlock:^{
        ;
    } ActionBlock:^{
        NSString *appId = [[[HDVAdsManager sharedInstance] getAppleStoreId] length]?[[HDVAdsManager sharedInstance] getAppleStoreId]:kStoreId;
        
        NSString *link = [NSString stringWithFormat:@"https://itunes.apple.com/app/id%@",appId];
        [[FBManager sharedManager] shareWithName2:kAppName caption:@"" description:@"" link:link picture:link success:^{
            
            // save suggest count to prevent suggest share in other time open app
            
            [self saveSuggestCount];
            [self saveSuggestCount];
            [self saveSuggestCount];
            
            [UIAlertView showAlertViewWithMessage:TextForKey(@"ThanksForSharing") cancelButtonTitle:TextForKey(@"Close")];
            
        } failed:^(NSString *reason) {
            
            NSLog(@"%s",__FUNCTION__);
            
        }];
    }];
}




+(void)saveCountry:(NSDictionary *)country
{
    //    if(![country isKindOfClass:[NSDictionary class]])
    //        return;
    //
    //    [[NSUserDefaults standardUserDefaults] setObject:country forKey:@"com.gamexp.country"];
    //    [[NSUserDefaults standardUserDefaults] synchronize];
    [[HDVLanguage sharedInstance] setAppLanguage:country[@"country"]];
    NSData *data = [NSJSONSerialization dataWithJSONObject:country options:NSJSONWritingPrettyPrinted error:nil];
    
    NSString *savePath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    savePath = [savePath stringByAppendingPathComponent:@"location"];
    
    [data writeToFile:savePath atomically:YES];
}

+(NSDictionary *)getCountry
{
    
    NSString *savePath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    savePath = [savePath stringByAppendingPathComponent:@"location"];
    
    NSData *data = [NSData dataWithContentsOfFile:savePath];
    
    if(data)
    {
        NSDictionary *country = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        return country;
    }
    else
    {
        return nil;
    }
}

#pragma mark - Collect Device info

+(NSString *)getUUID
{
    NSUUID *uuid=  [UIDevice currentDevice].identifierForVendor;
    
    return [uuid UUIDString];
}

+(NSString *)getAdId
{
    NSUUID *uuid = [ASIdentifierManager sharedManager].advertisingIdentifier;
    return [uuid UUIDString];
}

+(NSString*) platform{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *result = [NSString stringWithCString:systemInfo.machine
                                          encoding:NSUTF8StringEncoding];
    return result;
}

+(NSString *)getDeviceType
{
    NSString *platform = [self platform];
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad mini 2G (WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad mini 2G (Cellular)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    
    return platform;
}

+(NSString *)getCarrier
{
    CTTelephonyNetworkInfo* info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier* carrier = info.subscriberCellularProvider;
    NSString *carrierName = carrier.carrierName;
    
    return carrierName?:@"";
}

+(NSString *)getCurrentOSVersion
{
    return [UIDevice currentDevice].systemVersion;
}


#pragma mark - Score
+(void)saveScoreLimit:(NSInteger)score
{
    NSString *valid = [[NSString stringWithFormat:@"%@|%ld",[self getAdId],score] stringMD5];
    NSDictionary *info = @{
                           @"score":@(score),
                           @"check":valid,
                           };
    
    [info writeToFile:[self pathForFile:@"scoreLimit"] atomically:YES];
}
+(NSInteger)getScoreLimit
{
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:[self pathForFile:@"scoreLimit"]];
    NSInteger score = [dict[@"score"] integerValue];
    NSString *check = dict[@"check"];
    
    NSString *valid = [self checksumForIntValue:score];
    
    if([check isEqualToString:valid])
    {
        NSLog(@"ScoreLimit: %ld",score);
        return score;
    }
    else
    {
        [self saveScoreLimit:30000];
        return 0;
    }
}

+(void)saveBonusScore:(NSInteger)score
{
    NSString *valid = [[NSString stringWithFormat:@"%@|%ld",[self getAdId],score] stringMD5];
    NSDictionary *info = @{
                           @"score":@(score),
                           @"check":valid,
                           };
    
    [info writeToFile:[self pathForFile:@"score"] atomically:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kLSNotityChangeScore object:nil userInfo:nil];
    
    if(score>=[self getScoreLimitToPro])
    {
        [self setDisableShowAd:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:kLSNotityChangeScoreToPro object:nil userInfo:nil];
    }
}



+(NSInteger)getBonusScore{
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:[self pathForFile:@"score"]];
    NSInteger score = [dict[@"score"] integerValue];
    NSString *check = dict[@"check"];
    
    NSString *valid = [self checksumForIntValue:score];
    
    return score;
}


+(NSInteger)getScoreLimitToPro
{
    //    return [_dictAppControll[@"system_score"][@"score_convert_pro"] integerValue]?:30000;
    return [self getScoreLimit];
}



+(NSString *)pathForFile:(NSString *)file
{
    NSString *doc = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    return [doc stringByAppendingPathComponent:file];
}

+(NSString *)checksumForIntValue:(NSInteger)value
{
    return [[NSString stringWithFormat:@"%@|%ld",[self getAdId],value] stringMD5];
}

#pragma mark - save report apns
+(void) setReportStatus:(BOOL)status forApnsCode:(NSString *)code
{
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    path = [NSString stringWithFormat:@"%@/%@",path,code];
    
    NSString *s = status?@"1":@"0";
    [s writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
}

+(BOOL) getReportStatusForApnsCode:(NSString *)code
{
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    path = [NSString stringWithFormat:@"%@/%@",path,code];
    
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}
#pragma mark - page

+(void)savePendingPage:(NSDictionary *)page
{
    NSMutableArray *marr = [[NSMutableArray alloc] initWithArray:[self getPendingPageList]];
    
    for (NSDictionary *page2 in marr) {
        if([page2[@"id_fanpage"] isEqualToString:page[@"id"]])
        {
            return;
        }
    }
    
    if([page[@"id"] length] == 0)
    {
        return;
    }
    
    NSInteger time = [[NSDate date] timeIntervalSince1970];
    NSInteger score = [page[@"score"] integerValue]?:0;
    
    NSDictionary *page2 = @{
                            @"id_fanpage":page[@"id"],
                            @"time":@(time),
                            @"score":@(score),
                            @"pageName": page[@"pageName"]?:@"untitled",
                            @"check":[self checksumForIntValue:time+score],
                            };
    
    [marr addObject:page2];
    BOOL save = [marr writeToFile:[self pathForFile:@"pendingPage"] atomically:YES];
    NSLog(@"Save file: %d",save);
}
+(void)deletePendingPage:(NSDictionary *)page
{
    NSMutableArray *marrApp = [[NSMutableArray alloc] initWithArray:[self getPendingPageList]] ;
    
    for (int i = 0; i<marrApp.count;i++)
    {
        NSMutableDictionary *page2 = [[NSMutableDictionary alloc] initWithDictionary:marrApp[i]];
        
        if([page2[@"id_fanpage"] isEqualToString:page[@"id_fanpage"]])
        {
            int now = [[NSDate date] timeIntervalSince1970];
            
            page2[@"score"]=@(0);
            page2[@"time"]=@(now);
            page2[@"check"]=[self checksumForIntValue:now];
            marrApp[i] = page2;
        }
    }
    
    [marrApp writeToFile:[self pathForFile:@"pendingPage"] atomically:YES];
}

+(void) remeovePendingPage
{
    NSMutableArray *marrPage = [[NSMutableArray alloc] initWithArray:[self getPendingPageList]];
    
    NSInteger now = [[NSDate date] timeIntervalSince1970];
    NSInteger twoWeeks = 60;
    for (int i =0; i<marrPage.count;i++)
    {
        NSDictionary *page = marrPage[i];
        
        NSInteger time = [page[@"time"] integerValue];
        if(now - time > twoWeeks && [page[@"score"] intValue]>0)
        {
            [marrPage removeObject:page];
            i--;
        }
    }
    
    [marrPage writeToFile:[self pathForFile:@"pendingPage"] atomically:YES];
}

+(NSArray *)getPendingPageList
{
    NSArray *arr = [[NSArray alloc] initWithContentsOfFile:[self pathForFile:@"pendingPage"]];
    return arr;
}
+(NSArray *)checkPageInstallAndAddPointWithInputPages:(NSArray *)arrLikes
{
    NSArray *arrPage = [self getPendingPageList];
    NSMutableArray *arrScore = [[NSMutableArray alloc] init];
    
    // check to add score...
    for (NSDictionary *page  in arrPage)
    {
        NSString *fanpageId = page[@"id_fanpage"];
        for(NSDictionary *page2 in arrLikes)
        {
            if([fanpageId isEqualToString:page2[@"id"]])
            {
                NSString *check = page[@"check"];
                NSInteger score = [page[@"score"] integerValue];
                NSInteger time = [page[@"time"] integerValue];
                NSString *valid = [self checksumForIntValue:time+score];
                
                if([valid isEqualToString:check] && score>0)
                {
                    [[HDVAppControll sharedInstance] reportScoreFromEvent:[@"like_fanpage" stringByAppendingFormat:@" %@",page[@"pageName"]] andScore:score  Complete:nil];
                    
                    
                    score += [self getBonusScore];
                    [self saveBonusScore:score];
                    [arrScore addObject:page];
                    [self deletePendingPage:page];
                }
            }
        }
    }
    
    arrPage = [self getPendingPageList];
    
    // remove too old pending app
    NSInteger now = [[NSDate date] timeIntervalSince1970];
    NSInteger twoWeeks = 200;
    for (NSDictionary *page in arrPage)
    {
        NSInteger time = [page[@"time"] integerValue];
        if(now - time >= twoWeeks)
        {
            [self deletePendingPage:page];
        }
    }
    
    return arrScore;
}

+(void) forceRemovePendingPage
{
    NSMutableArray *marrPage = [[NSMutableArray alloc] initWithArray:[self getPendingPageList]];
    
    for (int i =0; i<marrPage.count;i++)
    {
        NSDictionary *page = marrPage[i];
        
        NSInteger score = [page[@"score"] integerValue];
        
        if(score>0)
        {
            [marrPage removeObject:page];
            i--;
            
        }
        
    }
    
    [marrPage writeToFile:[self pathForFile:@"pendingPage"] atomically:YES];
}

#pragma mark - limit click ads

+(void) increaseClickCountForCurrentDay
{
    NSDictionary *info = [[NSUserDefaults standardUserDefaults] objectForKey:@"limitClick"];
    NSMutableDictionary *marrInfo = [[NSMutableDictionary alloc] init];
    if(info!=nil)
    {
        marrInfo = [[NSMutableDictionary alloc] initWithDictionary:info];
    }
    
    NSString *date = [self getCurrentServerDate];
    NSString *oldDate = [NSString stringWithFormat:@"%@",marrInfo[@"date"]];
    if(![date isEqualToString:oldDate])
    {
        marrInfo[@"count"] = @(0);
    }
    
    marrInfo[@"count"] = @([marrInfo[@"count"] intValue]+1);
    marrInfo[@"date"] = date;
    
    [[NSUserDefaults standardUserDefaults] setObject:marrInfo forKey:@"limitClick"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+(NSInteger) getClickCountForCurrentDay
{
    NSDictionary *info = [[NSUserDefaults standardUserDefaults] objectForKey:@"limitClick"];
    NSMutableDictionary *marrInfo = [[NSMutableDictionary alloc] init];
    if(info!=nil)
    {
        marrInfo = [[NSMutableDictionary alloc] initWithDictionary:info];
    }
    
    NSString *date = [self getCurrentServerDate];
    NSString *oldDate = [NSString stringWithFormat:@"%@",marrInfo[@"date"]];
    
    if(![date isEqualToString:oldDate])
    {
        marrInfo[@"count"] = @(0);
    }
    
    return [marrInfo[@"count"] intValue];
}

+(NSString *) getCurrentServerDate
{
    NSDate *date = [NSDate date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSString *sDate = [df stringFromDate:date];
    
    return [[HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"currentDate"] length]?[HDVAppControll sharedInstance].dictAppControll[@"system_score"][@"currentDate"]:sDate;
}


+(void)saveCountryList:(NSArray *)countryList
{
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0] stringByAppendingPathComponent:@"contry_list"];
    
    [countryList writeToFile:path atomically:YES];
}

+(NSArray *)getCountryList
{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"country_list" ofType:@"json"];
    NSArray *arr = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:path] options:NSJSONReadingAllowFragments error:nil];
    
    return arr;
}
#pragma mark - savetoken
+(void)saveApnsToken:(NSString *)token
{
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    path = [NSString stringWithFormat:@"%@/apns_token",path];
    
    if([token isKindOfClass:[NSString class]])
    {
        [token writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
}

+(NSString *)getApnsToken
{
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    path = [NSString stringWithFormat:@"%@/apns_token",path];
    
    NSString *token = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    return token;
}

#pragma mark - setting download cellular

+(void) saveSettingCellularOff:(BOOL)value
{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:@"cellularDownload"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+(BOOL) getSettingCellularOff
{
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"cellularDownload"]?:NO;
}



@end
