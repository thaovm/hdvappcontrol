//
//  ObjectScoreApp.h
//  HDVKit
//
//  Created by doduong on 6/17/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjectScoreApp : NSObject

@property (strong, nonatomic) NSString *appStoreId;
@property (strong, nonatomic) NSString *appId;
@property (strong, nonatomic) NSString *appName;
@property (strong, nonatomic) NSString *appDesc;
@property (strong, nonatomic) NSString *appMessage;
@property (strong, nonatomic) NSString *appIcon;
@property (strong, nonatomic) NSString *appAward;
@property (strong, nonatomic) NSString *appUrlScheme;
@property (strong, nonatomic) NSString *appInstallLink;
@property (strong, nonatomic) NSString *appThumbnailiPhone;
@property (strong, nonatomic) NSString *appThumbnailiPad;


-(instancetype) initWithHdvInfo:(NSDictionary *)info;
-(BOOL) appDidInstalled;
-(void) openApp;
-(BOOL) appDidOpened;
-(BOOL) appDidAwarded;
-(void) markAppAwarded;

-(void) markAppOnWait;
-(BOOL) appOnWait;

-(NSDictionary *) getDictionary;

@end















