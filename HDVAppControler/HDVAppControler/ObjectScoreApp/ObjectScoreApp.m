//
//  ObjectScoreApp.m
//  HDVKit
//
//  Created by doduong on 6/17/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "ObjectScoreApp.h"
#import <UIKit/UIKit.h>

@implementation ObjectScoreApp

-(instancetype) initWithHdvInfo:(NSDictionary *)info
{
    if(self = [super init])
    {
        _appAward = [[NSString alloc] initWithFormat:@"%@",info[@"score"]];
        _appDesc = [[NSString alloc] initWithFormat:@"%@",info[@"description"]];
        _appIcon = [[NSString alloc] initWithFormat:@"%@",info[@"icon"]];
        _appId = [[NSString alloc] initWithFormat:@"%@",info[@"id"]];
        _appInstallLink = [[NSString alloc] initWithFormat:@"%@",info[@"url_store"]];
        _appMessage = [[NSString alloc] initWithFormat:@"%@",info[@"message"]];
        _appName = [[NSString alloc] initWithFormat:@"%@",info[@"title"]];
        _appStoreId = [NSString stringWithFormat:@"%@",info[@"appid"]];
        _appThumbnailiPad = @"";
        _appThumbnailiPhone = @"";
        _appUrlScheme = [[NSString alloc] initWithFormat:@"%@",info[@"url_scheme"]];
    }
    
    return self;
}

-(NSDictionary *)getDictionary
{
    NSDictionary *info = @{
                           @"score":_appAward?:@"0",
                           @"icon":_appIcon?:@"0",
                           @"appid":_appStoreId?:@"0",
                           @"url_store":_appInstallLink?:@"0",
                           @"url_scheme":_appUrlScheme?:@"0",
                           };
    
    return info;
}

-(BOOL) appDidInstalled
{
    NSString *scheme1 = [[NSString alloc] initWithFormat:@"%@://",_appUrlScheme];
    NSString *scheme2 = [[NSString alloc] initWithFormat:@"://%@",_appUrlScheme];
    
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:scheme1]])
    {
        return YES;
    }
    else if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:scheme2]])
    {
        return YES;
    }
    else
    {
        return FALSE;
    }
}

-(void) openApp
{
    NSString *scheme1 = [[NSString alloc] initWithFormat:@"%@://",_appUrlScheme];
    
    if([self appDidInstalled])
    {
        NSString *fileName = [NSString stringWithFormat:@"openapp_%@",_appStoreId];
        NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        path = [path stringByAppendingPathComponent:fileName];
        
        [[NSFileManager defaultManager] createFileAtPath:path contents:[@"1" dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:scheme1]];
    
//    [NSThread detachNewThreadSelector:@selector(openAppOnThread) toTarget:self withObject:nil];
    
}

-(void) openAppOnThread
{
    NSString *scheme1 = [[NSString alloc] initWithFormat:@"%@://",_appUrlScheme];

    
    if([self appDidInstalled])
    {
        NSString *fileName = [NSString stringWithFormat:@"openapp_%@",_appStoreId];
        NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        path = [path stringByAppendingPathComponent:fileName];
        
        [[NSFileManager defaultManager] createFileAtPath:path contents:[@"1" dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:scheme1]];
}

-(BOOL) appDidOpened
{
    NSString *fileName = [NSString stringWithFormat:@"openapp_%@",_appStoreId];
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    path = [path stringByAppendingPathComponent:fileName];
    
    return [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:nil];
}


-(void) markAppAwarded
{
    NSString *fileName = [NSString stringWithFormat:@"markedApp_%@",_appStoreId];
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    path = [path stringByAppendingPathComponent:fileName];
    
    [[NSFileManager defaultManager] createFileAtPath:path contents:[@"1" dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
    
    // remove onwait app
    fileName = [NSString stringWithFormat:@"waitApp_%@",_appStoreId];
    path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    path = [path stringByAppendingPathComponent:fileName];
    
    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
}

-(BOOL) appDidAwarded
{
    NSString *fileName = [NSString stringWithFormat:@"markedApp_%@",_appStoreId];
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    path = [path stringByAppendingPathComponent:fileName];
    
    return [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:nil];
}

-(void) markAppOnWait
{
    NSString *fileName = [NSString stringWithFormat:@"waitApp_%@",_appStoreId];
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    path = [path stringByAppendingPathComponent:fileName];
    
    [[NSFileManager defaultManager] createFileAtPath:path contents:[@"1" dataUsingEncoding:NSUTF8StringEncoding] attributes:nil];
}

-(BOOL) appOnWait
{
    NSString *fileName = [NSString stringWithFormat:@"waitApp_%@",_appStoreId];
    NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    path = [path stringByAppendingPathComponent:fileName];
    
    return [[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:nil];
}

@end













