//
//  AppDelegate.m
//  HDVAppControler
//
//  Created by ThaoVM on 9/2/15.
//  Copyright (c) 2015 ThaoVM. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    _menuVC = [[MenuVC alloc] initWithNibName:@"BaseVC" bundle:nil];
    _homeVC = [[HomeVC alloc] initWithNibName:@"BaseVC" bundle:nil];

    _navMenu = [[UINavigationController alloc] initWithRootViewController:_menuVC];
    _navHome = [[UINavigationController alloc] initWithRootViewController:_homeVC];
    _sideMenu = [[MFSideMenuContainerViewController alloc] init];
    _sideMenu.leftMenuWidth = kWinsize.width*0.85>350?350:kWinsize.width*0.85;
    _sideMenu.centerViewController = _navHome;
    _sideMenu.leftMenuViewController = _navMenu;
    
    _window.rootViewController = _sideMenu;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [FBSession.activeSession handleDidBecomeActive];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [[HDVAppControll sharedInstance] checkAppControll:^(BOOL result) {
        
    }];
    [self checkInstalledAppForScore];
    [self checkLikedPage];
    [self checkRate];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}
-(void) checkInstalledAppForScore
{
    // code to migrate with old version
    NSArray *arr = [[HDVAdsManager sharedInstance] getPendingAppList];
    for(NSDictionary *app in arr)
    {
        ObjectScoreApp *osa = [[ObjectScoreApp alloc] initWithHdvInfo:app];
        if ([app[@"score"] intValue] == 0)
        {
            [osa markAppAwarded];
        }
    }
    
    return;
    
}
-(void) checkRate
{
    NSInteger time = [[[NSUserDefaults standardUserDefaults] objectForKey:@"rateTime"] intValue];
    NSInteger now = [[NSDate date] timeIntervalSince1970];
    if(now - time <10)
    {
        [UIAlertView showAlertViewWithMessage:TextForKey(@"RateFail") cancelButtonTitle:TextForKey(@"Close")];
        
        NSString *key = [NSString stringWithFormat:@"com.hdv.rated.%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"currentRateId"]];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey: key];
        [[NSUserDefaults standardUserDefaults] setObject:@"noscore" forKey:@"currentRate"];
        [[NSUserDefaults standardUserDefaults] setObject:@(0) forKey:@"rateTime"];
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"currentRateId"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        return;
    }
    
    
    
    NSDictionary *_dictScoreRate = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentRate"];
    
    if([_dictScoreRate isKindOfClass:[NSDictionary  class]])
    {
        NSInteger more = [_dictScoreRate[@"score"] intValue];
        NSInteger score = [HDVLocalSetting getBonusScore] + more;
        
        [HDVLocalSetting saveBonusScore:score];
        
        [[HDVAppControll sharedInstance] reportScoreFromEvent:[@"rate" stringByAppendingFormat:@" %@",_dictScoreRate[@"appName"]] andScore:[_dictScoreRate[@"score"] integerValue] Complete:nil];
        
        
        if(score < [HDVLocalSetting getScoreLimitToPro])
        {
            NSString *msg = @"";
            if([[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
            {
                msg = [NSString stringWithFormat:@"Thưởng %ld điểm!\nCảm ơn bạn đã đánh giá ứng dụng!",more];
            }
            else
            {
                msg = [NSString stringWithFormat:@"Earn %ld coins!\nThanks you for your support!",more];
            }
            [UIAlertView  showAlertViewWithMessage:msg cancelButtonTitle:TextForKey(@"Close")];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:@"noscore" forKey:@"currentRate"];
    }
}

-(void)checkLikedPage
{
    NSArray *arr = [HDVLocalSetting getPendingPageList];
    
    NSInteger score = 0;
    BOOL timeOut = NO;
    NSInteger now = [[NSDate date] timeIntervalSince1970];
    
    for(NSDictionary *info in arr)
    {
        score +=[info[@"score"] integerValue];
        
        timeOut  = (now - [info[@"time"] integerValue] >= 200);
    }
    
    if(timeOut)
    {
        [HDVLocalSetting  remeovePendingPage];
    }
    
    if(score >0 && !timeOut)
    {
        if(![FBHelper sharedHelper].fbSession.isOpen)
        {
            [UIAlertView showAlertViewWithMessage:TextForKey(@"NeedLoginFBToCheckLikePage") cancelButtonTitle:TextForKey(@"Cancel") otherButtonTitle:TextForKey(@"Ok") tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if(buttonIndex == 1)
                {
                    [self callCheckLikePage];
                }
                else
                {
                    [HDVLocalSetting forceRemovePendingPage];
                }
            }];
        }
        else
        {
            [self callCheckLikePage];
        }
    }
}

-(void) callCheckLikePage
{
    static BOOL onLoginFb = NO;
    
    if(onLoginFb)
        return;
    
    onLoginFb = YES;
    
    [[FBHelper sharedHelper] loginFBWithCacheComplete:^(BOOL result) {
        
        onLoginFb = NO;
        
        if(result)
        {
            [[FBHelper sharedHelper] getCurrentUserLikedPageComplete:^(NSArray *arrPages) {
                if(arrPages.count>0)
                {
                    NSArray *arrLikedPage = [HDVLocalSetting checkPageInstallAndAddPointWithInputPages:arrPages];
                    if(arrLikedPage.count>0)
                    {
                        NSInteger score = 0;
                        NSString *pageName = @"";
                        for (NSDictionary *page in arrLikedPage)
                        {
                            score +=[page[@"score"] integerValue];
                            pageName = [[pageName stringByAppendingString:page[@"pageName"]] stringByAppendingString:@","];
                        }
                        
                        pageName = [pageName substringToIndex:pageName.length-1];
                        
                        if(score < [HDVLocalSetting getScoreLimitToPro])
                        {
                            NSString *msg = @"";
                            
                            if([[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
                            {
                                msg = [NSString stringWithFormat:@"Thưởng %ld điểm!\nLike fanpage: %@",score,pageName];
                            }
                            else
                            {
                                msg = [NSString stringWithFormat:@"Earn %ld coins!\nLike fanpage: %@",score,pageName];
                            }
                            
                            [UIAlertView showAlertViewWithMessage:msg cancelButtonTitle:TextForKey(@"Close")];
                        }
                    }
                    else
                    {
                        NSString *msg = @"";
                        
                        if([[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
                        {
                            msg = [NSString stringWithFormat:@"Bạn chưa like page thành công!"];
                        }
                        else
                        {
                            msg = [NSString stringWithFormat:@"You have not like page(s)!"];
                        }
                        
                        [UIAlertView showAlertViewWithMessage:msg cancelButtonTitle:TextForKey(@"Close")];
                        
                        [HDVLocalSetting forceRemovePendingPage];
                    }
                }
            }];
        }
        else
        {
            [FBHelper sharedHelper].fbSession = nil;
            [HDVLocalSetting forceRemovePendingPage];
            return;
        }
        
    }];
    
}


-(void) getUserLikePage
{
    [[FBHelper sharedHelper] loginFBWithCacheComplete:^(BOOL result) {
        if(result)
        {
            [[FBHelper sharedHelper] getCurrentUserLikedPageComplete:^(NSArray *arrPages) {
                if(arrPages.count>0)
                {
                    NSArray *arrLikedPage = [HDVLocalSetting checkPageInstallAndAddPointWithInputPages:arrPages];
                    if(arrLikedPage.count>0)
                    {
                        NSInteger score = 0;
                        NSString *pageName = @"";
                        for (NSDictionary *page in arrLikedPage)
                        {
                            score +=[page[@"score"] integerValue];
                            pageName = [[pageName stringByAppendingString:page[@"pageName"]] stringByAppendingString:@","];
                        }
                        
                        pageName = [pageName substringToIndex:pageName.length-1];
                        
                        if(score < [HDVLocalSetting getScoreLimitToPro])
                        {
                            NSString *msg = @"";
                            
                            if([[[HDVLanguage sharedInstance] getAppLanguage] isEqualToString:@"VN"])
                            {
                                msg = [NSString stringWithFormat:@"Thưởng %ld điểm!\nLike fanpage: %@",score,pageName];
                            }
                            else
                            {
                                msg = [NSString stringWithFormat:@"Earn %ld coins!\nLike fanpage: %@",score,pageName];
                            }
                            
                            [UIAlertView showAlertViewWithMessage:msg cancelButtonTitle:TextForKey(@"Close")];
                        }
                    }
                }
            }];
        }
        else
        {
            return;
        }
    }];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.thaovm.HDVAppControler" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"HDVAppControler" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"HDVAppControler.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
