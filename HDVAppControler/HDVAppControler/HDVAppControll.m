//
//  HDVAppControll.m
//  HDVAppControler
//
//  Created by ThaoVM on 9/5/15.
//  Copyright (c) 2015 ThaoVM. All rights reserved.
//

#import "HDVAppControll.h"
#import "HDVLocalSetting.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "AFNetworkReachabilityManager.h"
#import "AFHTTPRequestOperationManager.h"
#import "HDVDownloadManager.h"
#import "HDVLocalData.h"
#import "ObjectScoreApp.h"
#import "NotifyAlertView.h"
#import "HDVAdsManager.h"
#define kHDVAppManagerPathForRegister @"http://ios.hdvietpro.com/ios-manager/register.php"
#define kAppControllURl @"http://ios.hdvietpro.com/ios-manager/control-ads.php"
#define kAppControllParamCode @"code"
#define kAppControllParamDateInstall @"date_install"

#define kDelayRemindShare  86400
#define kDelayRemindUpdate  172800

#define kAlertUpdateTag 1010
#define kAlertForcedUpdateTag 1011

@interface HDVAppControll (){
    NSDictionary * _dictCountry;
    NSMutableArray * _marrVideoQuality;
    BOOL _getConfig;
    
}

@end
@implementation HDVAppControll
+(instancetype) sharedInstance{
    static HDVAppControll *obj = NULL;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        if(![HDVLocalSetting getInstallDate]||[HDVLocalSetting getInstallDate].length == 0)
        {
            [HDVLocalSetting saveInstallDate];
            [HDVLocalSetting saveInstallDateV2];
            [HDVLocalSetting setInstallStamp];
            
//            NSLog(@"Updated date: %@ version: %@",[HDVLocalSetting getInstallDate],[HDVLocalSetting getCurrentVersion]);
        }
        
        [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:true];
        obj = [[HDVAppControll alloc] init];
        obj.dictAppControll = [HDVLocalSetting getAppControl];
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        
    });
    
    return obj;

}

-(instancetype)init
{
    if(self = [super init])
    {
        _dictCountry = [[NSDictionary alloc] initWithDictionary:[HDVLocalSetting getCountry]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleReachableWifi) name:AFNetworkingReachabilityDidChangeNotification object:nil];
        _getConfig = NO;
    }
    
    return self;
}




-(void)checkAppControll:(void (^)(BOOL))completionBlock{
    [[HDVAppControll sharedInstance] getUserCountryCompletion:^(NSDictionary *dictInfo) {
        
        if([[[HDVLanguage sharedInstance] getAppLanguage] length]==0)
        {
            NSString *countryCode = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
            countryCode.length==0?countryCode=@"VN":0;
            [[HDVLanguage sharedInstance] setAppLanguage:dictInfo[@"country"]?:countryCode];
        }
        if([HDVLocalSetting getCountry] == nil)
        {
            [HDVLocalSetting saveCountry:@{@"country":@"US",@"city":@"",@"org":@"hdv"}];
        }
        [self registerUserCompletion:^(BOOL result) {
            NSLog(@"Register complete: %d",result);
        }];
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
        {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        }
        else
        {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
        }
        
        [self checkAppControllCompletion:completionBlock];

        
    }];
    
}
-(void)checkAppControllCompletion:(void (^)(BOOL result))completionBlock{
    [[HDVAppControll sharedInstance] getAppcontrollInfoComletion:^(BOOL result) {
        NSLog(@"Have Appcontrol info");
        
        if(![HDVLocalSetting getFullVersion])
        {
            return;
        }
//        [self updateMp3ZingJSFile];
        // update STA...
        [[HDVAdsManager sharedInstance] prepareAds];

       

        
        if(result){
            
            NSDictionary *updateInfo = [[HDVAppControll sharedInstance] getAppControllAttribute:TNAppControllAttirbuteUpdate];
            
            NSString *version = [[HDVAppControll sharedInstance] getAttritbue:TNAppControllAttirbuteUpdateVersion inDictionary:updateInfo];
            
            if([HDVLocalSetting getFullVersion])
            {
                completionBlock(result);
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [HDVLocalSetting suggestUserShare];
                });
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [HDVLocalSetting suggestUserSubscribeFBPage];
                });
            }
            
            int status = [[self getAttritbue:TNAppControllAttirbuteUpdateStatus inDictionary:updateInfo] intValue];
            if(status==1)
            {
                if([HDVLocalSetting getIgnoreUpdateVersion:version])
                {
                    return;
                }
                
                int lastRemind = [HDVLocalSetting getRemindUpdateTimeForVersion:version];
                int now = [[NSDate date] timeIntervalSince1970];
                
                NSInteger offset = [[HDVAdsManager sharedInstance] getTimeOffsetShowUpdate];
                
                
                if(now - lastRemind >= offset)
                {
                    NSString *title = [[HDVAppControll sharedInstance] getAttritbue:TNAppControllAttirbuteUpdateTitle inDictionary:updateInfo];
                    NSString *desc = [[HDVAppControll sharedInstance] getAttritbue:TNAppControllAttirbuteUpdateDesc inDictionary:updateInfo];
                    
                    [HDVLocalSetting setRemindUpdateTime:[[NSDate date] timeIntervalSince1970] forVersion:version];
                    
                    [NotifyAlertView showAlertWithTitle:title Message:desc AllowClose:YES ActionTitle:TextForKey(@"Update") CloseBlock:^{
                    } ActionBlock:^{
                        NSString *url = [[HDVAppControll sharedInstance] getAttritbue:TNAppControllAttirbuteUpdateUrl inDictionary:updateInfo];
                        
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
                    }];
                }
                else
                {
                    return;
                }
            }
            else if(status ==2)
            {
                NSString *title = [[HDVAppControll sharedInstance] getAttritbue:TNAppControllAttirbuteUpdateTitle inDictionary:updateInfo];
                NSString *desc = [[HDVAppControll sharedInstance] getAttritbue:TNAppControllAttirbuteUpdateDesc inDictionary:updateInfo];
                
                [NotifyAlertView showAlertWithTitle:title Message:desc AllowClose:NO ActionTitle:TextForKey(@"Update") CloseBlock:nil ActionBlock:^{
                    NSDictionary *updateInfo = [[HDVAppControll sharedInstance] getAppControllAttribute:TNAppControllAttirbuteUpdate];
                    NSString *url = [[HDVAppControll sharedInstance] getAttritbue:TNAppControllAttirbuteUpdateUrl inDictionary:updateInfo];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
                }];
            }
            else
            {
                NSString *version = [HDVAppControll sharedInstance].dictAppControll[@"last_version"];
                if([version isKindOfClass:[NSString class]])
                {
                    if(version.length>0)
                    {
                        NSString *key = [NSString stringWithFormat:@"suggestUpdate_%@",version];
                        NSInteger count = [[NSUserDefaults standardUserDefaults] integerForKey:key];
                        
                        if(count < 2)
                        {
                            [[NSUserDefaults standardUserDefaults] setInteger:count+1 forKey:key];
                            
                            if(![[HDVLocalSetting getCurrentVersion] isEqualToString:version])
                            {
                                NSString *title = TextForKey(@"SuggestNewUpdate");
                                NSString *desc = TextForKey(@"IntroNewUpdate");
                                [NotifyAlertView showAlertWithTitle:title Message:desc AllowClose:YES ActionTitle:TextForKey(@"Update") CloseBlock:nil ActionBlock:^{
                                    
                                    NSString *url = [NSString stringWithFormat:@"https://itunes.apple.com/app/id%@",[[HDVAdsManager sharedInstance] getAppleStoreId]?:kStoreId];
                                    
                                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
                                }];
                            }
                        }
                        
                    }
                }
            }
        }
        
    }];

}





-(void)handleReachableWifi{
    if(!_getConfig){
        _getConfig = YES;
        if(_blockReachableWifi){
            _blockReachableWifi();
            _blockReachableWifi = nil;
            _getConfig = NO;
        }
    }
}

-(void) getAppcontrollInfoComletion:(void (^)(BOOL result))completionBlock
{
    
    self.dictAppControll = [HDVLocalSetting getAppControl];
    
    if(self.dictAppControll) // pre set some value....
    {
        if([_dictAppControll[@"publish"] intValue]==1)
        {
            [HDVLocalSetting setFullVersion:YES];
        }
    }
    
    NSString *vip = [HDVLocalSetting getDisableShowAd]?@"1":@"0";
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:_dictCountry options:NSJSONWritingPrettyPrinted error:nil];
    NSString *s = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString *encode = [self simpleEncode:s];
    
    NSDictionary *param = @{
                            kAppControllParamCode:kAppId,
                            kAppControllParamDateInstall:[HDVLocalSetting getInstallDate],
                            @"time_install":[HDVLocalSetting getInstallDateV2],
                            @"version":[HDVLocalSetting getCurrentVersion],
                            @"vip":vip,
                            @"deviceID":[HDVLocalSetting getAdId],
                            @"info":encode,
                            @"publish":[HDVLocalSetting getFullVersion]?@"1":@"0",
                            @"wifi":[AFNetworkReachabilityManager sharedManager].reachableViaWiFi?@"1":@"0"
                            };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html",@"application/json"]];
    [manager GET:kAppControllURl parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        _dictAppControll = [[NSDictionary alloc] initWithDictionary:responseObject];
        
        NSDictionary *listAds = _dictAppControll[@"list_ads"];
        
        NSString *status = [NSString stringWithFormat:@"%@",_dictAppControll[@"thumbai"][@"status"]];
        if([status intValue] == 1)
        {
            for(NSDictionary *ads in listAds)
            {
                if([ads isKindOfClass:[NSDictionary class]])
                {
                    NSString *scheme1 = [NSString stringWithFormat:@"://%@",ads[@"url_scheme"]];
                    NSString *scheme2 = [NSString stringWithFormat:@"%@://",ads[@"url_scheme"]];
                    
                    if(![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:scheme1]] && ![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:scheme2]])
                    {
                        if(![HDVLocalSetting getDisableShowAd] && [HDVLocalSetting getFullVersion])
                        {
//                            _needShowThumbnail = YES;
                            [[NSNotificationCenter defaultCenter] postNotificationName:kAppControllHaveThumbnail object:nil userInfo:nil];
                        }
                        break;
                    }
                }
            }
        }
        
        if([_dictAppControll[@"publish"] intValue]==1)
        {
            [HDVLocalSetting setFullVersion:YES];
        }
        
        if([HDVLocalSetting getScoreLimit] < 30000)
        {
            NSInteger score = [_dictAppControll[@"system_score"][@"score_convert_pro"] integerValue]?:30000;
            [HDVLocalSetting saveScoreLimit:score];
        }
        
        //#if !DEBUG
        
        NSDictionary *dictPayment = [self getPaymentInfo];
        if([dictPayment[@"pro"] intValue] == 1) // proversion
        {
            [HDVLocalSetting setDisableShowAd:YES];
        }
        else
        {
            BOOL isPro = [HDVLocalSetting getDisableShowAd];
            
            if(isPro)
            {
                [HDVLocalSetting saveBonusScore:0];
            }
            
            [HDVLocalSetting setDisableShowAd:NO];
        }
        
        //#endif
        //[[HDVAdsManager sharedInstance] loadScorePopup];
        
        NSString *urlScheme = _dictAppControll[@"exit_apps"][@"url_scheme"];
        NSString *scheme1 = [NSString stringWithFormat:@"://%@",urlScheme];
        NSString *scheme2 = [NSString stringWithFormat:@"%@://",urlScheme];
        
        if(![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:scheme1]] && ![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:scheme2]] )
        {
            self.dictCloseAppAdd = _dictAppControll[@"exit_apps"];
        }
        
        [HDVLocalSetting saveAppControl:_dictAppControll];
        
        if(completionBlock)
            completionBlock(YES);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kHDVACM_Update object:nil];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"getAppcontrollInfoComletion Error: %@",error);
        if(completionBlock)
            completionBlock(NO);
    }];
    
}
-(void) sendAPNSToken:(NSString *)token Complete:(void (^)(BOOL))completionBlock
{
    NSData *data = [NSJSONSerialization dataWithJSONObject:_dictCountry options:NSJSONWritingPrettyPrinted error:nil];
    NSString *s = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString *encode = [self simpleEncode:s];
    
    NSDictionary *param = @{
                            @"token_id":token,
                            @"code":kAppId,
                            @"deviceID":[HDVLocalSetting getAdId],
                            @"info":encode,
                            };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html",@"application/json"]];
    
    [manager POST:@"http://ios.hdvietpro.com/ios-manager/push_notification/register_token.php" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            BOOL result = [responseObject[@"status"] intValue]==1;
            
            completionBlock?completionBlock(result):0;
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%s Fail: %@",__FUNCTION__,error);
        completionBlock?completionBlock(false):0;
    }];
}



#pragma mark - mp3 control

-(void)getMP3ConfigComplete:(void(^)())completeBlock;
{
    if(!_marrVideoQuality)
    {
        _marrVideoQuality = [[NSMutableArray alloc] init];
    }
    else
    {
        [_marrVideoQuality removeAllObjects];
    }
    
   
    
    NSString *vip = [HDVLocalSetting getDisableShowAd]?@"1":@"0";
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:_dictCountry options:NSJSONWritingPrettyPrinted error:nil];
    NSString *s = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString *encode = [self simpleEncode:s];
    
    NSDictionary *param = @{
                            kAppControllParamCode:kAppId,
                            kAppControllParamDateInstall:[HDVLocalSetting getInstallDate],
                            @"time_install":[HDVLocalSetting getInstallDateV2],
                            @"version":[HDVLocalSetting getCurrentVersion],
                            @"vip":vip,
                            @"deviceID":[HDVLocalSetting getAdId],
                            @"info":encode,
                            };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html",@"application/json"]];
    [manager GET:@"http://mp3.hdvietpro.com/mp3_zing_new/config.php" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            [[HDVDownloadManager sharedInstance]setDicInfoDownloadData:responseObject];
        }
        
        if(completeBlock)
        {
            completeBlock();
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%s: %@",__FUNCTION__,error);
        
        if(completeBlock)
        {
            completeBlock();
        }
        
    }];
}

-(void) getUserCountryCompletion:(void(^)(NSDictionary *dictInfo))completionBlock
{
//    HDVAppControll * reself = self;
    self.blockReachableWifi = ^{
        if(_dictCountry != nil && _dictCountry.allKeys.count>0)
        {
            if(completionBlock)
            {
                completionBlock(_dictCountry);
            }
            
            return;
        }
        
        NSString *path = @"https://ipinfo.io/json?token=78de68ddee312b";
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html", nil];
        
        [manager GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            if([responseObject isKindOfClass:[NSDictionary class]])
            {
                _dictCountry = [[NSDictionary alloc] initWithDictionary:responseObject];
                
                if([_dictCountry[@"country"] length]>0)
                {
                    [HDVLocalSetting saveCountry:_dictCountry];
                }
                
                if(completionBlock)
                {
                    completionBlock(responseObject);
                }
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            NSLog(@"getUserCountryCompletion Fail: %@",error);
            NSString *path2 = @"http://ipinfo.io/json";
            
            [manager GET:path2 parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                if([responseObject isKindOfClass:[NSDictionary class]])
                {
                    _dictCountry = [[NSDictionary alloc] initWithDictionary:responseObject];
                    
                    if([_dictCountry[@"country"] length]>0)
                    {
                        [HDVLocalSetting saveCountry:_dictCountry];
                    }
                    
                    if(completionBlock)
                    {
                        completionBlock(responseObject);
                    }
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                completionBlock?completionBlock(nil):0;
            }];
            
        }];
    };
    if(_getConfig){
        self.blockReachableWifi();
        self.blockReachableWifi = nil;
        _getConfig = NO;
    }
    
}

#pragma mark - register user

-(void)registerUserCompletion:(void (^)(BOOL))completionBlock
{
    if([HDVLocalData getBoolForKey:eLocalFileKeyUserRegister appendKey:@""])
    {
        completionBlock = Nil;
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html", nil];
    
    NSData *data = [NSJSONSerialization dataWithJSONObject:_dictCountry options:NSJSONWritingPrettyPrinted error:nil];
    NSString *s = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString *encode = [self simpleEncode:s];
    
    NSDictionary *param = @{
                            @"deviceID":[HDVLocalSetting getAdId],
                            @"deviceID_old":[HDVLocalSetting getUUID],
                            @"phone_name":[HDVLocalSetting getDeviceType],
                            @"os_version":[UIDevice currentDevice].systemVersion,
                            @"network":[HDVLocalSetting getCarrier],
                            @"code":kAppId,
                            @"info":encode,
                            };
    
    [manager POST:kHDVAppManagerPathForRegister parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"registerUserCompletion success: %@",responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            if([responseObject[@"status"] intValue]==1)
            {
                [HDVLocalData saveBool:YES forKey:eLocalFileKeyUserRegister appendKey:@""];
                completionBlock?completionBlock(true):0;
                
                NSString *timeStamp = [NSString stringWithFormat:@"%@",responseObject[@"timestamp"]];
                NSString *date = [NSString stringWithFormat:@"%@",responseObject[@"date_install"]];
                NSString *timeInstall = [NSString stringWithFormat:@"%@",responseObject[@"time_install"]];
                [HDVLocalSetting setInstallStamp:[timeStamp integerValue]];
                [HDVLocalSetting saveInstallDate:date];
                [HDVLocalSetting saveInstallDateV2:timeInstall];
            }
            else
            {
                completionBlock?completionBlock(false):0;
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"registerUserCompletion fail: %@",error);
        completionBlock?completionBlock(false):0;
    }];
}

-(void) getSMSContentComplete:(void(^)(NSString *sms))completeBlock
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html", nil];
    
    NSDictionary *param = @{
                            @"deviceID":[HDVLocalSetting getAdId],
                            @"code":kAppId,
                            };
    [manager POST:@"http://ios.hdvietpro.com/ios-manager/get_key_sms.php" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            
            if([responseObject[@"key"] isKindOfClass:[NSNull class]])
            {
                completeBlock(nil);
                return;
            }
            
            NSString *sms = [NSString stringWithFormat:@"%@",responseObject[@"key"]];
            
            if(completeBlock && [sms length]>0)
            {
                completeBlock(sms);
            }
            else if(completeBlock)
            {
                completeBlock(nil);
            }
        }
        else
        {
            if(completeBlock)
            {
                completeBlock(nil);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"getSMSContentComplete fail: %@",error);
        if(completeBlock)
        {
            completeBlock(nil);
        }
    }];
}
-(void) reportProByScoreComplete:(void (^)(bool))completeBlock
{
    NSDictionary *param = @{
                            @"code":kAppId,
                            @"deviceID":[HDVLocalSetting getAdId],
                            @"amount":@([HDVLocalSetting getBonusScore]),
                            };
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html",@"application/json"]];
    [manager POST:@"http://ios.hdvietpro.com/ios-manager/upgrade-pro-buy-score.php" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%s Succes: %@",__FUNCTION__,responseObject);
        completeBlock?completeBlock(true):0;
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%s Fail: %@",__FUNCTION__,error);
        completeBlock?completeBlock(false):0;
    }];
    
}

-(void) reportPushNotifyCode:(NSString *)code NotifyToken:(NSString *)token Complete:(void (^)())completionBlock
{
    if([HDVLocalSetting getReportStatusForApnsCode:code])
    {
        return;
    }
    
    if(token == nil)
    {
        return;
    }
    
    NSDictionary *param = @{
                            @"code":code,
                            @"device_id":[HDVLocalSetting getAdId],
                            @"token_id":token,
                            @"app_id":kAppId,
                            };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html",@"application/json"]];
    
    [manager POST:@"http://ios.hdvietpro.com/service_push/client_push_response.php" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%s success: %@",__FUNCTION__,responseObject);
        
        if([responseObject isKindOfClass:[NSDictionary class]])
        {
            if([responseObject[@"result"] isKindOfClass:[NSDictionary class]])
            {
                NSString *status = [NSString stringWithFormat:@"%@",responseObject[@"result"][@"status"]];
                
                if([status intValue] == 1)
                {
                    [HDVLocalSetting setReportStatus:YES forApnsCode:code];
                }
            }
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%s fail: %@",__FUNCTION__,error);
    }];
}



-(id)getAppControllAttribute:(TNAppControllAttirbute)attName
{
    switch (attName)
    {
        case TNAppControllAttirbuteUpdate:
            return _dictAppControll[@"update"];
            break;
        case TNAppControllAttirbuteAdsNetwork:
            return _dictAppControll[@"ads_network"];
            break;
        case TNAppControllAttirbuteListApps:
            return _dictAppControll[@"list_apps"];
            break;
        case TNAppControllAttirbuteExitApps:
            return _dictAppControll[@"exit_apps"];
            break;
        case TNAppControllAttirbuteRemoveAd:
            return _dictAppControll[@"uninstall_ads"];
        case  TNAppControllAttirbuteOffsetShowAds:
            return _dictAppControll[@"offset_show_ads"];
        case  TNAppControllAttirbuteOffsetShowApp:
            return _dictAppControll[@"offset_show_apps"];
        default:
            return nil;
            break;
    };
}
-(id)getAttritbue:(TNAppControllAttirbute)attName inDictionary:(NSDictionary *)dict
{
    switch (attName)
    {
        case TNAppControllAttirbuteExitAppsImage:
            return dict[@"img"];
            break;
        case TNAppControllAttirbuteExitAppsUrl:
            return dict[@"url"];
            break;
        case TNAppControllAttirbuteAppButton1:
            return dict[@"button_1"];
            break;
        case TNAppControllAttirbuteAppButton2:
            return dict[@"button_2"];
            break;
        case TNAppControllAttirbuteAppUrl:
            return dict[@"store_url"];
            break;
        case TNAppControllAttirbuteAppDesc:
            return dict[@"description"];
        case TNAppControllAttirbuteAppTitle:
            return dict[@"title"];
        case TNAppControllAttirbuteAppUrlScheme:
            return dict[@"url_scheme"];
        case TNAppControllAttirbuteUpdateDesc:
            return dict[@"description"];
        case TNAppControllAttirbuteUpdateStatus:
            return dict[@"status"];
        case TNAppControllAttirbuteUpdateTitle:
            return dict[@"title"];
        case TNAppControllAttirbuteUpdateUrl:
            return dict[@"url"];
        case TNAppControllAttirbuteUpdateVersion:
            return dict[@"version"];
        default:
            return nil;
            break;
    }
}




-(NSDictionary *)getPaymentInfo
{
    return _dictAppControll[@"payment"];
}

-(NSInteger)getProPrice
{
    NSDictionary *payment = [self getPaymentInfo];
    if([payment[@"amonut"] isKindOfClass:[NSNull class]])
    {
        return -1;
    }
    
    return [payment[@"amonut"] integerValue];
}


#pragma mark - simple encode
-(NSString *)simpleEncode:(NSString *)input
{
    NSString *s = @"";
    
    for(int i = 0; i<input.length;i++)
    {
        int asciValue = [input characterAtIndex:i];
        if(asciValue>=65 && asciValue<126)
        {
            asciValue++;
            s = [s stringByAppendingFormat:@"%c",asciValue];
        }
        else
        {
            s = [s stringByAppendingFormat:@"%c",[input characterAtIndex:i]];
        }
    }
    
    return s;
}


#pragma mark - get appinfor for remote notify

-(void) getAppInfoForNotify:(NSString *)appId Complete:(void (^)(NSObject *))completionBlock
{
    //    https://api.bigcoin.vn/api/get_info_app.php
    
    NSDictionary *param = @{
                            @"appid":appId,
                            };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html", nil];
    manager.securityPolicy.validatesDomainName = YES;
    
    
    [manager POST:@"https://api.bigcoin.vn/api/get_info_app.php" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        ObjectScoreApp *app = [[ObjectScoreApp alloc] initWithHdvInfo:responseObject];
        
        completionBlock?completionBlock(app):0;
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"%s: %@",__FUNCTION__,error);
        
        completionBlock?completionBlock(nil):0;
        
    }];
    
}

-(void) reportScoreFromEvent:(NSString *)eventName andScore:(NSInteger)score Complete:(void (^)())completionBlock
{
    //    http://ios.hdvietpro.com/ios-manager/post-back-bonus-coin.php
    
    NSDictionary *param = @{
                            @"deviceID":[HDVLocalSetting getAdId],
                            @"code":kAppId,
                            @"event":eventName,
                            @"score":@(score),
                            };
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"text/html",@"application/json"]];
    
    [manager POST:@"http://ios.hdvietpro.com/ios-manager/post-back-bonus-coin.php" parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"%s: %@",__FUNCTION__,responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"%s: %@",__FUNCTION__,error);
        
    }];
}

//

//-(void)updateMp3ZingJSFile{
////    http://linode1-sg.hdvietpro.com/mp3zing.js
//    NSString *stringURL = @"http://linode1-sg.hdvietpro.com/mp3zing.js";
//    NSURL  *url = [NSURL URLWithString:stringURL];
//    NSData *urlData = [NSData dataWithContentsOfURL:url];
//    if ( urlData )
//    {
//        NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString  *documentsDirectory = [paths objectAtIndex:0];
//        
//        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"mp3zing.js"];
//        [urlData writeToFile:filePath atomically:YES];
//    }
//}

@end
