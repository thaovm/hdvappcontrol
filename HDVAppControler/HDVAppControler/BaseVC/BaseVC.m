//
//  BaseVC.m
//  tubemate
//
//  Created by doduong on 2/9/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "BaseVC.h"
#import "MenuVC.h"
#import "UIView+Toast.h"
@interface BaseVC (){
    CGRect _frameViewBottom;
}

@end

@implementation BaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _frameViewBottom = _viewBottom.frame;

    
    [_cvMain registerNib:[UINib nibWithNibName:@"BaseCVCell" bundle:nil] forCellWithReuseIdentifier:@"BaseCVCell"];
    [_cvMain registerNib:[UINib nibWithNibName:@"BaseCVHeader" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"BaseCVHeader"];
    [_cvMain registerNib:[UINib nibWithNibName:@"BaseCVHeader" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"BaseCVHeader"];

    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleDeviceNotify:) name:UIDeviceOrientationDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLSNotify:) name:kLSNotityChangeScoreToPro object:nil];
    [self setupNavItem];

}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAdsNotify:) name:kHDVAdsNotifyHaveBanner object:nil];
    [[HDVAdsManager sharedInstance] showAds];
    [self layoutForAds];
    [_cvMain reloadData];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
//    [[HDVAdsManager sharedInstance] showMobileCoreInViewController:self];
    if([self isKindOfClass:[MenuVC class]])
    {
        return;
    }
    
}

-(void) viewWillDisappear:(BOOL)animated
{
//    [[HDVAdsManager sharedInstance] hideMobileCore];
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kHDVAdsNotifyHaveBanner object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) loadMore
{
    ;
}
-(void) refresh
{
    ;
}

-(void) layoutForAds
{
    ;
}

-(void)setContentForViewBottom:(UIView *)viewContent
{
    if(viewContent!=nil)
    {

        float heighBanner = [[HDVAdsManager sharedInstance] bannerBottomIsReady]?kAdHeigh:0;
        float heigh = heighBanner + viewContent.bounds.size.height;

        _viewBottom.frame = CGRectMake(0, self.view.frame.size.height - heigh, self.view.frame.size.width, heigh);
        
        CGRect frameCVMain = _cvMain.frame;
        frameCVMain.size.height = self.view.frame.size.height - heigh;
        _cvMain.frame = frameCVMain;
        _viewBottom.hidden = NO;
        
        [viewContent setFrame:CGRectMake(0, heighBanner, _viewBottom.frame.size.width, heigh - heighBanner)];
        [_viewBottom addSubview:viewContent];
        if([[HDVAdsManager sharedInstance]bannerBottomIsReady]){
            UIView * banner = [[HDVAdsManager sharedInstance] getCurrentBannerBottom];
            CGRect frameBanner = banner.frame;
            frameBanner.origin.x = (_viewBottom.frame.size.width - frameBanner.size.width)/2;
            frameBanner.origin.y = 0;
            banner.frame = frameBanner;
            [_viewBottom addSubview:banner];
        }
        
    }
    else
    {
        _viewBottom.hidden = YES;
    }
}

-(void)updateTitle
{

    self.title = TextForKey(_titleKey);
}


#pragma mark - nav action
-(void)setupNavItem
{
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [btn setBackgroundImage:[UIImage imageNamed:@"search_offline77"] forState:UIControlStateNormal];
    btn.adjustsImageWhenHighlighted = NO;
    btn.showsTouchWhenHighlighted = YES;
    [btn addTarget:self action:@selector(clickSearch) forControlEvents:UIControlEventTouchUpInside];
    btn.adjustsImageWhenHighlighted = NO;
    btn.adjustsImageWhenDisabled = NO;
    btn.showsTouchWhenHighlighted = YES;
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    [self.navigationItem setRightBarButtonItem:item];
    
    if([self.navigationController.viewControllers count] > 1 && !_btLeftMenu)
    {
        UIButton *btnBack = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 12, 20)];
        [btnBack setBackgroundImage:[UIImage imageNamed:@"btn_nav_back77"] forState:0];
        [btnBack addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
        
        btnBack.adjustsImageWhenHighlighted = NO;
        btnBack.adjustsImageWhenDisabled = NO;
        btnBack.showsTouchWhenHighlighted = YES;
        
        UIBarButtonItem *itemMenu = [[UIBarButtonItem alloc] initWithCustomView:btnBack];
        self.navigationItem.leftBarButtonItem = itemMenu;
    }
    else
    {
        UIButton *btnMenu = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 25)];
        btnMenu.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
        [btnMenu setImage:[UIImage imageNamed:@"menu77"] forState:0];
        [btnMenu addTarget:self action:@selector(clickMenu) forControlEvents:UIControlEventTouchUpInside];
        btnMenu.adjustsImageWhenHighlighted = NO;
        btnMenu.adjustsImageWhenDisabled = NO;
        btnMenu.showsTouchWhenHighlighted = YES;
        
        UIBarButtonItem *itemMenu = [[UIBarButtonItem alloc] initWithCustomView:btnMenu];
        
        self.navigationItem.leftBarButtonItem = itemMenu;
    }
}

-(void) clickMenu
{
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [app.sideMenu toggleLeftSideMenuCompletion:nil];
}

-(void) clickSearch
{
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
//    [SearchView showInView:app.window WithMode:eSearchViewModeOnline];
}

-(void) clickBack
{
    [self.navigationController popViewControllerAnimated:YES];    
}

#pragma mark - Orientation
-(BOOL)shouldAutorotate
{
    return true;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

#pragma mark - handle notify

-(void)handleLSNotify:(NSNotification *)notify
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        if([notify.name isEqualToString:kLSNotityChangeScoreToPro])
        {
            CGRect frame = _cvMain.frame;
            frame.size.height = self.view.frame.size.height;
            _cvMain.frame = frame;
            
            [UIAlertView showAlertViewWithMessage:TextForKey(@"RemoveAdsByScore") cancelButtonTitle:TextForKey(@"Close")];
            [[HDVAppControll sharedInstance] reportProByScoreComplete:^(bool result){
                ;
            }];
        }
    });
}

-(void)handleDeviceNotify:(NSNotification *)notify
{
    if([notify.name isEqualToString:UIDeviceOrientationDidChangeNotification])
    {
        if([UIDevice currentDevice].orientation == UIDeviceOrientationPortraitUpsideDown||[UIDevice currentDevice].orientation == UIDeviceOrientationFaceDown||[UIDevice currentDevice].orientation == UIDeviceOrientationFaceUp)
        {
            return;
        }
    }
}

-(void) handleAdsNotify:(NSNotification *)notify
{
    if([notify.name isEqualToString:kHDVAdsNotifyHaveBanner])
    {
        [self layoutForAds];
    }
}

#pragma mark - UICollectionView
-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    if([[HDVAdsManager sharedInstance] bannerIsReady] && section == [self numberOfSectionsInCollectionView:collectionView]-1)
    {
        return CGSizeMake(kWinsize.width, kAdHeigh);
    }
    else
    {
        return CGSizeZero;
    }
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if([kind isEqualToString:UICollectionElementKindSectionFooter])
    {
        BaseCVHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"BaseCVHeader" forIndexPath:indexPath];
        
        [header addThumbnai];
        
        return header;
    }
    
    return nil;
}





@end




