//
//  BaseCVCell.m
//  MegaPlayer
//
//  Created by doduong on 3/25/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "BaseCVCell.h"

@implementation BaseCVCell

- (void)awakeFromNib {
    // Initialization code
    _orginBgColor = self.backgroundColor;
}

-(void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
    selected?(self.backgroundColor = kColor90):(self.backgroundColor = [UIColor whiteColor]);
}

-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];

    highlighted?(self.backgroundColor = kColor90):(self.backgroundColor = [UIColor whiteColor]);
}


@end
