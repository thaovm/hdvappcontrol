//
//  BaseVC.h
//  tubemate
//
//  Created by doduong on 2/9/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCVCell.h"
#import "BaseCVHeader.h"
#import "StretchyHeaderFlowLayout.h"

@interface BaseVC : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (assign, nonatomic)BOOL btLeftMenu;
@property (weak, nonatomic) IBOutlet UIView *viewBottom;
@property (weak, nonatomic) IBOutlet UICollectionView *cvMain;
@property (strong, nonatomic) NSString *titleKey;

-(void)updateTitle;

-(void) setupNavItem;
-(void) loadMore;
-(void) refresh;
-(void) layoutForAds;

-(void)clickMenu;
-(void)clickSearch;
-(void) clickBack;

-(void)setContentForViewBottom:(UIView *)viewContent;
-(void)setCVBackground:(NSArray *)dataSource;

@end
