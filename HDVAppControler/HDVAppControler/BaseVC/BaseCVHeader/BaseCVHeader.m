//
//  BaseCVHeader.m
//  Mp3Player
//
//  Created by doduong on 3/27/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "BaseCVHeader.h"

@implementation BaseCVHeader

- (void)awakeFromNib {
    // Initialization code
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addThumbnai) name:kHDVAdsNotifyHaveBanner object:nil];
//    
//    if([HDVAdsManager sharedInstance].haveThumbnai)
//    {
//        [self addThumbnai];
//    }
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)addBanner
{

    
    UIView *banner = [[HDVAdsManager sharedInstance] getCurrentBanner];
    CGRect frame = banner.frame;
    frame.origin.x = (self.frame.size.width - frame.size.width)*0.5;
    frame.origin.y = self.frame.size.height - banner.frame.size.height;
    banner.frame = frame;
    banner.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    
    [self addSubview:banner];
}
-(void)addThumbnai
{
    
    UIView *thumbnai = [[HDVAdsManager sharedInstance] getCurrentThumbnai];
    CGRect frame = thumbnai.frame;
    frame.origin.x = (self.frame.size.width - frame.size.width)*0.5;
    frame.origin.y = self.frame.size.height - thumbnai.frame.size.height;
    thumbnai.frame = frame;
    thumbnai.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    thumbnai.hidden = NO;
    [self addSubview:thumbnai];
}
@end
