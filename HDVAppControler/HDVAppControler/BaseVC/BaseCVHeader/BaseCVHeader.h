//
//  BaseCVHeader.h
//  Mp3Player
//
//  Created by doduong on 3/27/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseCVHeader : UICollectionReusableView

-(void) addBanner;
-(void) addThumbnai;
@end
