//
//  StretchyHeaderFlowLayout.m
//  Mp3Player
//
//  Created by doduong on 3/27/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "StretchyHeaderFlowLayout.h"

@implementation StretchyHeaderFlowLayout

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return YES;
}

-(UICollectionViewScrollDirection)scrollDirection
{
    return UICollectionViewScrollDirectionVertical;
}

-(NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    UICollectionView *cv = [self collectionView];
    UIEdgeInsets insets = [cv contentInset];
    CGPoint offset = [cv contentOffset];
    CGFloat minY = -insets.top;
    
    NSArray *attributes = [super layoutAttributesForElementsInRect:rect];
    
    if(offset.y < minY)
    {
        CGFloat deltaY = fabsf(offset.y - minY);
        for(UICollectionViewLayoutAttributes *att in attributes)
        {
            NSString *kind = [att representedElementKind];
            if([kind isEqualToString:UICollectionElementKindSectionHeader])
            {
                CGRect headerRect = [att frame];
                CGSize headerSize = headerRect.size;
                
                headerRect.size.height = MAX(minY, headerSize.height+deltaY);
                headerRect.origin.y = headerRect.origin.y - deltaY;
                [att setFrame:headerRect];
                break;
            }
        }
    }
    
    return attributes;
}

-(CGFloat)minimumInteritemSpacing
{
    return 0;
}

-(CGFloat)minimumLineSpacing
{
    return 0;
}


@end
