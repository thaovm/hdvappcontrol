//
//  HDVAdsPopup.m
//  Mp3Player
//
//  Created by ThaoVM on 9/12/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "HDVAdsPopup.h"
#import "UIImageView+AFNetworking.h"
@interface HDVAdsPopup ()

@end

@implementation HDVAdsPopup

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [[UIApplication sharedApplication] setStatusBarHidden:YES
                                                withAnimation:UIStatusBarAnimationFade];
    }else{
        [UIApplication sharedApplication].statusBarHidden = YES;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")){
        [[UIApplication sharedApplication] setStatusBarHidden:NO
                                                withAnimation:UIStatusBarAnimationFade];
    }else{
        [UIApplication sharedApplication].statusBarHidden = NO;
    }
}

-(id)initHDVPopup:(NSDictionary *)dict{
    [self.view setFrame:CGRectMake(0, 0, kWinsize.width, kWinsize.height)];
    self.imgView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:self.imgView];
    [self.imgView setImageWithURL:[NSURL URLWithString:dict[@"popup"]?:@""] placeholderImage:self.imgView.image];
    self.urlStore = dict[@"store_url"]?:@"";
    
    self.btAds = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btAds.frame = self.view.bounds;
    [self.btAds  addTarget:self action:@selector(didClickAds:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.btAds];
    
    self.btClose = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-60, 0, 60, 60)];
    [self.btClose setImage:[UIImage imageNamed:@"ic_close"] forState:0];
    [self.btClose setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [self.btClose  addTarget:self action:@selector(didClickClose:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.btClose];
    
    return self;
    
}

-(void)showOnViewController:(UIViewController *)rootViewController{
    [rootViewController presentViewController:self animated:YES completion:nil];
}


-(void)didClickClose:(id)sender{
//    _didClickClose?_didClickClose():0;
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate hdvAdsPopupDidClose:self];
    }];
    
}

-(void)didClickAds:(id)sender{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_urlStore]];
            [self didClickClose:nil];
            [self.delegate hdvAdsPopupDidClick:self];
        });
    });
}
@end
