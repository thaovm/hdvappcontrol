//
//  HDVAdsView.h
//  Mp3Player
//
//  Created by ThaoVM on 9/10/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HDVAdsView : UIView
@property(nonatomic,strong) UIImageView * imgView;
@property(nonatomic,strong) UIButton * btClose;
@property(nonatomic,strong) UIButton * btAds;
@property(nonatomic,strong) NSString * urlStore;
@property(nonatomic,strong) void(^didClickClose)();
-(id)initHDVBanner:(NSDictionary *)dict;
-(id)initHDVThumbnail:(NSDictionary *)dict;
@end

