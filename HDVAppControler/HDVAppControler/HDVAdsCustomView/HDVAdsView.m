//
//  HDVAdsView.m
//  Mp3Player
//
//  Created by ThaoVM on 9/10/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import "HDVAdsView.h"
@implementation HDVAdsView

-(id)initHDVBanner:(NSDictionary *)dict{
    self = [super initWithFrame:CGRectMake(0, 0, kWinsize.width, kIsIPhone?60:90)];
    self.imgView = [[UIImageView alloc] initWithFrame:self.bounds];
    [self addSubview:self.imgView];
    [self.imgView setImageWithURL:[NSURL URLWithString:dict[@"banner"]?:@""] placeholderImage:self.imgView.image];
    self.urlStore = dict[@"store_url"]?:@"";
    self.imgView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
    
    self.btAds = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btAds.frame = self.bounds;

    [self.btAds  addTarget:self action:@selector(didClickAds:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btAds];
    
    return self;
}
-(id)initHDVThumbnail:(NSDictionary *)dict{
    self = [super initWithFrame:CGRectMake(0, 0, kWinsize.width, kWinsize.width * 9/16)];
    self.imgView = [[UIImageView alloc] initWithFrame:self.bounds];
    [self addSubview:self.imgView];
    [self.imgView setImageWithURL:[NSURL URLWithString:dict[@"thumbai"]?:@""] placeholderImage:self.imgView.image];
    self.urlStore = dict[@"store_url"]?:@"";
    self.imgView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleRightMargin;
    
    
    self.btAds = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btAds.frame = self.bounds;
    [self.btAds  addTarget:self action:@selector(didClickAds:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btAds];
    
//    self.btClose = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width-40, 0, 40, 40)];
//    [self.btClose setImage:[UIImage imageNamed:@"close_eq"] forState:0];
//    [self.btClose setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
//    [self.btClose  addTarget:self action:@selector(didClickClose:) forControlEvents:UIControlEventTouchUpInside];
//    [self addSubview:self.btClose];

    return self;
}



-(void)didClickClose:(id)sender{
    _didClickClose?_didClickClose():0;
}

-(void)didClickAds:(id)sender{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_urlStore]];
        });
    });
}

@end
