//
//  HDVAdsPopup.h
//  Mp3Player
//
//  Created by ThaoVM on 9/12/15.
//  Copyright (c) 2015 doduong. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HDVAdsPopup;
@protocol HDVAdsPopupDelegate<NSObject>
@optional
-(void)hdvAdsPopupDidClick:(HDVAdsPopup *)adsPopup;
-(void)hdvAdsPopupDidClose:(HDVAdsPopup *)adsPopup;
@end

@interface HDVAdsPopup : UIViewController
@property(nonatomic,strong) UIImageView * imgView;
@property(nonatomic,strong) UIButton * btClose;
@property(nonatomic,strong) UIButton * btAds;
@property(nonatomic,strong) id<HDVAdsPopupDelegate> delegate;
@property(nonatomic,strong) NSString * urlStore;
-(void)showOnViewController:(UIViewController *)rootViewController;
-(id)initHDVPopup:(NSDictionary *)dict;

@end


