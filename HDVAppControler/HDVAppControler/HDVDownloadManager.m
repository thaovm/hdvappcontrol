//
//  HDVDownloadManager.m
//  HDVAppControler
//
//  Created by ThaoVM on 9/7/15.
//  Copyright (c) 2015 ThaoVM. All rights reserved.
//

#import "HDVDownloadManager.h"
#import "HDVLocalData.h"
#import "HDVAdsManager.h"
#import "HDVLocalSetting.h"
@implementation HDVDownloadManager
+(instancetype)sharedInstance{
    static HDVDownloadManager *manager = NULL;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[HDVDownloadManager alloc] init];
    });
    return manager;
}
-(instancetype) init
{
    if(self = [super init])
    {
        
    }
    return self;
}

-(void)setDicInfoDownloadData:(NSDictionary *)dicInfoDownload{
    _dicInfoDownload = [[NSDictionary alloc] initWithDictionary:dicInfoDownload];
    if(![self haveInitDownloadLimit])
    {
        [self saveDownloadLimit:[self getCreateDownload]];
    }

}

-(BOOL)allowDownloadLimit{
    if(!_dicInfoDownload[@"limit_download_status"])
    {
        return false;
    }
    
    return [_dicInfoDownload[@"limit_download_status"] intValue];
}

-(BOOL)haveInitDownloadLimit{
    if(![HDVLocalData getBoolForKey:eLocalFileKeyHaveInitDownloadLimit appendKey:@""])
    {
        [HDVLocalData saveBool:YES forKey:eLocalFileKeyHaveInitDownloadLimit appendKey:@""];
        return NO;
    }
    return YES;
}

-(int)getCreateDownload{
    return [_dicInfoDownload[@"number_create_download"] intValue];
}


-(int)getAddDownload
{
    //#if DEBUG
    //    return 2;
    //#endif
    
    return [_dicInfoDownload[@"number_add_download"] intValue];
}

-(int)getPreAddDownload
{
    if(![self allowDownloadLimit])
    {
        return 0;
    }
    
    return [_dicInfoDownload[@"number_download_add_to_agree"] intValue];
}

-(NSString *)getDownloadMessage
{
    return [_dicInfoDownload[@"message"] length]? _dicInfoDownload[@"message"]:@"Bạn đã sử dụng hết lượt tải. Bạn có muốn click quảng cáo để có thêm lượt tải";
}

-(NSString *)getSongSourceName
{
    return [_dicInfoDownload[@"name_var_mp3_download"] length]?_dicInfoDownload[@"name_var_mp3_download"]:@"source";
}
-(NSString *)getVideoSourceName
{
    return [_dicInfoDownload[@"name_var_video_download"] length]?_dicInfoDownload[@"name_var_video_download"]:@"source";
}

-(BOOL) getAllowShowPurchaseVip
{
    //    if(![_mdictDownloadLimit[@"url"] isKindOfClass:[NSString class]])
    //    {
    //        return false;
    //    }
    //    else if(![_mdictDownloadLimit[@"url"] length] == 0)
    //    {
    //        return true;
    //    }
    
    NSString *value = [NSString stringWithFormat:@"%@",_dicInfoDownload[@"view_button_buy_pro"]];
    return [value intValue] == 1;
}

-(NSString *)getUrlPurchaseVip
{
    return _dicInfoDownload[@"url_store"]?:@"";
}


-(void)saveDownloadLimit:(NSInteger)downloadCount{
    [HDVLocalData saveInteger:downloadCount forKey:eLocalFileKeyHaveDownloadLimit appendKey:@""];
}

-(NSInteger)getDownloadLimit{
    NSInteger s = [HDVLocalData getIntegerForKey:eLocalFileKeyHaveDownloadLimit appendKey:@""];
    if(s <=0)
    {
        if(![self allowDownloadLimit])
        {
            return 2;
        }
        
        if(![[HDVAdsManager sharedInstance] bannerIsReady])
        {
            return 2;
        }
        
        if(![[HDVAdsManager sharedInstance] popupDownloadIsReady])
        {
            return 2;
        }
        
        if([HDVLocalSetting getDisableShowAd])
        {
            return 10000;
        }
    }
    
    return s;
}
-(void)saveDownloadedCount{
    NSInteger count = [self getDownloadedCount] +1;
    [HDVLocalData saveInteger:count forKey:eLocalFileKeyDownloadCount appendKey:@""];
}

-(NSInteger)getDownloadedCount{
    return [HDVLocalData getIntegerForKey:eLocalFileKeyDownloadCount appendKey:@""];
}
@end
