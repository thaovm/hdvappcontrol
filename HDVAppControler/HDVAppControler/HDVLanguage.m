//
//  HDVLanguage.m
//  HDVAppControler
//
//  Created by ThaoVM on 9/7/15.
//  Copyright (c) 2015 ThaoVM. All rights reserved.
//

#import "HDVLanguage.h"

@implementation HDVLanguage

+(instancetype)sharedInstance
{
    static HDVLanguage *language = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        language = [[HDVLanguage alloc] init];
    });
    
    return language;
}

-(instancetype)init
{
    if(self = [super init])
    {
        _dictLanguage = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MultiLanguage" ofType:@"plist"]];
    }
    
    return self;
}

-(void) setAppLanguage:(NSString *)langeCode
{
    if (![langeCode isEqualToString:@"VN"]) {
        langeCode = @"EN";
    }
    [HDVLocalData saveString:langeCode forKey:eLocalFileKeyLanguage appendKey:@""];

}


-(NSString *) getAppLanguage
{
    NSString *language = [HDVLocalData getStringForKey:eLocalFileKeyLanguage appendKey:@""];
    if(language.length == 0)
        return @"EN";
    return language;
}





-(NSString *)stringForKey:(NSString *)key
{
    NSString *language = [self getAppLanguage];
    
    NSDictionary *dictContent = _dictLanguage[key];
    if(dictContent == nil)
    {
        return key;
    }
    else
    {
        if([language isEqualToString:@"VN"])
        {
            return dictContent[@"VN"]?:key;
        }
        else
        {
            return dictContent[@"Default"]?:key;
        }
    }
    
    return key;
}

@end
