//
//  HDVAppControll.h
//  HDVAppControler
//
//  Created by ThaoVM on 9/5/15.
//  Copyright (c) 2015 ThaoVM. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kAppControllHaveThumbnail @"kac_1"
#define kHDVACM_Update @"khdvacm_1"

#define kAppId @"85652"
typedef NS_ENUM(NSUInteger, TNAppControllAttirbute) {
    TNAppControllAttirbuteUpdate,
    TNAppControllAttirbuteUpdateStatus,
    TNAppControllAttirbuteUpdateTitle,
    TNAppControllAttirbuteUpdateDesc,
    TNAppControllAttirbuteUpdateUrl,
    TNAppControllAttirbuteUpdateVersion,
    TNAppControllAttirbuteAdsNetwork,
    TNAppControllAttirbuteOffsetShowApp,
    TNAppControllAttirbuteOffsetShowAds,
    TNAppControllAttirbuteListApps,
    TNAppControllAttirbuteAppTitle,
    TNAppControllAttirbuteAppDesc,
    TNAppControllAttirbuteAppUrlScheme,
    TNAppControllAttirbuteAppUrl,
    TNAppControllAttirbuteAppButton1,
    TNAppControllAttirbuteAppButton2,
    TNAppControllAttirbuteExitApps,
    TNAppControllAttirbuteExitAppsImage,
    TNAppControllAttirbuteExitAppsUrl,
    TNAppControllAttirbuteRemoveAd
};
@interface HDVAppControll : NSObject
@property(nonatomic,strong) NSDictionary * dictAppControll;
@property (strong, nonatomic) NSDictionary *dictCloseAppAdd;
@property (copy, nonatomic) void(^blockReachableWifi)();
+(instancetype) sharedInstance;
//-(void) loadFileApiConfig;
-(void) checkAppControll:(void (^)(BOOL result))completionBlock;
-(void) getAppcontrollInfoComletion:(void (^)(BOOL result))completionBlock;
//-(void) handleRemoteNotify:(NSDictionary *)userInfo onLaunch:(BOOL)onLaunch;

//-(id) getAppControllAttribute:(TNAppControllAttirbute)attName;
//-(id) getAttritbue:(TNAppControllAttirbute)attName inDictionary:(NSDictionary *)dict;
//-(void) getUserCountryCompletion:(void(^)(NSDictionary *dictInfo))completionBlock;
//-(void) registerUserCompletion:(void(^)(BOOL result))completionBlock;
-(void) getSMSContentComplete:(void(^)(NSString *sms))completeBlock;
-(void) reportScoreFromEvent:(NSString *)eventName andScore:(NSInteger)score Complete:(void(^)())completionBlock;
-(void) sendAPNSToken:(NSString *)token Complete:(void(^)(BOOL result))completionBlock;
-(void) reportProByScoreComplete:(void (^)(bool))completeBlock;
-(void) reportPushNotifyCode:(NSString *)code NotifyToken:(NSString *)token Complete:(void(^)())completionBlock;
#pragma mark - get appinfor for remote notify
-(void) getAppInfoForNotify:(NSString *)appId Complete:(void(^)(NSObject *osa))completionBlock;
@end
