//
//  HDVAdsManager.h
//  HDVAppControler
//
//  Created by ThaoVM on 9/4/15.
//  Copyright (c) 2015 ThaoVM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AdColony/AdColony.h>
#import <GoogleMobileAds/GoogleMobileAds.h>
#import <StartApp/StartApp.h>
#import <FBAudienceNetwork/FBAdView.h>
#import <FBAudienceNetwork/FBInterstitialAd.h>
#import <MobileCore/MobileCore.h>
#if ZING
#define kHDVAdsGADBannerId @"ca-app-pub-2559325879253235/6845103909"
#define kHDVAdsGADPopupId @"ca-app-pub-2559325879253235/9798570302"
#elif DUONG_BD
#define kHDVAdsGADBannerId @"ca-app-pub-3400509913902446/6421335415"
#define kHDVAdsGADPopupId @"ca-app-pub-3400509913902446/7898068613"
#elif NCT
#define kHDVAdsGADBannerId @"ca-app-pub-8390394110183761/8140161131"
#define kHDVAdsGADPopupId @"ca-app-pub-8390394110183761/8000560334"
#else
#define kHDVAdsGADBannerId @"ca-app-pub-2559325879253235/6845103909"
#define kHDVAdsGADPopupId @"ca-app-pub-2559325879253235/9798570302"
#endif
#define kHDVFaceBannerId @"999215223462174_1003051093078587"
#define kHDVFacePopupId @"999215223462174_1003051513078545"
#define kHDVFaceThumbnaiId @"999215223462174_1003051586411871"

#define kAdsColonyScoreZoneID @"vzf48437c5763a478c84"
#define kAdsColonyZoneID @"vz7febb3f6aece445094"
#define kAdsColonyAppID @"appfc92832ddfee4b10bc"
#define kHDVAdsSTAAppId @"210165628"
#define kHDVAdsSTADevId @"110765044"
#define KHDVMobileCoreHashCode @"SBTO9NPNTY2470CF8DQQ21Z2M078"
#define kHDVStringSTA    @"startapps"
#define kHDVStringAdmob       @"admob"
#define kHDVStringFace       @"facebook"

#define kHDVAdsNotifyHaveBanner @"khdva_1"
#define kHDVAdsNotifyClickBanner @"khdva_2"
#define kAppControllHaveThumbnail @"kac_1"
#define kHDVAdsNoneReuseTag 126
typedef NS_ENUM(NSInteger, eAdsType){
    eAdsTypeGadBanner = -1,
    eAdsTypeGadPopup = -2,
    eAdsTypeGadThumbnai = -3,
    eAdsTypeStaAppId = -4,
    eAdsTypeStaDevId = -5,
    eAdsTypeFaceBanner = -7,
    eAdsTypeFacePopup = -8,
    eAdsTypeFaceThumbnai = -9,
    eAdsTypeMobileCore = -10,
    eAdsTypeCodonyAppId = -11,
    eAdsTypeCodonyZoneId = -12
    
};
typedef NS_ENUM(NSInteger, eAdsTypePopup){
    eAdsTypePopupNomal = 1,
    eAdsTypePopupScore = 2,
    eAdsTypePopupDownload = 3,
    eAdsTypePopupDownload2 = 4
    
};
@interface HDVAdsManager : NSObject
@property (assign, nonatomic)BOOL leaveAppWhenClick;
@property (copy, nonatomic) void(^onClickRemoveHDVAds)();
@property (copy, nonatomic) void(^dismissEndPopupBlock)();
@property (copy, nonatomic) void(^didClickDownloadPopup)();
@property (copy, nonatomic) void(^doneWithViewPopup)();
@property (copy, nonatomic) void(^didClickScorePopup)();
@property (nonatomic, assign) BOOL haveScoreAdcolony;

+(instancetype)sharedInstance;

-(BOOL)thumbnailIsReady;
-(BOOL)popupIsReady;
-(BOOL)popupScoreIsReady;
-(BOOL)popupDownloadIsReady;
-(BOOL)popupDownload2IsReady;
-(BOOL)bannerIsReady;
-(BOOL)bannerBottomIsReady;
-(BOOL)bannerAudioIsReady;

-(UIView *)getCurrentBannerBottom;
-(UIView *)getCurrentBannerForAudio;
-(UIView *)getCurrentBanner;
-(UIView *)getCurrentThumbnai;


-(void)showCurrentNormalPopup;
-(void)showCurrentScorePopup;
-(void)showCurrentDownloadPopup;
-(void)showCurrentDownloadPopup2;
-(void)prepareAds;
-(void)showAds;

-(NSInteger)getTimeoffsetShowScoreAd;
-(NSInteger)getTimeoffsetShowScoreViewPoup;
-(NSInteger)getTimeClickDownloadAds;
-(NSInteger)getClickAdsLimit;
-(NSInteger)getTimeOffsetShowUpdate;

-(void)saveTimeClickDownloadAds;
-(void) saveTimeCloseThumbnail:(NSInteger)time;
-(void)showDownloadPopup:(id)idcontroller;
-(BOOL)getSystemScoreStatus;
-(void)savePendingApp:(NSDictionary *)app;
-(void)deletePendingApp:(NSDictionary *)app;

-(NSArray *)getPendingAppList;
-(NSArray *)checkAppInstallAndAddPoint;
-(NSArray *)getScoreRateList;
-(NSArray *)getListBonus;
-(NSArray *)getScorePageList;
-(NSArray *)getScoreShareList;
-(NSArray *)getListBonusApps;
-(NSArray *)getAllListBonus;
-(NSArray *)getListAds;
-(NSString *)getAppleStoreId;
-(void) showScoreAdcolony;

#pragma mark - limit click


@end
