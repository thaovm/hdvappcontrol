//
//  HDVLanguage.h
//  HDVAppControler
//
//  Created by ThaoVM on 9/7/15.
//  Copyright (c) 2015 ThaoVM. All rights reserved.
//

#import <Foundation/Foundation.h>
#define TextForKey(s) [[HDVLanguage sharedInstance] stringForKey:s]
@interface HDVLanguage : NSObject{
    NSDictionary *_dictLanguage;
}
+(instancetype) sharedInstance;

-(void) setAppLanguage:(NSString *)langeCode;
-(NSString *) getAppLanguage;
-(void)saveAppLanguage;
-(NSString *) stringForKey:(NSString *)key;
@end
