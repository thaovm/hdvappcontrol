//
//  FBHelper.m
//  FacebookAPI
//
//  Created by doduong on 8/26/13.
//  Copyright (c) 2013 doduong. All rights reserved.
//

#import "FBHelper.h"


@implementation FBHelper

#pragma mark - private method;
-(void) startSession
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,
    ^{
//        @[@"publish_stream"]
        _fbSession = [[FBSession alloc] initWithAppID:kFBHAppID permissions:@[@"public_profile",@"email",@"user_likes"] defaultAudience:FBSessionDefaultAudienceFriends urlSchemeSuffix:@"fb" tokenCacheStrategy:[FBSessionTokenCachingStrategy defaultInstance]];
        
        NSLog(@"Call startSession");
        
        // if have old token a session will open* without switch app.
        if(_fbSession.state == FBSessionStateCreatedTokenLoaded)
        {
            [_fbSession openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error)
            {
                [self sessionOpenStatusChange];
            }];
        }
    });
}

-(void) sessionOpenStatusChange
{
    if(_fbSession.isOpen)
    {
        if(_delegate!= nil)
        {
            if([_delegate respondsToSelector:@selector(fbHelperDidLogin)])
            {
                [_delegate fbHelperDidLogin];
            }
        }
    }
    else
    {
        if(_delegate!= nil)
        {
            if([_delegate respondsToSelector:@selector(fbHelperDidLogout)])
            {
                [_delegate fbHelperDidLogout];
            }
        }
    }
}


#pragma mark - public + class methods.

-(id) init
{
    self = [super init];
    if (self)
    {
        _fbSession = nil;
        _delegate = nil;
        _dictFBUserProfile = nil;
    }
    return self;
}

-(void) loginFB
{
    [self startSession];
    
    if(!_fbSession.isOpen)
    {
        if(_fbSession.state != FBSessionStateCreated)
        {
            _fbSession = [[FBSession alloc] initWithAppID:kFBHAppID permissions:@[@"public_profile",@"email",@"user_likes"] defaultAudience:FBSessionDefaultAudienceFriends urlSchemeSuffix:@"fb" tokenCacheStrategy:[FBSessionTokenCachingStrategy defaultInstance]];
        }
        
        [_fbSession openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error){
            [self sessionOpenStatusChange];
        }];
    }
    else
    {
        if(_delegate!= nil)
        {
            if([_delegate respondsToSelector:@selector(fbHelperDidLogin)])
            {
                [_delegate fbHelperDidLogin];
            }
        }
    }
}

-(void) loginFBWithCallbackBlock:(void (^)(void))finished
{
    [self startSession];
    
    if(!_fbSession.isOpen)
    {
        if(_fbSession.state != FBSessionStateCreated)
        {
            _fbSession = [[FBSession alloc] initWithAppID:kFBHAppID permissions:@[@"public_profile",@"email",@"user_likes"] defaultAudience:FBSessionDefaultAudienceFriends urlSchemeSuffix:@"fb" tokenCacheStrategy:[FBSessionTokenCachingStrategy defaultInstance]];
        }
        
        [_fbSession openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error){
            [self sessionOpenStatusChange];
            finished();
        }];
    }

}

-(void) loginFBComplete:(void (^)(BOOL))finished
{
    [self startSession];
    
    if(!_fbSession.isOpen)
    {
        if(_fbSession.state != FBSessionStateCreated)
        {
            _fbSession = [[FBSession alloc] initWithAppID:kFBHAppID permissions:@[@"public_profile",@"email",@"user_likes"] defaultAudience:FBSessionDefaultAudienceFriends urlSchemeSuffix:@"fb" tokenCacheStrategy:[FBSessionTokenCachingStrategy defaultInstance]];
        }
        
        [_fbSession openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error){
            [self sessionOpenStatusChange];
            if(finished)
            {
                if(error)
                {
                    finished(NO);
                }
                else
                {
                    finished(YES);
                }
            }
        }];
    }
    else
    {
        finished(YES);
    }
}

-(void) loginFBWithCacheComplete:(void(^)(BOOL result))finished
{
    [self startSession];
    
    if(_fbSession == nil)
    {
        _fbSession = [[FBSession alloc] initWithAppID:kFBHAppID permissions:@[@"public_profile",@"email",@"user_likes"] defaultAudience:FBSessionDefaultAudienceFriends urlSchemeSuffix:@"fb" tokenCacheStrategy:[FBSessionTokenCachingStrategy defaultInstance]];
        
        NSLog(@"Call startSession");
        
        // if have old token a session will open* without switch app.
        if(_fbSession.state == FBSessionStateCreatedTokenLoaded)
        {
            [_fbSession openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error)
             {
                 [self sessionOpenStatusChange];
             }];
        }
    }
    
    if(!_fbSession.isOpen)
    {        
        [_fbSession openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error){
            [self sessionOpenStatusChange];
            if(finished)
            {
                if(error)
                {
                    finished(NO);
                }
                else
                {
                    finished(YES);
                }
            }
        }];
    }
    else
    {
        finished(YES);
    }
}

-(void) logoutFB
{
    if(_fbSession.isOpen)
    {
        [_fbSession closeAndClearTokenInformation];
    }
    _dictFBUserProfile = nil;
    
    // clear cookie
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) getCurrentUserData
{
    if(_fbSession== nil)
    {
        NSLog(@"FBHelper getCurrentUserInfo: session nil");
        return;
    }
    
    if(!_fbSession.isOpen)
    {
        NSLog(@"FBHelper getCurrentUserInfo: session not open");
        return;
    }
    
    if(_dictFBUserProfile!= nil)
    {
        if(_delegate != nil)
        {
            if([_delegate respondsToSelector:@selector(fbHelperDidReceiveUserData)])
            {
                [_delegate fbHelperDidReceiveUserData];
            }
        }
        NSLog(@"FBHelper getCurrentUserInfo: user data received b4!");
        return;
    }
    
    [FBSession setActiveSession:_fbSession];
    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        NSLog(@"FBHelper: Get current user profile have Error: %@",error);
        
//        NSString *resultString = [result JSONString];
//        _dictFBUserProfile = [[NSDictionary alloc] initWithDictionary: [resultString objectFromJSONString]];

        
        NSDictionary *info = [NSJSONSerialization JSONObjectWithData:result options:NSJSONReadingAllowFragments error:nil];
        
        _dictFBUserProfile = [[NSDictionary alloc] initWithDictionary: info];
        
        if(_delegate!= nil)
        {
            if([_delegate respondsToSelector:@selector(fbHelperDidReceiveUserData)])
            {
                [_delegate fbHelperDidReceiveUserData];
            }
        }
    }];
}

-(void) getCurrentUserLikedPageComplete:(void (^)(NSArray *))completionBlock
{
    if(_fbSession== nil)
    {
        NSLog(@"FBHelper getCurrentUserInfo: session nil");
        return;
    }
    
    if(!_fbSession.isOpen)
    {
        NSLog(@"FBHelper getCurrentUserInfo: session not open");
        return;
    }
    
    [FBSession setActiveSession:_fbSession];
    [FBRequestConnection startWithGraphPath:@"/me/likes?limit=2000" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        NSLog(@"%s: %@",__FUNCTION__,result);
        
        if(error == nil)
        {
            if(completionBlock)
            {
                completionBlock(result[@"data"]);
            }
        }
        else
        {
            if(completionBlock)
            {
                completionBlock(nil);
            }
        }
        
    }];

}

+(FBHelper *) sharedHelper
{
    static FBHelper *helper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        helper = [[FBHelper alloc] init];
    });
    
    return helper;
}

-(void) shareLink:(NSString *)link
{
    NSDictionary *myParams = nil;
    
    myParams = @{
                 @"link": link,
                 };
    
    NSLog(@"My param: %@",myParams);
    
    // If the Facebook app is installed and we can present the share dialog
    [FBSettings setDefaultAppID:kFBHAppID];
    
    [FBWebDialogs presentFeedDialogModallyWithSession:[FBHelper sharedHelper].fbSession parameters:myParams handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
        ;
    }];
    
}

@end
