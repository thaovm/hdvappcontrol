//
//  FBManager.m
//  XemBoi
//
//  Created by MacBookPro on 10/21/13.
//  Copyright (c) 2013 GallGall. All rights reserved.
//

#import "FBManager.h"

@implementation FBManager

+ (id)sharedManager{
    static FBManager *sharedInstances = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstances = [[self alloc] init];
    });
    return sharedInstances;
}

- (id)init{
    self = [super init];
    if (self) {
    }
    return self;
}

#pragma mark - Function Methods
- (void)loginWithSuccess:(FBLoginSuccess)success failed:(FBLoginFailed)failed{
    self.loginSuccess = success;
    self.loginFailed = failed;
    [FBSession openActiveSessionWithReadPermissions:nil allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
        [self sessionStateChanged:session state:status error:error];
    }];
}

- (void)logout{
    [[FBSession activeSession] closeAndClearTokenInformation];
}

- (void)shareWithName:(NSString *)name caption:(NSString *)caption description:(NSString *)description link:(NSString *)link picture:(NSString *)picture success:(FBShareSuccess)success failed:(FBShareFailed)failed{
    self.shareSuccess = success;
    self.shareFailed  = failed;
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            name        , @"name",
                            caption     , @"caption",
                            description , @"description",
                            link        , @"link",
                            picture     , @"picture",
                            nil];
    
    FBLinkShareParams *paramFb = [[FBLinkShareParams alloc] init];
    paramFb.link = [NSURL URLWithString:link];
    paramFb.name = name;
    paramFb.caption = caption;
    paramFb.linkDescription = description;
    paramFb.picture = [NSURL URLWithString:picture];
    
    
    if([FBDialogs canPresentMessageDialogWithParams:paramFb])
    {
        
        [FBDialogs presentShareDialogWithParams:paramFb clientState:nil handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
            if(error)
            {
                self.shareFailed(error.localizedDescription);
            }
            else
            {
                
                NSLog(@"Result: %@",results);
                if([results[@"completionGesture"] isEqualToString:@"cancel"])
                {
                    self.shareFailed(error.localizedDescription);
                    return ;
                }
                
                self.shareSuccess();

            }
        }];
    }
    else
    {
        [FBWebDialogs presentFeedDialogModallyWithSession:nil parameters:params handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
            if (result == FBWebDialogResultDialogCompleted && self.shareSuccess) {
                self.shareSuccess();
            }
            else if (result == FBWebDialogResultDialogNotCompleted && self.shareFailed){
                if (error) {
                    self.shareFailed(error.localizedDescription);
                }
            }
        }];
    }
}

- (void)shareWithName2:(NSString *)name caption:(NSString *)caption description:(NSString *)description link:(NSString *)link picture:(NSString *)picture success:(FBShareSuccess)success failed:(FBShareFailed)failed{
    self.shareSuccess = success;
    self.shareFailed  = failed;
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            name        , @"name",
                            caption     , @"caption",
                            description , @"description",
                            link        , @"link",
                            picture     , @"picture",
                            nil];
    
    FBLinkShareParams *params2 = [[FBLinkShareParams alloc] init];
    params2.link = [NSURL URLWithString:link?:@""];
    
    //     If the Facebook app is installed and we can present the share dialog
    if ([FBDialogs canPresentShareDialogWithParams:params2]) {
        // Present the share dialog
        [FBDialogs presentShareDialogWithLink:params2.link handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
            
            if(error==nil)
            {
                if(_shareSuccess)
                {
                    _shareSuccess();
                }
            }
            else
            {
                if(_shareFailed)
                {
                    _shareFailed(error.localizedDescription);
                }
            }
        }];
    } else {
        // Present the feed dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil parameters:params handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
            if (result == FBWebDialogResultDialogCompleted && self.shareSuccess) {
                self.shareSuccess();
            }
            else if (result == FBWebDialogResultDialogNotCompleted && self.shareFailed){
                if (error) {
                    self.shareFailed(error.localizedDescription);
                }
            }
        }];
    }
}

-(void)inviteFriendSuccess:(void (^)())successBlock Failed:(void (^)())failedBlock
{
    NSDictionary *param = @{
                            @"link":@"https://fb.me/562889707183549",
                            @"app_link":@"https://fb.me/562889707183549",
                            @"app_link_url":@"https://fb.me/562889707183549",
                            };
    
    [FBWebDialogs presentRequestsDialogModallyWithSession:nil message:NSLocalizedString(@"FBinviteMessage", nil) title:nil parameters:param handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
        NSLog(@"complete...");
    }];
}

- (void)getCurrentUserSuccess:(FBGetUserInfoSuccess)success failed:(FBGetUserInfoFailed)failed{
    [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (failed && error) {
            failed(error.description);
        }
        else if (success && !error)
        {
            NSDictionary<FBGraphUser> *user = (NSDictionary<FBGraphUser> *)result;
            success(user);
        }
    }];
}

#pragma mark - Private Methods
- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen:
        {
            if (self.loginSuccess) {
                self.loginSuccess();
            }
        }
            break;
        
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
        {
            [[FBSession activeSession] closeAndClearTokenInformation];
            if (error && self.loginFailed) {
                self.loginFailed(error.localizedDescription);
            }
        }
            break;
            
        default:
            break;
    }
}

@end
