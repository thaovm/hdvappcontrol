//
//  FbUtil.h
//  TrieuPhu
//
//  Created by doduong on 12/31/13.
//
//

#ifndef __TrieuPhu__FbUtil__
#define __TrieuPhu__FbUtil__

#include <iostream>

class FbUtil
{
public:
    static void CallLogin();
    static void CallFbNativeApp(const char* localImgPath,const char* name);
};

#endif /* defined(__TrieuPhu__FbUtil__) */
