//
//  FbUtil.cpp
//  TrieuPhu
//
//  Created by doduong on 12/31/13.
//
//

#include "FbUtil.h"
#include "FBHelper.h"

//#ifdef TIM_UBRAIN
//#define kStoreURL @"https://itunes.apple.com/app/id950930270"
//#elif TIM_JUNE
//#define kStoreURL @"https://itunes.apple.com/app/id958813853"
//#else
#define kStoreURL @"https://itunes.apple.com/app/id963409747"
//#endif

void FbUtil::CallLogin()
{
    [[FBHelper sharedHelper] loginFB];
}

void FbUtil::CallFbNativeApp(const char* localImgPath,const char* name)
{
    NSDictionary *myParams = nil;
    
    if(strlen(localImgPath) && strlen(name))
    {
        myParams = @{
                     @"link": kStoreURL,
                     @"name": [NSString stringWithUTF8String:name],
                     @"caption":@"",
                     @"picture": @"",
                     @"description": @"Download and play with me!"
                     };
    }
    else
    {
        myParams = @{
                     @"link": kStoreURL,
                     @"name": @"",
                     @"caption":@"",
                     @"picture": @"",
                     @"description": @"Download and play with me!"
                     };
    }
    

    
    NSLog(@"My param: %@",myParams);
    
    // Check if the Facebook app is installed and we can present the share dialog
    FBShareDialogParams *params = [[FBShareDialogParams alloc] init];
    params.link = [NSURL URLWithString:myParams[@"link"]];
    params.name = myParams[@"name"];
    params.caption = myParams[@"caption"];
    params.picture = [NSURL URLWithString:myParams[@"picture"]];
//    params.description = myParams[@"description"];
    
    // If the Facebook app is installed and we can present the share dialog
    [FBSettings setDefaultAppID:kFBHAppID];
//    if ([FBDialogs canPresentShareDialogWithParams:params])
//    {
//        // Present the share dialog
//        [FBDialogs presentShareDialogWithLink:params.link
//                                         name:params.name
//                                      caption:params.caption
//                                  description:params.description
//                                      picture:params.picture
//                                  clientState:nil
//                                      handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
//                                          if(error) {
//                                              // An error occurred, we need to handle the error
//                                              // See: https://developers.facebook.com/docs/ios/errors
//                                              NSLog([NSString stringWithFormat:@"Error publishing story: %@", error.description]);
//                                          } else {
//                                              // Success
//                                              NSLog(@"result %@", results);
//                                          }
//                                      }];
//    }
//    else
//    {
        [FBWebDialogs presentFeedDialogModallyWithSession:[FBHelper sharedHelper].fbSession parameters:myParams handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
            ;
        }];
//    }
}