//
//  FBHelper.h
//  FacebookAPI
//
//  Created by doduong on 8/26/13.
//  Copyright (c) 2013 doduong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

#if ZING
#define kFBHAppID @"533448736794313"
#elif DUONG_BD
#define kFBHAppID @"533448736794313"
#else
#define kFBHAppID @"701648873254318"
#endif

@protocol FBHelperDelegate <NSObject>

-(void) fbHelperDidLogin;
-(void) fbHelperDidLogout;
-(void) fbHelperDidReceiveUserData;

@end

@interface FBHelper : NSObject
{
}

@property (strong, nonatomic) FBSession *fbSession;
@property (strong, nonatomic) NSDictionary *dictFBUserProfile;
@property (assign, nonatomic) id<FBHelperDelegate> delegate;

-(id) init;
-(void) startSession;
-(void) loginFB;
-(void) logoutFB;
-(void) getCurrentUserData;
-(void)getCurrentUserLikedPageComplete:(void(^)(NSArray *arrPages))completionBlock;
-(void) loginFBWithCallbackBlock:(void (^)(void))finished;

-(void) loginFBComplete:(void(^)(BOOL result))finished;
-(void) loginFBWithCacheComplete:(void(^)(BOOL result))finished;


+(FBHelper *) sharedHelper;

-(void) shareLink:(NSString *)link;


@end
