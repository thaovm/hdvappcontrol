//
//  FBManager.h
//  XemBoi
//
//  Created by MacBookPro on 10/21/13.
//  Copyright (c) 2013 GallGall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

typedef void(^FBLoginSuccess)(void);
typedef void(^FBLoginFailed)(NSString *reason);
typedef void(^FBShareSuccess)(void);
typedef void(^FBShareFailed)(NSString *reason);
typedef void(^FBGetUserInfoSuccess)(NSDictionary<FBGraphUser> *user);
typedef void(^FBGetUserInfoFailed)(NSString *reason);

@interface FBManager : NSObject

+ (id)sharedManager;

- (void)loginWithSuccess:(FBLoginSuccess)success failed:(FBLoginFailed)failed;
- (void)logout;
- (void)shareWithName:(NSString *)name
                 caption:(NSString *)caption
             description:(NSString *)description
                    link:(NSString *)link
                 picture:(NSString *)picture
                 success:(FBShareSuccess)success
                  failed:(FBShareFailed)failed;

- (void)shareWithName2:(NSString *)name caption:(NSString *)caption description:(NSString *)description link:(NSString *)link picture:(NSString *)picture success:(FBShareSuccess)success failed:(FBShareFailed)failed;

- (void)getCurrentUserSuccess:(FBGetUserInfoSuccess)success failed:(FBGetUserInfoFailed)failed;

-(void)inviteFriendSuccess:(void(^)())successBlock Failed:(void(^)())failedBlock;

@property (nonatomic, copy) FBLoginSuccess    loginSuccess;
@property (nonatomic, copy) FBLoginFailed     loginFailed;
@property (nonatomic, copy) FBShareSuccess    shareSuccess;
@property (nonatomic, copy) FBShareFailed     shareFailed;

@end
