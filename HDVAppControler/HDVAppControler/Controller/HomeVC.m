//
//  HomeVC.m
//  HDVAppControler
//
//  Created by ThaoVM on 9/19/15.
//  Copyright (c) 2015 ThaoVM. All rights reserved.
//

#import "HomeVC.h"
#import "HomeCell.h"
@interface HomeVC (){
    NSMutableArray * _marrData;
    BOOL _showBanner, _showThumbnail;
}

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor lightGrayColor];
    // Do any additional setup after loading the view from its nib.
    [self.cvMain registerNib:[UINib nibWithNibName:@"HomeCell" bundle:nil] forCellWithReuseIdentifier:@"HomeCell"];
    _marrData = [NSMutableArray arrayWithObjects:@"Show Banner",@"Show Thumbnail",@"Show Popup Score",@"Show Popup Download",@"Show Popup Download2",@"Show Popup Normal", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
  
}


#pragma mark - UICollectionViewDelegate + Datasource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return _marrData.count;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    // disable bottom ads
    return CGSizeZero;
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        if([[HDVAdsManager sharedInstance] bannerIsReady] && _showBanner)
        {
            return CGSizeMake(kWinsize.width, kAdHeigh);
        }

    }
    if(section == 1){
        if([[HDVAdsManager sharedInstance] thumbnailIsReady] && _showThumbnail)
        {
            return CGSizeMake(kWinsize.width, kThumHeigh);
        }
    }
    return CGSizeZero;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(kWinsize.width, 50);
    
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    
    BaseCVHeader * cell = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"BaseCVHeader" forIndexPath:indexPath];
    if(indexPath.section == 0)
    {
        [cell addBanner];
    }else
        if(indexPath.section == 1)
        {
            [cell addThumbnai];
        }
    

    return cell;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeCell" forIndexPath:indexPath];
    cell.lbTitle.text = [_marrData objectAtIndex:indexPath.section];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    if(indexPath.section == 0){
        _showBanner = !_showBanner;
        _marrData[0] = _showBanner?@"Hide Banner":@"Show Banner";
        [self.cvMain reloadData];
    }
    if(indexPath.section == 1){
        _showThumbnail = !_showThumbnail;
        _marrData[1] = _showThumbnail?@"Hide Thumbnail":@"Show Thumbnail";
        [self.cvMain reloadData];
    }
    
   
    if(indexPath.section == 2){
        [[HDVAdsManager sharedInstance] showCurrentScorePopup];
    }
    if(indexPath.section == 3){
        [[HDVAdsManager sharedInstance] showCurrentDownloadPopup];
    }
    if(indexPath.section == 4){
        [[HDVAdsManager sharedInstance] showCurrentDownloadPopup2];
    }
    
    if(indexPath.section == 5){
        [[HDVAdsManager sharedInstance] showCurrentNormalPopup];
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0.5, 0, 0.5, 0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.5;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 1;
}


@end
