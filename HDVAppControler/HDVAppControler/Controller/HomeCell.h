//
//  HomeCell.h
//  HDVAppControler
//
//  Created by ThaoVM on 9/19/15.
//  Copyright (c) 2015 ThaoVM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCVCell.h"
@interface HomeCell : BaseCVCell
@property(nonatomic,strong)IBOutlet UILabel * lbTitle;
@end
